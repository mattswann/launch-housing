<?php
/*
Plugin Name: Icon Box for Visual Composer
Plugin URI: https://www.brainstormforce.com
Description: A plugin to add font-awesome icon type and icon-box component to Visual Composer
Author: Brainstorm Force
Author URI: https://www.brainstormforce.com
Version: 1.4.4
Text Domain: icon-box
*/
if(!class_exists('VC_Icon_Box'))
{
	class VC_Icon_Box
	{
		function __construct()
		{
			// Enqueue admin style
			add_action('admin_enqueue_scripts', array(&$this, 'icon_admin_styles' ) );
			// Enqueue icon box style
			add_action('wp_enqueue_scripts', array(&$this, 'icon_styles' ) );
			// Add shortcode for icon box
			add_shortcode('icon-box', array(&$this, 'icon_box' ) );
			if(defined('WPB_VC_VERSION') && version_compare(WPB_VC_VERSION, 4.8) >= 0) {
				if(function_exists('vc_add_shortcode_param'))
				{
					// Generate param type "icon"
					vc_add_shortcode_param('icon' , array(&$this, 'icon_settings_field' ) );
					// Generate param type "number"
					vc_add_shortcode_param('number' , array(&$this, 'number_settings_field' ) );
				}
			}
			else {
				if ( function_exists('add_shortcode_param'))
				{
					// Generate param type "icon"
					add_shortcode_param('icon' , array(&$this, 'icon_settings_field' ) );
					// Generate param type "number"
					add_shortcode_param('number' , array(&$this, 'number_settings_field' ) );
				}
			}
			// Initialize the icon box component for Visual Composer
			add_action('init', array( &$this, 'icon_box_init' ) );
		}
		// Enqueue admin styles
		function icon_admin_styles()
		{
			wp_enqueue_style('vc-font-awesome', plugins_url( '/css/font-awesome.min.css' , __FILE__ ));
			wp_enqueue_style('vc-icon-box', plugins_url( '/css/icon-box.css' , __FILE__ ));
		}
		// Enqueue styles
		function icon_styles()
		{
			wp_enqueue_style('vc-font-awesome', plugins_url( '/css/font-awesome.min.css' , __FILE__ ));
			wp_enqueue_style('vc-icon-box', plugins_url( '/css/style.css' , __FILE__ ));
		}
		// Add shortcode for icon-box
		function icon_box($atts)
		{
			$pos = $css_class = '';
			extract(shortcode_atts(array(
				'title'	  => '',
				'desc'	   => '',
				'link'	   => '',
				'icon_type'  => 'font-awesome',
				'icon_img'   => '',
				'icon'	   => 'android',
				'size'	   => '32',
				'icon_color' => '#89BA49',
				'color' 	  => '',
				'border'	 => '',
				'icon_border_color' => '',
				'icon_bg_color' => '',
				'pos'	    => 'default',
				'read_more'  => '',
				'read_text'  => 'Read More',
				'padding'	=> '5',
				'width'	  => '',
				'el_class'	  => '',
				'animation'  => 'top-to-bottom',
				),$atts,'icon-box'));
			if ($animation) {
				wp_enqueue_script('waypoints');
				$animation .= ' animate-element';
				$css_class = 'wpb_animate_when_almost_visible wpb_'.$animation;
			}
			$html = '';
			$prefix = '<div class="smicon-component '.$css_class.' '.$el_class.'">';
			$suffix = '</div> <!-- smicon-component -->';
			if($link !== ''){
				if($read_more == '')
				{
					$href = vc_build_link($link);
					$prefix .= '<a class="smicon-box-link" href="'.$href['url'].'" target="'.trim($href['target']).'">';
					$suffix .= '</a>';
				}
			}
			$ex_class = '';
			if($pos != '')
				$ex_class .= ' icon-'.$pos;
			if($border != '')
				$ex_class .= ' icon-'.$border;
			else
				$ex_class .= ' sm-icon-normal';
			$html .= '<div class="smicon-box '.$ex_class.'">';
				if($icon_type == 'font-awesome')
				{
					$html .= '<div class="smicon-icon">';
					$class = 'fa fa-'.$icon.' ';
					$border_width = ($width !== '') ? $width.'px' : ' 1px ';
					$style = ($color !== '') ? ' color:'.$icon_color.';' : ' ';
					$style .= ($size !== '') ? ' font-size:'.$size.'px;' : ' ';
					$style .= ($size !== '') ? ' width:'.$size.'px;' : ' ';
					$style .= ($size !== '') ? ' height:'.$size.'px;' : ' ';
					$style .= ($icon_border_color !== '') ? ' border: '.$border_width.' solid '.$icon_border_color.';' : ' ';
					$style .= ($icon_bg_color !== '') ? ' background: '.$icon_bg_color.';' : ' ';
					$style .= ($padding !== '') ? ' padding: '.$padding.'px;' : ' ';
					$html .= '<i class=" smicon-box-icon '.$class.'" style="'.$style.'"></i>';
					$html .= '</div> <!-- icon -->';
				} else {
					$img = wp_get_attachment_image_src( $icon_img, 'large');
					$html .= '<div class="smicon-icon">';
					$border_width = ($width !== '') ? $width.'px' : ' 1px ';
					$style = ($size !== '') ? ' width:'.$size.'px;height:'.$size.'px;' : ' ';
					$style .= ($icon_border_color !== '') ? ' border: '.$border_width.' solid '.$icon_border_color.';' : ' ';
					$style .= ($icon_bg_color !== '') ? ' background: '.$icon_bg_color.';' : ' ';
					$style .= ($padding !== '') ? ' padding: '.$padding.'px;' : ' ';
					$html .= '<img class=" smicon-box-icon " style="'.$style.'" src="'.$img[0].'"/>';
					$html .= '</div> <!-- icon -->';
				}
				if($title !== ''){
					$html .= '<div class="smicon-header">';
					$link_prefix = $link_sufix = '';
					if($link !== ''){
						if($read_more == 'title')
						{
							$href = vc_build_link($link);
							$link_prefix = '<a class="smicon-box-link" href="'.$href['url'].'">';
							$link_sufix = '</a>';
						}
					}
					$html .= $link_prefix.'<h3 class="smicon-title">'.$title.'</h3>'.$link_sufix;
					$html .= '</div> <!-- header -->';
				}
				if($desc !== ''){
					$html .= '<div class="smicon-description">';
					$html .= $desc;
					if($link !== ''){
						if($read_more == 'more')
						{
							$href = vc_build_link($link);
							$more_link = '<a class="smicon-read" href="'.$href['url'].'">';
							$more_link .= $read_text;
							$more_link .= '<i class="fa fa-angle-double-right"></i>';
							$more_link .= '</a>';
							$html .= $more_link;
						}
					}
					$html .= '</div> <!-- description -->';
				}
			$html .= '</div> <!-- smicon-box -->';
			$html = $prefix.$html.$suffix;
			return $html;
		}
		// Function generate param type "number"
		function number_settings_field($settings, $value)
		{
			$dependency = '';
			$param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
			$type = isset($settings['type']) ? $settings['type'] : '';
			$min = isset($settings['min']) ? $settings['min'] : '';
			$max = isset($settings['max']) ? $settings['max'] : '';
			$suffix = isset($settings['suffix']) ? $settings['suffix'] : '';
			$class = isset($settings['class']) ? $settings['class'] : '';
			$output = '<input type="number" min="'.$min.'" max="'.$max.'" class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" value="'.$value.'" style="max-width:100px; margin-right: 10px;" />'.$suffix;
			return $output;
		}
		// Function generate param type "radioimage"
		function radioimage_settings_field($settings, $value)
		{
			$dependency = '';
			$param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
			$type = isset($settings['type']) ? $settings['type'] : '';
			$radios = isset($settings['options']) ? $settings['options'] : '';
			$output = '';
			if($radios != '' && is_array($radios))
			{
				foreach($radios as $radio => $img)
				{
					$output .= '<input type="radio" name="'.$radio.'" id="' . $param_name . '" class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '">';
					$output .= '<label for="'. $param_name .'"><img src="'.$img.'"></label>';
				}
			}
			return $output;
		}
		// create icon style attribute
		function icon_settings_field($settings, $value)
		{
			$dependency = '';
			$param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
			$type = isset($settings['type']) ? $settings['type'] : '';
			$class = isset($settings['class']) ? $settings['class'] : '';
			$icons = array('ambulance','h-square','hospital-o','medkit','plus-square','stethoscope','user-md','wheelchair','adn','android','apple','bitbucket','bitbucket-square','bitcoin','btc','css3','dribbble','dropbox','facebook','facebook-square','flickr','foursquare','github','github-alt','github-square','gittip','google-plus','google-plus-square','html5','instagram','linkedin','linkedin-square','linux','maxcdn','pagelines','pinterest','pinterest-square','renren','skype','stack-exchange','stack-overflow','trello','tumblr','tumblr-square','twitter','twitter-square','vimeo-square','vk','weibo','windows','xing','xing-square','youtube','youtube-play','youtube-square','arrows-alt','backward','compress','eject','expand','fast-backward','fast-forward','forward','pause','play','play-circle','play-circle-o','step-backward','step-forward','stop','youtube-play','rub','ruble','rouble','pagelines','stack-exchange','arrow-circle-o-right','arrow-circle-o-left','caret-square-o-left','toggle-left','dot-circle-o','wheelchair','vimeo-square','try','turkish-lira','plus-square-o','adjust','anchor','archive','arrows','arrows-h','arrows-v','asterisk','ban','bar-chart-o','barcode','bars','beer','bell','bell-o','bolt','book','bookmark','bookmark-o','briefcase','bug','building-o','bullhorn','bullseye','calendar','calendar-o','camera','camera-retro','caret-square-o-down','caret-square-o-left','caret-square-o-right','caret-square-o-up','certificate','check','check-circle','check-circle-o','check-square','check-square-o','circle','circle-o','clock-o','cloud','cloud-download','cloud-upload','code','code-fork','coffee','cog','cogs','comment','comment-o','comments','comments-o','compass','credit-card','crop','crosshairs','cutlery','dashboard','desktop','dot-circle-o','download','edit','ellipsis-h','ellipsis-v','envelope','envelope-o','eraser','exchange','exclamation','exclamation-circle','exclamation-triangle','external-link','external-link-square','eye','eye-slash','female','fighter-jet','film','filter','fire','fire-extinguisher','flag','flag-checkered','flag-o','flash','flask','folder','folder-o','folder-open','folder-open-o','frown-o','gamepad','gavel','gear','gears','gift','glass','globe','group','hdd-o','headphones','heart','heart-o','home','inbox','info','info-circle','key','keyboard-o','laptop','leaf','legal','lemon-o','level-down','level-up','lightbulb-o','location-arrow','lock','magic','magnet','mail-forward','mail-reply','mail-reply-all','male','map-marker','meh-o','microphone','microphone-slash','minus','minus-circle','minus-square','minus-square-o','mobile','mobile-phone','money','moon-o','music','pencil','pencil-square','pencil-square-o','phone','phone-square','picture-o','plane','plus','plus-circle','plus-square','plus-square-o','power-off','print','puzzle-piece','qrcode','question','question-circle','quote-left','quote-right','random','refresh','reply','reply-all','retweet','road','rocket','rss','rss-square','search','search-minus','search-plus','share','share-square','share-square-o','shield','shopping-cart','sign-in','sign-out','signal','sitemap','smile-o','sort','sort-alpha-asc','sort-alpha-desc','sort-amount-asc','sort-amount-desc','sort-asc','sort-desc','sort-down','sort-numeric-asc','sort-numeric-desc','sort-up','spinner','square','square-o','star','star-half','star-half-empty','star-half-full','star-half-o','star-o','subscript','suitcase','sun-o','superscript','tablet','tachometer','tag','tags','tasks','terminal','thumb-tack','thumbs-down','thumbs-o-down','thumbs-o-up','thumbs-up','ticket','times','times-circle','times-circle-o','tint','toggle-down','toggle-left','toggle-right','toggle-up','trash-o','trophy','truck','umbrella','unlock','unlock-alt','unsorted','upload','user','users','video-camera','volume-down','volume-off','volume-up','warning','wheelchair','wrench','check-square','check-square-o','circle','circle-o','dot-circle-o','minus-square','minus-square-o','plus-square','plus-square-o','square','square-o','bitcoin','btc','cny','dollar','eur','euro','gbp','inr','jpy','krw','money','rmb','rouble','rub','ruble','rupee','try','turkish-lira','usd','won','align-center','align-justify','align-left','align-right','bold','chain','chain-broken','clipboard','columns','copy','cut','dedent','eraser','file','file-o','file-text','file-text-o','files-o','floppy-o','font','indent','italic','link','list','list-alt','list-ol','list-ul','outdent','paperclip','paste','repeat','rotate-left','rotate-right','save','scissors','strikethrough','table','text-height','text-width','th','th-large','th-list','underline','undo','unlink','angle-double-down','angle-double-left','angle-double-right','angle-double-up','angle-down','angle-left','angle-right','angle-up','arrow-circle-down','arrow-circle-left','arrow-circle-o-down','arrow-circle-o-left','arrow-circle-o-right','arrow-circle-o-up','arrow-circle-right','arrow-circle-up','arrow-down','arrow-left','arrow-right','arrow-up','arrows','arrows-alt','arrows-h','arrows-v','caret-down','caret-left','caret-right','caret-square-o-down','caret-square-o-left','caret-square-o-right','caret-square-o-up','caret-up','chevron-circle-down','chevron-circle-left','chevron-circle-right','chevron-circle-up','chevron-down','chevron-left','chevron-right','chevron-up','hand-o-down','hand-o-left','hand-o-right','hand-o-up','long-arrow-down','long-arrow-left','long-arrow-right','long-arrow-up','toggle-down','toggle-left','toggle-right','toggle-up',);

			$output = '<input type="hidden" name="'.$param_name.'" class="wpb_vc_param_value '.$param_name.' '.$type.' '.$class.'" value="'.$value.'" id="trace"/>
					<div class="icon-preview"><i class=" fa fa-'.$value.'"></i></div>';
			$output .='<input class="search" type="text" placeholder="Search" />';
			$output .='<div id="icon-dropdown" >';
			$output .= '<ul class="icon-list">';
			$n = 1;
			foreach($icons as $icon)
			{
				$selected = ($icon == $value) ? 'class="selected"' : '';
				$id = 'icon-'.$n;
				$output .= '<li '.$selected.' data-icon="'.$icon.'"><i class="icon fa fa-'.$icon.'"></i><label class="icon">'.$icon.'</label></li>';
				$n++;
			}
			$output .='</ul>';
			$output .='</div>';
			$output .= '<script type="text/javascript">
					jQuery(document).ready(function(){
						jQuery(".search").keyup(function(){

							// Retrieve the input field text and reset the count to zero
							var filter = jQuery(this).val(), count = 0;

							// Loop through the icon list
							jQuery(".icon-list li").each(function(){

								// If the list item does not contain the text phrase fade it out
								if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
									jQuery(this).fadeOut();
								} else {
									jQuery(this).show();
									count++;
								}
							});
						});
					});

					jQuery("#icon-dropdown li").click(function() {
						jQuery(this).attr("class","selected").siblings().removeAttr("class");
						var icon = jQuery(this).attr("data-icon");
						jQuery("#trace").val(icon);
						jQuery(".icon-preview").html("<i class=\'icon fa fa-"+icon+"\'></i>");
					});
			</script>';
			return $output;
		}
		/* Add icon box Component*/
		function icon_box_init()
		{
			if ( function_exists('vc_map'))
			{
				vc_map(
					array(
						"name"		=> __("Icon Box", "icon-box"),
						"base"		=> "icon-box",
						"custom_markup" => "icon-box",
						"icon"		=> "vc_icon_box",
						"class"	   => "icon_box",
						"category"  => __('Icon Box', "icon-box"),
						"description" => "Adds icon box with font-awesome icons",
						"controls" => "full",
						"show_settings_on_create" => true,
						"params" => array(
							// Icon Box Heading
							array(
								"type" => "textfield",
								"class" => "",
								"heading" => __("Title", "icon-box"),
								"param_name" => "title",
								"admin_label" => true,
								"value" => "This is an icon box.",
								"description" => __("Provide the title for this icon box.", "icon-box"),
							),
							// Add some description
							array(
								"type" => "textarea",
								"class" => "",
								"heading" => __("Description", "icon-box"),
								"param_name" => "desc",
								"value" => "Write a short description, that will describe the title or something informational and useful.",
								"description" => __("Provide the description for this icon box.", "icon-box")
							),
							// Add link to existing content or to another resource
							array(
								"type" => "vc_link",
								"class" => "",
								"heading" => __("Add Link", "icon-box"),
								"param_name" => "link",
								"value" => "",
								"description" => __("Provide the link that will be applied to this icon box.", "icon-box")
							),
							// Select link option - to box or with read more text
							array(
								"type" => "dropdown",
								"class" => "",
								"heading" => __("Apply link to:", "icon-box"),
								"param_name" => "read_more",
								"value" => array(
									"Complete Box" => "",
									"Box Title" => "title",
									"Display Read More" => "more",
								),
								"description" => __("Select whether to use color for icon or not.", "icon-box")
							),
							// Link to traditional read more
							array(
								"type" => "textfield",
								"class" => "",
								"heading" => __("Read More Text", "icon-box"),
								"param_name" => "read_text",
								"value" => "Read More",
								"description" => __("Customize the read more text.", "icon-box"),
								"dependency" => Array("element" => "read_more","value" => array("more")),
							),
							// Play with icon selector
							array(
								"type" => "dropdown",
								"class" => "",
								"heading" => __("Icon to display:", "icon-box"),
								"param_name" => "icon_type",
								"value" => array(
									"Font Awesome Icon" => "font-awesome",
									"Custom Image Icon" => "custom",
								),
								"description" => __("Select which icon you would like to use", "icon-box")
							),
							// Play with icon selector
							array(
								"type" => "icon",
								"class" => "",
								"heading" => __("Select Icon:", "icon-box"),
								"param_name" => "icon",
								"admin_label" => true,
								"value" => "android",
								"description" => __("Select the icon from the list.", "icon-box"),
								"dependency" => Array("element" => "icon_type","value" => array("font-awesome")),
							),
							// Play with icon selector
							array(
								"type" => "attach_image",
								"class" => "",
								"heading" => __("Upload Image Icon:", "icon-box"),
								"param_name" => "icon_img",
								"admin_label" => true,
								"value" => "",
								"description" => __("Upload the custom image icon.", "icon-box"),
								"dependency" => Array("element" => "icon_type","value" => array("custom")),
							),
							// Resize the icon
							array(
								"type" => "number",
								"class" => "",
								"heading" => __("Icon Size", "icon-box"),
								"param_name" => "size",
								"value" => 32,
								"min" => 16,
								"max" => 100,
								"suffix" => "px",
								"description" => __("Select the icon size.", "icon-box")
							),
							// Icon color - default or customize
							array(
								"type" => "dropdown",
								"class" => "",
								"heading" => __("Icon Color:", "icon-box"),
								"param_name" => "color",
								"value" => array(
									"Use Default" => "",
									"Custom Color" => "color",
								),
								"description" => __("Select whether to use color for icon or not.", "icon-box"),
								"dependency" => Array("element" => "icon_type","value" => array("font-awesome")),
							),
							// Customize Icon Color
							array(
								"type" => "colorpicker",
								"class" => "",
								"heading" => __("Select Icon Color:", "icon-box"),
								"param_name" => "icon_color",
								"value" => "#89BA49",
								"description" => __("Select the icon color.", "icon-box"),
								"dependency" => array(
												"element" => "color",
												"not_empty" => true,
											),
								"dependency" => Array("element" => "color","value" => array("color")),
							),
							// Would you like icon border?
							array(
								"type" => "dropdown",
								"class" => "",
								"heading" => __("Icon Border:", "icon-box"),
								"param_name" => "border",
								"value" => array(
									"No Border" => "",
									"Square Border" => "square",
									"Square Border With Background" => "square-solid",
									"Circle Border" => "circle",
									"Circle Border With Background" => "circle-solid",
								),
								"description" => __("Select if you want to display border around icon.", "icon-box")
							),
							// Give some spacing in icon and border
							array(
								"type" => "number",
								"class" => "",
								"heading" => __("Icon Border Spacing", "icon-box"),
								"param_name" => "padding",
								"value" => 5,
								"min" => 0,
								"max" => 20,
								"suffix" => "px",
								"description" => __("Select spacing between icon and border.", "icon-box"),
								"dependency" => array(
												"element" => "border",
												"not_empty" => true,
											),
							),
							// Resize the border
							array(
								"type" => "number",
								"class" => "",
								"heading" => __("Icon Border Width", "icon-box"),
								"param_name" => "width",
								"value" => "",
								"min" => 1,
								"max" => 10,
								"suffix" => "px",
								"description" => __("Select border width for icon.", "icon-box"),
								"dependency" => array(
												"element" => "border",
												"not_empty" => true,
											),
							),
							// Customize the border color
							array(
								"type" => "colorpicker",
								"class" => "",
								"heading" => __("Icon Border Color:", "icon-box"),
								"param_name" => "icon_border_color",
								"value" => "",
								"description" => __("Select the color for icon border.", "icon-box"),
								"dependency" => array(
												"element" => "border",
												"not_empty" => true,
											),
							),
							// Give some background to icon
							array(
								"type" => "colorpicker",
								"class" => "",
								"heading" => __("Icon Background Color:", "icon-box"),
								"param_name" => "icon_bg_color",
								"value" => "",
								"description" => __("Select the color for icon background.", "icon-box"),
								"dependency" => array(
												"element" => "border",
												"not_empty" => true,
											),
							),
							// Position the icon box
							array(
								"type" => "dropdown",
								"class" => "",
								"heading" => __("Box Style:", "icon-box"),
								"param_name" => "pos",
								"value" => array(
									"Icon at Left with heading" => "default",
									"Icon at Left" => "left",
									"Icon at Top" => "top",
								),
								"description" => __("Select icon position. Icon box style will be changed according to the icon position.", "icon-box")
							),
							// Lets animate the icon box
							array(
								"type" => "dropdown",
								"class" => "",
								"heading" => __("Animation","icon-box"),
								"param_name" => "animation",
								"value" => array(
									__("No", "icon-box") => '',
									__("Top to bottom", "icon-box") => "top-to-bottom",
									__("Bottom to top", "icon-box") => "bottom-to-top",
									__("Left to right", "icon-box") => "left-to-right",
									__("Right to left", "icon-box") => "right-to-left",
									__("Appear from center", "icon-box") => "appear"
								),
								"description" => __("Select animation type if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", "icon-box")
							),
							// Customize everything
							array(
								"type" => "textfield",
								"class" => "",
								"heading" => __("Extra Class", "icon-box"),
								"param_name" => "el_class",
								"value" => "",
								"description" => __("Add extra class name that will be applied to the icon box, and you can use this class for your customizations.", "icon-box"),
							),
						) // end params array
					) // end vc_map array
				); // end vc_map
			} // end function check 'vc_map'
		}// end function icon_box_init
	}//Class end
}
if(class_exists('VC_Icon_Box'))
{
	$VC_Icon_Box = new VC_Icon_Box;
}
// bsf core
$bsf_core_version_file = realpath(dirname(__FILE__).'/admin/bsf-core/version.yml');
if(is_file($bsf_core_version_file)) {
	global $bsf_core_version, $bsf_core_path;
	$bsf_core_dir = realpath(dirname(__FILE__).'/admin/bsf-core/');
	$version = file_get_contents($bsf_core_version_file);
	if(version_compare($version, $bsf_core_version, '>')) {
		$bsf_core_version = $version;
		$bsf_core_path = $bsf_core_dir;
	}
}
add_action('init', 'bsf_core_load', 999);
if(!function_exists('bsf_core_load')) {
	function bsf_core_load() {
		global $bsf_core_version, $bsf_core_path;
		if(is_file(realpath($bsf_core_path.'/index.php'))) {
			include_once realpath($bsf_core_path.'/index.php');
		}
	}
}
// BSF CORE commom functions
if(!function_exists('bsf_get_option')) {
	function bsf_get_option($request = false) {
		$bsf_options = get_option('bsf_options');
		if(!$request)
			return $bsf_options;
		else
			return (isset($bsf_options[$request])) ? $bsf_options[$request] : false;
	}
}
if(!function_exists('bsf_update_option')) {
	function bsf_update_option($request, $value) {
		$bsf_options = get_option('bsf_options');
		$bsf_options[$request] = $value;
		return update_option('bsf_options', $bsf_options);
	}
}
add_action( 'wp_ajax_bsf_dismiss_notice', 'bsf_dismiss_notice');
if(!function_exists('bsf_dismiss_notice')) {
	function bsf_dismiss_notice() {
		$notice = $_POST['notice'];
		$x = bsf_update_option($notice, true);
		echo ($x) ? true : false;
		die();
	}
}

add_action('admin_init', 'bsf_core_check',10);
if(!function_exists('bsf_core_check')) {
	function bsf_core_check() {
		if(!defined('BSF_CORE')) {
			if(!bsf_get_option('hide-bsf-core-notice'))
				add_action( 'admin_notices', 'bsf_core_admin_notice' );
		}
	}
}

if(!function_exists('bsf_core_admin_notice')) {
	function bsf_core_admin_notice() {
		?>
		<script type="text/javascript">
		(function($){
			$(document).ready(function(){
				$(document).on( "click", ".bsf-notice", function() {
					var bsf_notice_name = $(this).attr("data-bsf-notice");
				    $.ajax({
				        url: ajaxurl,
				        method: 'POST',
				        data: {
				            action: "bsf_dismiss_notice",
				            notice: bsf_notice_name
				        },
				        success: function(response) {
				        	console.log(response);
				        }
				    })
				})
			});
		})(jQuery);
		</script>
		<div class="bsf-notice update-nag notice is-dismissible" data-bsf-notice="hide-bsf-core-notice">
            <p><?php _e( 'License registration and extensions are not part of plugin/theme anymore. Kindly download and install "BSF CORE" plugin to manage your licenses and extensins.', 'bsf' ); ?></p>
        </div>
		<?php
	}
}

if(isset($_GET['hide-bsf-core-notice']) && $_GET['hide-bsf-core-notice'] === 're-enable') {
	$x = bsf_update_option('hide-bsf-core-notice', false);
}

// end of common functions