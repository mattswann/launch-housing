<?php
/*
Plugin Name: Return To Parent Widget
Description: Creates a return to parent page button widget
Author: Kylie Sy
Version: 1.0

Usage: This file goes to wp-content/plugins. Remember to activate
*/

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
add_action( 'widgets_init', function(){
     register_widget( 'Return_To_Parent_Widget' );
});	

/**
 * Adds My_Widget widget.
 */
class Return_To_Parent_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Return_To_Parent_Widget', // Base ID
			__('Return to Parent Widget', 'text_domain'), // Name
			array( 'description' => __( 'Creates a return to parent page button', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		global $post;
     	echo $args['before_widget'];
     	// If the post/page has a parent, display the return button
     	if($post->post_parent) {
		?>
		<section id="return">
	        <div class="return-title">RETURN TO</div>
	        <div class="return-btn">
	          <?php
	            $parent_link = get_permalink($post->post_parent); 
	            $parent_title = get_the_title($post->post_parent);
	          ?>
	          <a href="<?php echo $parent_link; ?>"><?php echo $parent_title; ?></a>
	        </div>
	    </section>
		<?php
		}
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		
	}

} // class Return_To_Parent_Widget