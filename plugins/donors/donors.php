<?php
/*
 Plugin Name: Donors
 Description: Show the list of donors
 Author: The One
 Version: 0.1
 License: GPL2
*/
	
	include_once 'class_donors.php';
	
	$donorObj = new donors();
	
	register_activation_hook( __FILE__, array( $donorObj, 'install' ) );
	
	register_deactivation_hook( __FILE__, array( $donorObj, 'uninstall' ) );
	
?>
