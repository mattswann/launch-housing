<?php
class donors {
	function donors(){
		add_action('admin_menu', array($this,'adminMenu'));
	}
	function install()
	{
		global  $wpdb;
		
		$sql ="CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."donors` (
			  `donor_id` int(11) NOT NULL AUTO_INCREMENT,
			  `donor_date` date NOT NULL,
			  `donor_title` varchar(20) NOT NULL,
			  `donor_fname` varchar(255) NOT NULL,
			  `donor_lname` varchar(255) NOT NULL,
			  `donor_position` varchar(255) NOT NULL,
			  `donor_organisation` varchar(255) NOT NULL,
			  `donor_email` varchar(255) NOT NULL,
			  `donor_day_phone` varchar(50) NOT NULL,
			  `donor_street` varchar(255) NOT NULL,
			  `donor_suburb` varchar(100) NOT NULL,
			  `donor_state` varchar(100) NOT NULL,
			  `donor_postcode` varchar(10) NOT NULL,
			  `donor_amount` float NOT NULL,
			  `donor_towards` varchar(255) NOT NULL,
			  `donor_campaign` varchar(255) NOT NULL,
			  `donor_temptation` varchar(255) NOT NULL,
			  `donor_donation_type` varchar(100) NOT NULL,
			  `donor_start_date` date NOT NULL,
			  `donor_end_date` date NOT NULL,
			  `donor_payment_interval` varchar(20) NOT NULL,
			  `donor_no_payments` float NOT NULL,
			  `donor_send_info` enum('yes','no') NOT NULL DEFAULT 'no',
			  `donor_approved` enum('1','0') NOT NULL,
			  `donor_transaction_id` varchar(100) NOT NULL,
			  PRIMARY KEY (`donor_id`)
			)";
		$wpdb->query($sql);
	}
	
	function uninstall()
	{
		global $wpdb;
		$sql ="drop table ".$wpdb->prefix."donors";
		$wpdb->query($sql);
	}
	
	function adminMenu(){
		add_menu_page( 'Donors List', 'Donors', 'manage_options', 'list_donors', array($this,'donors_list'),plugins_url().'/donors/List.png');
	}
	
	function donors_list() {
		global $wpdb;
		
		$sql = "SELECT * FROM ".$wpdb->prefix."donors";
		$rows = $wpdb->get_results($sql);
		?>
<div class="wrap">
		<h2><img src="<?php echo plugins_url();?>/donors/List-large.png" align="absmiddle" />Donors List</h2>
        <?php
        if(!isset($_GET['donor_id'])) {
        	?>
        	<table width="100%" class="wp-list-table widefat fixed posts">
        <?php
		if($rows) {
			?>
            <thead>
			<tr>
            	<th class="manage-column">Date</th>
                <th class="manage-column">Name</th>
                <th class="manage-column">Email</th>
                <th class="manage-column">Organisation</th>
                <th class="manage-column">Position</th>
                <th class="manage-column">Phone</th>
                <th class="manage-column">Address</th>
                <th class="manage-column">Amount</th>
                <th class="manage-column"></th>
            </tr>
            </thead>
			<?php
			$cnt = 0;
			foreach($rows as $row) {
				if($cnt %2 == 0)
					$class='class="alternate"';
				else
					$class = '';
				?>
				<tr <?php echo $class?>>
                	<td valign="top"><?php echo $row->donor_date;?></td>
					<td valign="top"><?php echo $row->donor_fname.' '.$row->donor_lname;?></td>
                    <td valign="top"><?php echo $row->donor_email;?></td>
                    <td valign="top"><?php echo $row->donor_organisation;?></td>
                    <td valign="top"><?php echo $row->donor_position;?></td>
                    <td valign="top"><?php echo $row->donor_day_phone;?></td>
                    <td valign="top"><?php echo $row->donor_street;?>,<br>
                    	<?php echo $row->donor_suburb;?>,<br>
                        <?php echo $row->donor_state;?>,<br>
                        <?php echo $row->donor_postcode;?>
                    </td>
                    <td valign="top"><?php echo $row->donor_amount;?></td>
                    <td valign="top"><a href="admin.php?page=list_donors&donor_id=<?php echo $row->donor_id;?>">Details</a></td>
				</tr>
				<?php
				$cnt++;
			}
		}
		else {
			?>
            <tr>
            	<td>No donors yet!</td>
            </tr>
            <?php
		}
		?>
        </table>
   	<?php
		}
		else {
			$sql_detail = "SELECT * FROM ".$wpdb->prefix."donors WHERE donor_id = '".$_GET['donor_id']."' ORDER BY donor_id DESC";
			$row_detail = $wpdb->get_row($sql_detail);
			?>
	<table width="100%" class="wp-list-table widefat fixed posts">
       	<thead>
           	<tr>
               	<th class="manage-column" colspan="2">Donor Details</th>
            </tr>
        </thead>
        <tbody>
        	<tr>
               	<th class="manage-column">Transaction ID</th>
               	<td><?php echo $row_detail->donor_transaction_id;?></td>
            </tr>
           	<tr>
               	<th class="manage-column">Name</th>
               	<td><?php echo $row_detail->donor_fname.' '.$row_detail->donor_lname;?></td>
            </tr>
            <tr>
               	<th class="manage-column">Email</th>
               	<td><?php echo $row_detail->donor_email;?></td>
            </tr>
            <tr>
               	<th class="manage-column">Organisation</th>
               	<td><?php echo $row_detail->donor_organisation;?></td>
            </tr>
            <tr>
               	<th class="manage-column">Position</th>
               	<td><?php echo $row_detail->donor_position;?></td>
            </tr>
            <tr>
               	<th class="manage-column">Phone</th>
               	<td><?php echo $row_detail->donor_day_phone;?></td>
            </tr>
            <tr>
               	<th class="manage-column">Street</th>
               	<td><?php echo $row_detail->donor_street;?></td>
            </tr>
            <tr>
               	<th class="manage-column">Suburb</th>
               	<td><?php echo $row_detail->donor_suburb;?></td>
            </tr>
            <tr>
               	<th class="manage-column">State</th>
               	<td><?php echo $row_detail->donor_state;?></td>
            </tr>
            <tr>
               	<th class="manage-column">Postcode</th>
               	<td><?php echo $row_detail->donor_postcode;?></td>
            </tr>
            <tr>
               	<th class="manage-column">Amount</th>
               	<td><?php echo $row_detail->donor_amount;?></td>
            </tr>
            <tr>
               	<th class="manage-column">Put my donation towards</th>
               	<td><?php echo $row_detail->donor_towards;?></td>
            </tr>
            <tr>
               	<th class="manage-column">Which campaign/appeal are you donating to?</th>
               	<td><?php echo $row_detail->donor_campaign;?></td>
            </tr>
            <tr>
               	<th class="manage-column">What prompted you to make this donation to Hanover?</th>
               	<td><?php echo $row_detail->donor_temptation;?></td>
            </tr>
            <tr>
                <th class="manage-column">Type of donation</th>
                <td><?php echo $row_detail->donor_donation_type;?></td>
            </tr>
            <tr>
                <th class="manage-column">Start Date</th>
                <td><?php echo $row_detail->donor_start_date;?></td>
            </tr>
            <tr>
                <th class="manage-column">End Date</th>
                <td><?php echo $row_detail->donor_end_date;?></td>
            </tr>
            <tr>
                <th class="manage-column">Payment Interval</th>
                <td><?php echo $row_detail->donor_payment_interval;?></td>
            </tr>
            <tr>
                <th class="manage-column">No. of payments</th>
                <td><?php echo $row_detail->donor_no_payments;?></td>
            </tr>
            <tr>
                <th class="manage-column">Send informational material about Hanover</th>
                <td><?php echo $row_detail->donor_send_info;?></td>
            </tr>
            <tr>
            	<th class="manage-column" colspan="2"><a href="admin.php?page=list_donors">&laquo; Back to donors list</a></th>
            </tr>
        </tbody>
    </table>
	<?php
		}
		?>
  </div>
        <?php
	}
}
?>