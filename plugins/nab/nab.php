<?php
/**
* Plugin Name: Nab Payments
* Plugin URI: http://mypluginuri.com/
* Description: A brief description about your plugin.
* Version: 1.0 or whatever version of the plugin (pretty self explanatory)
* Author: Plugin Author's Name
* Author URI: Author's website
* License: A "Slug" license name e.g. GPL12
*/
	register_activation_hook( __FILE__, 'install' );
	register_deactivation_hook( __FILE__, 'uninstall' );

add_action('admin_menu', 'nab_plugin_setup_menu');

function nab_plugin_setup_menu(){
        add_menu_page( 'NAB Payments', 'NAB Payments', 'manage_options', 'nab-plugin', 'test_init' );
}
function test_init(){
	echo '<h2>NAB Transact Direct Payment </h2>';
        echo "<form method='POST' action=''>
	<h3>Merchant ID:</h3>
<input type='text' name='EPS_MERCHANT' value='2MO0010'>
<h3>NAB Password</h3>
<input type='text' name='EPS_PASSWORD' value='Ouu2JXYA'>
<h3>Live and Test Hosted Payment Page Implementation </h3>
<input type='text' name='merchant_id' value='2MO0010'>

<h2>Live Direct Post and API Implementation </h2>
<h3>Merchant ID:</h3>
<input type='text' name='merchant_id' value='2MO0010'>
<h3>Live Transaction Password</h3>
<input type='text' name='EPS_PASSWORD' value='3zvIeYjh'>

<h2>Test Direct Post and API Implementation </h2>
<h3>Merchant ID:</h3>
<input type='text' name='merchant_id' value='2MO0010'>
<h3>Test Transaction Password</h3>
<input type='text' name='EPS_PASSWORD' value='Ouu2jJXYA'>
</form>";



$testNab = "https://transact.nab.com.au/test/xmlapi/payment";
$nabXml = "<NABTransactMessage>
    <MerchantInfo>
        <merchantID>2MO0010</merchantID>
        <password>3zvIeYjh37_d</password>
    </MerchantInfo>
    <RequestType>Payment</RequestType>
    <Payment>
        <TxnList count='1'>
            <Txn ID='1'>
                <txnType>0</txnType>
                <txnSource>23</txnSource>
                <amount>5000</amount>
                <currency>AUD</currency>
                <purchaseOrderNo>100000</purchaseOrderNo>
                <CreditCardInfo>
                    <cardNumber>4444333322221111</cardNumber>
                    <cvv>123</cvv>
                    <expiryDate>01/15</expiryDate>
                </CreditCardInfo>
            </Txn>
        </TxnList>
    </Payment>
</NABTransactMessage>";
function makeCurlCall($url, $method = "GET", $headers = null, $gets = null, $posts = null) {
    $ch = curl_init();
    if($gets != null)
    {
        $url.="?".(http_build_query($gets));
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    if($posts != null)
    {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $posts);
    }
    if($method == "POST") {
        curl_setopt($ch, CURLOPT_POST, true);
    } else if($method == "PUT") {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    } else if($method == "HEAD") {
        curl_setopt($ch, CURLOPT_NOBODY, true);
    }
    if($headers != null && is_array($headers))
    {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }
    $response = curl_exec($ch);
    $code = curl_getinfo($ch,CURLINFO_HTTP_CODE);

    curl_close($ch);
    return array(
        "code" => $code,
        "response" => $response
    );
}
$result = makeCurlCall(
    $testNab, /* CURL URL */
    "POST", /* CURL CALL METHOD */
    array( /* CURL HEADERS */
        "Content-Type: text/xml; charset=utf-8",
        "Accept: text/xml",
        "Pragma: no-cache",
        "Content_length: ".strlen(trim($nabXml))
    ),
    null, /* CURL GET PARAMETERS */
    $nabXml /* CURL POST PARAMETERS AS XML */
);

//print_r($result);
//echo "AAAAA";

}

function form_creation(){
	 //print_r($_REQUEST);
?>
<div class="container--nabForm-greySection">
<form style="float: left; width: 100%; background-color: #f8f8f8; padding-top: 45px;" action="https://transact.nab.com.au/test/directpost/authorise" id="contactForm" name="form" onsubmit="return validateForm(this);" method="post">
<!--<form method="post" onSubmit="return validateForm(this)" name="form" id="contactForm" action="http://www.launchhousing.org.au/donate-now/?pageid=process" style="float: left;
width: 100%;
background-color: #f8f8f8;">-->
<div class="form-colour">
<div class="container">
<div class="col-md-offset-3 col-md-4 form">
<p style="margin-bottom: 55px;
float: left;
font-weight: 400;
font-size: 18px;
color: #262626;
font-family: 'Raleway', Arial, sans-serif;font-weight: normal;">Personal Details</p>
<p><label for="title title">Title:: </label><span style="color: #f00;">*</span>
<select required="" id="title" name="title">
<option value="">Please select title</option>
<option value="Mr">Mr</option>
<option value="Mrs">Mrs</option>
<option value="Mr &amp; Mrs">Mr &amp; Mrs</option>
<option value="Ms">Ms</option>
<option value="Mids">Miss</option>
<option value="Dr">Dr</option>
</select></p>
<p><label for="fname">First Name: <span style="color: #f00;">*</span></label><input type="text" required="" value="" class="inputbox" size="40" id="fname" name="fname"></p>
<p><label for="last_name">Last Name: <span style="color: #f00;">*</span></label><input type="text" required="" value="" class="inputbox" size="40" id="last_name" name="last_name"></p>
<p><label for="position">Position:</label><input type="text" value="" class="inputbox" size="40" id="position" name="position"></p>
<p><label for="organisation">Organisation:</label><input type="text" value="" class="inputbox" size="40" id="organisation" name="organisation"></p>
<p><label for="han_email">Email address: </label><span style="color: #f00;">*</span><input type="email" required="" value="" class="inputbox" size="40" id="han_email" name="han_email"></p>
<p><label for="phone">Daytime Phone: </label>
<input type="text" value="" class="inputbox" size="40" id="phone" name="phone"></p>
<p><label for="street">Street: </label><span style="color: #f00;">*</span><input type="text" required="" value="" class="inputbox" size="40" id="street" name="street"></p>
<p><label for="city">Suburb: </label><span style="color: #f00;">*</span><input type="text" required="" value="" class="inputbox" size="40" id="city" name="city"></p>
<p><label for="state">State: </label><span style="color: #f00;">*</span>
<select required="" name="state" id="state">
<option value="">Please select region, state or province</option>
<option title="Australian Capital Territory" value="Australian Capital Territory">Australian Capital Territory</option>
<option title="New South Wales" value="New South Wales">New South Wales</option>
<option title="Northern Territory" value="Northern Territory">Northern Territory</option>
<option title="Queensland" value="Queensland">Queensland</option>
<option title="South Australia" value="South Australia">South Australia</option>
<option title="Tasmania" value="Tasmania">Tasmania</option>
<option title="Victoria" value="Victoria">Victoria</option>
<option title="Western Australia" value="Western Australia">Western Australia</option>
</select>
</p>
<p><label for="postcode">Postcode: </label><span style="color: #f00;">*</span><input type="text" required="" value="" class="inputbox" size="40" id="postcode" name="postcode"></p>
<p><input type="checkbox" style="width:5%;" checked="" value="yes" name="signup" class="roundedOne"> <span style="width: 96%;float: left;font-size: 12px;margin-top: -18px;line-height: 15px;">Please send me informational material about Launch Housing</span></p>


<img src="/captcha/securimage_show.php" id="captcha" style="display:block;" />
<input name="captcha" id="captcha" required title="Security Code" type="text" value="" class="mlfmrInpTxt requiredField" style="width:164px; margin-top:10px;" />
<p><img src="http://www.launchhousing.org.au/wp-content/themes/elise/captcha_p3l/captcha.php" id="captcha" style="float:left;" /><input type="text" name="captcha" id="captcha-form" autocomplete="off" required />
</p>

<p style="font-size: 12px;">Please check the box below to secure this page.</p>
<fieldset style="float:left;">
<div data-sitekey="6Ld1Nw4TAAAAAHbojD60ZL76eE8w_-t3FSM94-JG" class="g-recaptcha"><div style="width: 304px; height: 78px;"><div><iframe width="304" height="78" frameborder="0" src="https://www.google.com/recaptcha/api2/anchor?k=6Ld1Nw4TAAAAAHbojD60ZL76eE8w_-t3FSM94-JG&amp;co=aHR0cDovL3d3dy5sYXVuY2hob3VzaW5nLm9yZy5hdTo4MA..&amp;hl=en&amp;v=r20160817124709&amp;size=normal&amp;cb=n3qbuosriwuf" title="recaptcha widget" role="presentation" scrolling="no" name="undefined"></iframe></div><textarea style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; " class="g-recaptcha-response" name="g-recaptcha-response" id="g-recaptcha-response"></textarea></div></div>
<script src="https://www.google.com/recaptcha/api.js?hl=" type="text/javascript">
</script>
<p>What is <a target="_blank" href="https://support.google.com/recaptcha/?hl=en#6080904">reCAPTCHA</a>?</p>
</fieldset>
<p></p>
<p></p>

<div style="display:none;" id="directentrytable">
<p id="directentrytable"><strong>Direct Entry Details</strong></p>
<p id="directentrytable">BSB Number: <input type="text" value="123456" maxlength="6" size="7" name="bsb_number"></p>
<p id="directentrytable">Account Number:<input type="text" value="123456789" maxlength="9" size="10" name="account_number"></p>
<p id="directentrytable">Account Name:<input type="text" value="Test" maxlength="32" size="34" name="account_name"></p>
</div>
</div>
<div class="col-md-4 form">
<p style="font-size: 18px;color: #262626;font-family: 'Raleway', Arial, sans-serif;font-weight: normal; margin-bottom:47px;"><span style="float: left;">Donation Details</span>

	<!-- OLD LOGOS start -->
	<!-- <img style="float:right;     height: 25px; margin-top: 3px;" alt="Securepay" src="http://www.launchhousing.org.au/wp-content/themes/elise/img/securepay-logo.png" class="secure-pay-logo"> -->
	<!-- OLD LOGOS end-->


</p>
<input type="hidden" value="live" name="server">
<input type="hidden" value="AUD" name="currency">
<p>Amount:  <span style="color: #f00;">*</span><span style="float:right;" class="“currency_type”">AUD</span><input type="text" required="" style="font-size:15px;" value="0" size="8" class="amount" name="payment_amount"></p>

<p><label for="prompt">What prompted you to make this donation?</label>
<select style="width:100%;" onchange="checkOption('prompt')" id="prompt" name="prompt">
<option selected="" disabled="" value=""> -- select an option -- </option>
<option value="I received a Launch Housing appeal">I received the Launch Housing appeal</option>
<option value="I received the Newsletter">I received the Newsletter</option>
<option value="I saw Launch Housing in the media">I saw Launch Housing in the media</option>
<option value="I saw a Launch Housing staff member speak at an event">I saw a Launch Housing staff member speak at an event</option>
<option value="I saw Launch Housing Community Service Announcement">I saw Launch Housing's Community Service Announcement</option>
<option value="I was referred to Launch Housing by a friend">I was referred to Launch Housing by a friend</option>
<option value="I Googled homelessness">I Googled "homelessness"</option>
<option value="client">I met a Launch Housing client</option>
<option value="Launch Housing has supported my friends or family in the past">Launch Housing has supported my friends or family in the past</option>
<option value="I saw a Launch Housing advertisement">I saw a Launch Housing advertisement</option>
<option value="I attended the mooting competition">I attended the mooting competition</option>
<option value="I saw Launch Housing on social media">I saw Launch Housing on social media</option>
<option value="other">Other - please specify</option>
</select></p>
&nbsp;<textarea style="display: none; width: 250px" id="prompt_other" name="prompt_other" cols="20" rows="2"></textarea>
<p><label for="prompt">Type of donation <span style="color: #f00;">*</span></label> </p>
<p>
<span style="width: 50%;float: left;"><input type="radio" style="width: 10%;float: left;" checked="" onclick="showCreditCardTable(); showSubmitTable();" value="payment" name="request_type"> <label style="margin-left: 10px;">One time donation </label></span>
<span style="width: 50%;float: left;">
<input type="radio" style="width: 10%;float: left;" onclick="showRequestType('periodic'); showAddOptions('yes'); showCreditCardTable(); showSubmitTable();" value="periodic" name="request_type"><label style="margin-left: 10px;">Periodic donation</label>
</span>
</p>
<div style="display:none;" id="paymenttable">
<strong style="float: left; margin-bottom: 15px;">Payment Details</strong>
<p>Payment Type: <input type="hidden" value="0" name="payment_type"></p>
<p>Payment Reference / Purchase Order Number: <br>
<font size="2">
&nbsp;&nbsp;-Refunds/Reversals/Completes: must match original payments ref.<br>
&nbsp;&nbsp;-Direct Entry: 18 chars max.
</font>
<input type="text" value="referenceNumHere" maxlength="18" size="20" name="payment_reference"></p>
<p>Transaction ID:<br>
<font size="2">
&nbsp;&nbsp;-Refund/Reversal payments only.
</font>
<input type="text" value="" size="16" name="txnid"></p>
</div>
<div style="display:none" id="periodictable">
<strong>Periodic Details </strong>
<input type="hidden" value="add" name="action_type">
<input type="hidden" value="creditcard" name="periodic_payment_type">
<input type="hidden" value="3" name="periodic_type">
<p><span style="font-size: 13px;float: left; width:39%;"><label for="start_date">Start Date: (dd/mm/yyy)</label></span><span style="color: #f00;">*</span><input type="text" class="date-pick" id="start-date" name="start_date"></p>
<p><span style="float: left; width: 39%; font-size:13px;"><label for="payment_interval">Payment Interval:</label></span><span style="color: #f00;">*</span>
<select id="payment_interval" name="payment_interval">
<option selected="">-- Choose payment interval --</option>
<option value="1">Weekly</option>
<option value="2">Fortnightly</option>
<option value="3">Monthly</option>
<option value="4">Quarterly</option>
<option value="5">Half Yearly</option>
<option value="6">Annually</option>
</select>
</p>
<p><span style="float: left;width: 39%; font-size:13px;">Number of Payments:</span> <span style="color: #f00;">*</span>
<input type="text" maxlength="3" size="5" name="number_of_payments">
</p></div>
<div id="creditcardtable" style="display: block;">
<p><strong style="float: left; margin-bottom: 15px;">Credit Card Details</strong>
<label style="float: left;clear: both;" for="number_of_payments">Card No:<span style="color: #f00;float: left;">*</span></label>
<input type="text" maxlength="19" size="21" name="EPS_CARDNUMBER"><br>
<span style="font-size: 10px;float: left;width: 100%;clear: both;text-align: right;">Please note we only accept Visa and Mastercard</span></p>
<p><label for="card_CVV">CVV No: </label><span style="color: #f00;">*</span>
<input type="text" maxlength="4" size="5" name="EPS_CCV"></p>
<p>
<label style="width: 100%;float: left;" for="card_expiry_month"><span style="float: left;width: 40%;">Exp:</span>
<span>
<select style="width: 26% !important;" class="card_exp_year" value="Year" name="EPS_EXPIRYYEAR">
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
</select>
<select style="width: 25%;" class="card_exp_month" value="Month" name="EPS_EXPIRYMONTH">
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>
</span>
</label></p>
</div>
<div style="float: left; width: 100%; display: block;" id="submittable">
<!--<input type="hidden" name="EPS_MERCHANT" value="XYZ0010">
<input type="hidden" name="EPS_REFERENCEID" value="Test Reference">
<input type="hidden" name="EPS_AMOUNT" value="1.00">
<input type="hidden" name="EPS_PASSWORD" value="abcd1234">-->


<input type="hidden" name="EPS_MERCHANT" value="2MO0010">
<!--<input type="hidden" name="EPS_REFERENCEID" value="Test Reference">-->
<input type="hidden" name="EPS_REFERENCEID" value="My Reference">
<input type="hidden" name="EPS_AMOUNT" value="1.00">
<input type="hidden" name="EPS_PASSWORD" value="3zvIeYjh37_d">



<!--<input type="text" name="EPS_CARDNUMBER">-->

<!-- <input type="text" name="EPS_CARDTYPE" value="visa"> -->
<input type="hidden" name="EPS_TIMESTAMP" value="20160827073505">
<!--<input type="HIDDEN" name='3D_XID' value="20050714114257796000">

<input type="HIDDEN" name="EPS_3DSECURE" value="true">
-->
<input type="HIDDEN" name="EPS_FINGERPRINT" value="1967d6e2d0cde1850c86c01b83c13138ad3c3384">

<input type="hidden" name="EPS_RESULTURL" value="http://www.launchhousing.org.au/save_transaction.php">
<p><input type="submit" style="width:100%;" class="submit plainButton" name="submit" value="Submit Donation">
</p>
<p>Click the submit button ONCE only. <br>Your donation may take a few moments to process.</p>
</div>
<!--tr>
<td colspan="2" style="font-style:italic;">If you are donating using a Visa card and receive the error message 'Expired Card-Pick Up', please contact Andy Grant, Fundraising Specialist on 1800 720 660, he can securely process your payment over the phone. It is a known issue with Visa and we are working with them to fix it.</td>
</tr-->
</div>
</div>
</div>
</form>
</div>
<style>
input[type="text"]{
	border: 1px solid #ccc !important;
	float:right !important;
}
#contactForm p{ clear:both; }
input[type="email"]{
	border: 1px solid #ccc !important;
	float:right !important;
}
.secure-pay-logo{ display:none; }
.contact-page-form .wpcf7-form input, textarea, select {
  padding: 11px 10px;
  width: 65%;
  float: right;
}
</style>
<script type="text/javascript">
<!--
// Save typing document.getElementById repeatedly.
var id = function(elementid)
{
return document.getElementById(elementid);
};
// Hide Tables when page first loads.
function setTables()
{
id("paymenttable").style.display = 'none';
id("periodictable").style.display = block;
id("creditcardtable").style.display = 'block';
id("directentrytable").style.display = 'none';
//id("creditflag").style.display = 'none';
id("submittable").style.display = 'block';
id("periodicpaymenttype").style.display = 'none';
id("periodictype").style.display = 'none';
id("startdate").style.display = 'none';
id("paymentinterval").style.display = 'none';
id("numberofpayments").style.display = 'none';
id("visarecurring").style.display = 'none';
}
// Show request type table.
function showRequestType(selection)
{
switch(selection)
{
case "payment":
id("paymenttable").style.display = 'block';
id("periodictable").style.display = 'none';
id("creditcardtable").style.display = 'block';
id("directentrytable").style.display = 'none';
id("submittable").style.display = 'none';
break;
case "periodic":
id("periodictable").style.display = 'block';
id("paymenttable").style.display = 'none';
id("creditcardtable").style.display = 'block';
id("directentrytable").style.display = 'none';
id("submittable").style.display = 'block';
break;
case "echo":
id("paymenttable").style.display = 'none';
id("periodictable").style.display = 'none';
id("creditcardtable").style.display = 'block';
id("directentrytable").style.display = 'none';
break;
}
}
// Show payment type table.
function showPaymentType(selection)
{
if (selection == "0" || selection == "4" || selection == "6" || selection == "10" || selection == "11")
{
id("creditcardtable").style.display = 'block';
id("directentrytable").style.display = 'none';
}
else
{
id("creditcardtable").style.display = 'block';
id("directentrytable").style.display = 'none';
}
}
// Show the credit flag row.
function showCreditFlag(selection)
{
if (selection == "yes")
{
//id("creditflag").style.display = 'block';
}
else
{
id("creditflag").style.display = 'none';
}
}
// Show periodic Add table.
function showAddOptions(selection)
{
if (selection == "yes")
{
id("periodicpaymenttype").style.display = 'block';
id("periodictype").style.display = 'block';
id("startdate").style.display = 'block';
id("paymentinterval").style.display = 'block';
id("numberofpayments").style.display = 'block';
id("visarecurring").style.display = 'block';
id("submittable").style.display = 'none';
}
else
{
id("periodicpaymenttype").style.display = 'none';
id("periodictype").style.display = 'none';
id("startdate").style.display = 'none';
id("paymentinterval").style.display = 'none';
id("numberofpayments").style.display = 'none';
id("visarecurring").style.display = 'none';
id("creditcardtable").style.display = 'block';
id("directentrytable").style.display = 'none';
showCreditFlag('no');
}
}
// Show Submit table.
function showSubmitTable()
{
id("submittable").style.display = 'block';
}
// Show Credit Card table.
function showCreditCardTable()
{
id("creditcardtable").style.display = 'block';
id("periodictable").style.display = 'none';
}
//-->
</script>


<?php
}
add_shortcode('nab_payment_form', 'form_creation');?>
