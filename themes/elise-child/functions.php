<?php 

/* ------------------------------------------------------------ */
/*  Overwriting parent functions */
/* ------------------------------------------------------------ */

// add Page Options metabox to service post type (this is not automatically added to new custom post types)
require_once( get_stylesheet_directory() . '/eliseCore/ReduxCore/elise-meta-config.php' );

/* ------------------------------------------------------------ */
/*  Unhook Parent Theme Functions */
/* ------------------------------------------------------------ */

// Unhook default Thematic functions
function unhook_thematic_functions() {
    // use original priority number of the function to be removed

    // removes fa-search icon/nav-icons from being added automatically, and we aren't using woocommerce anyway
    remove_filter( 'wp_nav_menu_items', 'navigation_icons', 10 );
}
add_filter( 'init', 'unhook_thematic_functions' );

/* ------------------------------------------------------------ */
/*  Elise Breadcrumbs Override */
/* ------------------------------------------------------------ */
// modified from parent functions.php to add breadcrumbs for service post_type too

// if ( ! function_exists ( 'elise_breadcrumbs' ) ) { // removed because child function.php gets run first
function elise_breadcrumbs() {
    global $elise_options, $post;

    if ( $elise_options['opt-breadcrumbs-bar'] == 1 ) {
    ?>

    <?php if ( $elise_options['opt-breadcrumbs-style'] == 1 ) { ?>
    <div class="page-navigation">
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <?php } // breadcrumbs style - bar ?>

    <?php
    echo '<ol class="breadcrumb">';
    //echo '<li>You are here: </li>';
    echo '<li><i class="fa fa-home"></i> <a href="'. home_url() .'">'. __('Home', 'Elise') .'</a></li>';
    if ( is_singular('post') ) { 
        echo '<li>'; the_category('<span class="cat-sep"> / </span>'); echo '</li>';
        if ( is_single() ) { 
            echo '<li>'. __('Blog Post', 'Elise') .'';  echo '</li>';
        }
    } elseif ( is_singular('service') ) {
        echo '<li><a href="'. get_permalink(2235) . '">Housing & Support</a></li>';
        if ( has_term( "", 'service_category') ) {
            if ( !has_term( 80, 'service_category' ) ) {
                $post_id = $post->ID;
                $term = get_the_terms( $post_id, 'service_category' )[0];
                echo '<li><a href="'; echo get_bloginfo('url').'/service/'.$term->slug; echo '">'; echo $term->name; echo '</a></li>';
            }
        }
        echo '<li>'; echo get_the_title(); echo '</li>';
    } elseif ( is_home() && !is_front_page() ) { 
        echo '<li>'; single_post_title(); echo '</li>';

    // } elseif (function_exists('is_woocommerce') && is_woocommerce()) {
    // $args = array(
    //     'delimiter'  => '',
    //     'wrap_before'  => '',
    //     'wrap_after' => '',
    //     'before'   => '<li>',
    //     'after'   => '</li>',
    //     'home'    => ''
    // );
    // woocommerce_breadcrumb( $args );
    } elseif ( is_tax('region') || is_tax('client-type') ) {
        echo '<li><a href="'. get_permalink(2235) . '">Housing & Support</a></li>';
        echo'<li>'. __('Search Results', 'Elise') .''; echo'</li>';
    } elseif ( is_category() ) { 
        echo '<li>'; single_cat_title(); echo '</li>';
    } elseif ( is_singular('portfolio') ) { 
        echo '<li>'. __('Project details', 'Elise') .''; echo '</li>';
    } elseif ( is_page() && (!is_front_page()) ) {     
        if ( $post->post_parent ) {
            $parent_title = get_the_title($post->post_parent);
            $parent_link = get_permalink($post->post_parent);
            echo '<li><a href="'. $parent_link .'">' . $parent_title .'</a></li>';
        }
        echo '<li>'; the_title(); echo '</li>';
    } elseif ( is_tag() ) { 
        echo '<li>'.__('Tag: ', 'Elise').''; single_tag_title(); echo '</li>';
    } elseif ( is_day() ) { 
        echo'<li>'. __('Archive for ', 'Elise') .''; the_time('F jS, Y');     echo'</li>';
    } elseif ( is_month() ) { 
        echo'<li>'. __('Archive for ', 'Elise') .''; the_time('F, Y'); echo'</li>';
    } elseif ( is_year() ) { 
        echo'<li>'. __('Archive for ', 'Elise') .''; the_time('Y'); echo'</li>';
    } elseif ( is_author() ) { 
        echo'<li>'. __('Author Archives', 'Elise') .''; echo'</li>';
    } elseif ( isset($_GET['paged']) && !empty($_GET['paged']) ) { 
        echo '<li>'. __('Blog Archives', 'Elise') .''; echo'</li>';
    } elseif ( is_search() ) { 
        echo'<li>'. __('Search Results', 'Elise') .''; echo'</li>'; }

    echo '</ol>';
    ?>

    <?php if ( $elise_options['opt-breadcrumbs-style'] == 1 ) { ?>
    <ul class="buttons">
        
        <?php if ( is_singular(array('post', 'portfolio')) ) {
                if (comments_open( $post->ID )) { ?>
                    <li class="btn-comments" title="<?php _e('Go to Comments', 'Elise') ?>"><a href="#comments"><i class="fa fa-comments"></i><?php comments_number(' 0',' 1',' %'); ?></a></li>
        <?php }} ?>

        <?php if ( is_page_template('portfolio.php') && $elise_options['opt-portfolio-filtering'] == 1 ) { ?>
            <li class="btn-filtering"><a href="#"><?php _e('Filter Projects ', 'Elise') ?><i class="fa fa-caret-down"></i></a>
            </li>
        <?php } ?>

        <?php 
            if ( is_singular(array('post', 'portfolio')) ) {
                single_navigation(get_post_type());
            }
        ?>

        <?php if ($elise_options['opt-breadcrumbs-share'] == 1) { ?>
        <li class="btn-share"><a href="#" title="<?php _e('Share', 'Elise') ?>"><i class="fo icon-share-1"></i></a>
            <ul class="pn-share-buttons social-icons">
                  <li class="facebook">
                    <a href="#" class="facebook-share">
                      <i class="fa fa-facebook"></i>
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li class="twitter">
                    <a href="#" class="twitter-share">
                      <i class="fa fa-twitter"></i>
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li class="google-plus">
                    <a href="#" class="google-plus-share">
                      <i class="fa fa-google-plus"></i>
                      <i class="fa fa-google-plus"></i>
                    </a>
                  </li>
                  <li class="pinterest">
                    <a href="#" class="pinterest-share">
                      <i class="fa fa-pinterest"></i>
                      <i class="fa fa-pinterest"></i>
                    </a>
                  </li>
            </ul>
        </li>
        <?php } ?>
    </ul>
    <?php } // breadcrumbs style - bar ?>

    <?php if ($elise_options['opt-breadcrumbs-style'] == 1) { ?>
    </div>
    </div>
    </div>
    </div> <!-- page navigation end -->
    <?php } // breadcrumbs style - bar ?>

    <?php
    // }
    } //breadcrumbs bar endif
}
// }

/* ------------------------------------------------------------ */
/*  Nav Register */
/* ------------------------------------------------------------ */
function elise_child_register_menus(){
    register_nav_menus(
        array(
            'donate-nav'          => __('Donate Navigation', 'elise-child')
        )
    );
}

add_action('init', 'elise_child_register_menus');

/* ------------------------------------------------------------ */
/*  Loads Scripts */
/* ------------------------------------------------------------ */
function load_scripts() {
    global $post;
    // Fixes for media breadcrumbs
    wp_register_script( 'inthemedia', get_template_directory_uri() . '-child' . '/js/inthemedia.js', array('jquery'));
    wp_register_script( 'mediareleases', get_template_directory_uri() . '-child' . '/js/mediareleases.js', array('jquery'));

    wp_register_script( 'searchFilterPluginWrap', get_template_directory_uri() . '-child' . '/js/searchFilterPluginWrap.js', array('jquery'));

    if( is_tax() || is_singular('service') || is_category() ) {
        wp_enqueue_script('searchFilterPluginWrap');
    } elseif( is_page() || is_single() ) {
        switch($post->post_name) 
        {
            case 'launch-housing-in-the-media':
                wp_enqueue_script('inthemedia');
                break;
            case 'media-release-archive':
                wp_enqueue_script('mediareleases');
                break;
            case 'housing-support':
            case 'need-help':
                wp_enqueue_script('searchFilterPluginWrap');
                break;
        }
    } 
}

add_action('wp_enqueue_scripts', 'load_scripts');

/* ------------------------------------------------------------ */
/*  New Post Format */
/* ------------------------------------------------------------ */

function add_post_formats() {
    add_theme_support( 'post-formats', array( 'gallery', 'quote', 'video', 'aside', 'image', 'link', 'status' ) );
}
 
add_action( 'after_setup_theme', 'add_post_formats', 20 );


/* ------------------------------------------------------------ */
/*  Shortcodes and Visual Composer Integration */
/* ------------------------------------------------------------ */

/** 
 * example: add_shortcode( 'name_of_shortcode', 'query_output_function' )
 * shortcode: [name_of_shortcode]
*/

/** 
 * LH Post Grid
 * Displays posts in VC Composer Icon Box format    
*/
add_shortcode( 'post_grid_shortcode', 'register_post_grid_shortcode' );
function register_post_grid_shortcode( $atts ) {
    // extract changes parameters from array $atts to variables which we can use in our returned code
    extract(shortcode_atts(array(
                'padding'   => '5',
                'el_class'    => '',
                'taxonomy_term'  => '',
                'post_type' => '',
                'posts_per_page' => '100',
                // 'animation'  => 'top-to-bottom',
                ),$atts));

    if($taxonomy_term === '') {
        $post_type = 'story';
        $tax_query = '';
    } else {
        $post_type = 'service';
        $tax_query = array(
                            array(
                                'taxonomy' => 'service_category',
                                'field'    => 'term_id',
                                'terms'    => $taxonomy_term,
                            ),
                        ); 
    }

    // query posts to display
    $args = array(
                    // 'post_type' => 'service',
                    'post_type' => $post_type,
                    // 'category__in' => $post_category,
                    // 'parent' => 0,
                    'orderby' => 'post_id', 
                    'date' => 'ASC',
                    'posts_per_page' => '100',
                    'tax_query' => $tax_query
                );

    $query = new WP_Query( $args );
    $html = '';
    $separator = '<div class="wpb_column vc_column_container vc_col-md-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="elise_separator wpb_content_element elise_separator_align_center elise_el_width_wide"><div class="elise_sep_wrapper"><div class="elise_sep_container">';
    $separator .= '<span class="elise_sep_holder elise_sep_holder_l"><span class="elise_sep_line"></span></span>';
    $separator .= '<span class="elise_sep_holder elise_sep_holder_r"><span class="elise_sep_line"></span></span>';
    $separator .= '</div></div></div></div></div></div> <!-- elise-separator -->';
    $colnum = 0;
    if( $query->have_posts() ) :
        while ( $query->have_posts() ) : $query->the_post();
            // classes used from VC composer markup
            // 4-column container
            $html .= '<div class="vc_col-md-4 wpb_column vc_column_container"><div class="vc_column-inner"><div class="wpb_wrapper">';
            $html .= '<div class="smicon-component';
            $html .= '">';
            // link for whole box
            $html .= '<a class="smicon-box-link-2" href="'.get_permalink().'">';
            // icon position
            $html .= '<div class="smicon-box icon-top sm-icon-normal">';
            // icon
            $html .= '<div class="smicon-icon">';
            $html .= get_the_post_thumbnail();
            $html .= '</div> <!-- smicon-icon -->';
            $html .= '<div class="smicon-contents">';
            // header
            $html .= '<div class="smicon-header">';  
            $html .= '<h3 class="smicon-title">';
            $html .= get_the_title();
            $html .= '</h3>';
            $html .= '</div> <!-- smicon-header -->';
            //description
            $html .= '<div class="smicon-description">';
            $html .= get_the_excerpt().'&nbsp;&nbsp;<i class="fa fa-caret-right"></i>';
            $html .= '</div> <!-- smicon-description -->';
            $html .= '</div> <!-- smicon-contents -->';
            $html .= '</div> <!-- smicon-box -->';
            $html .= '</a></div> <!-- smicon-component -->';
            $html .= '</div></div></div>';
            $colnum ++;
            if( ($colnum%3) === 0 && ($colnum !== ($query->post_count)) ) :
                $html .= $separator;
            endif;
        endwhile;
    endif;
    wp_reset_query();
    return $html;
}

add_action( 'vc_before_init', 'post_grid_shortcode_integrateWithVC' );
// this allows the shortcode to be customisable in VC, as well as add it to the list of components
function post_grid_shortcode_integrateWithVC() {
   vc_map( array(
      "name" => __( "LH Post Grid", "my-text-domain" ),
      "base" => "post_grid_shortcode",
      "class" => "",
      "category" => __( "LH Custom", "my-text-domain"), // VC category
      // 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      // 'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
      "description" => __( "post grid for services post type", "my-text-domain"),
      "params" => array(
         array(
            "type" => "dropdown",
            "class" => "",
            "heading" => __( "Page Layout", "my-text-domain" ),
            "param_name" => "taxonomy_term",
            "value" => array(
                                "Choose one..." => '',
                                "Service Category Main" => 80,
                                "Assistance and Housing Advice" => 78,
                                "Specialist Advice" => 79,
                            ),
            "description" => __( "display posts of this page (if taxonomy: not children of this taxonomy)", "my-text-domain" )
         )
      )
   ) );
}
/* LH Post Grid end */

/** 
 * Image Box
 * Displays posts with title on top, an image and description
*/

add_shortcode( 'image_box_shortcode', 'register_image_box_shortcode' );
function register_image_box_shortcode( $atts ) {
    // extract changes parameters from array $atts to variables which we can use in our returned code
    extract(shortcode_atts(array(
                'padding'   => '5',
                'el_class'    => '',
                'title' => '',
                'title_position' => '',
                'title_size' => '',
                'description' => '',
                'image_url' => '',
                'link_url' => '#' 
                ),$atts));
    $html = '';
    $prefix = '<div class="image-box">';
    $html .= '<h2>'.$title.'</h2>'; 
    $html .= '<img class="image-box-img" src="'.wp_get_attachment_image_src($image_url, 'full')[0].'">';
    $html .= '<div class="wrap blog-content">';
    $html .= '<p>'.$description.'</p>';
    $html .= '<a href="'.$link_url.'" class="more-link">Website</a>';
    $suffix = '</div></div>' ;
    return $prefix.$html.$suffix;
}

add_action( 'vc_before_init', 'image_box_shortcode_integrateWithVC' );
// this allows the shortcode to be customisable in VC, as well as add it to the list of components
function image_box_shortcode_integrateWithVC() {
   vc_map( array(
      "name" => __( "LH Image Box", "my-text-domain" ),
      "base" => "image_box_shortcode",
      "class" => "",
      "category" => __( "LH Custom", "my-text-domain"), // VC category
      // 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      // 'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
      "description" => __( "image box with header on top of image", "my-text-domain"),
      "params" => array(
         // array(
         //    "type" => "dropdown",
         //    "class" => "",
         //    "heading" => __( "Title Position", "my-text-domain" ),
         //    "param_name" => "title_position",
         //    "value" => array(
         //                        "Choose one..." => '',
         //                        "Top" => 'top',
         //                        "After Image" => 'after-image'
         //                    )
         // ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => __( "Title", "my-text-domain" ),
            "param_name" => "title"
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => __( "Image", "my-text-domain" ),
            "param_name" => "image_url"
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => __( "Description", "my-text-domain" ),
            "param_name" => "description"
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => __( "Link Url", "my-text-domain" ),
            "param_name" => "link_url"
         )
      )
   ) );
}
/* Image Description Box end */
?>