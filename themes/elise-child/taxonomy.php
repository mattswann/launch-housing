<?php 
// for this page, classes from VC Composer markup structure was used

wp_reset_postdata();
get_header(); ?>
     
  <div class="content section">
    <div class="container">
      <div class="row">
        <section class="col-md-12">
          <!-- search bar -->
          <?php get_template_part( 'template-parts/content', 'searchbar' );  ?>
          <!-- search bar end -->
          
          <!-- services post grid -->
          <div class="vc_row wpb_row vc_row_stcontent vc_bg_standard vc_row-fluid">
              <!-- vc separator (elise) -->
              <div class="wpb_column vc_column_container vc_col-md-12">
                <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                    <div class="elise_separator wpb_content_element elise_separator_align_center elise_el_width_wide">
                      <div class="elise_sep_wrapper">
                        <div class="elise_sep_container">
                          <span class="elise_sep_holder elise_sep_holder_l"><span class="elise_sep_line"></span></span>
                          <span class="elise_sep_holder elise_sep_holder_r"><span class="elise_sep_line"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- vc separator (elise) end -->
              <?php $colnum = 0; if(have_posts()) : while(have_posts()) : the_post(); ?>
              <div class="vc_col-md-4 wpb_column vc_column_container">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper">
                    <!-- vc icon box -->
                    <div class="smicon-component">
                      <!-- link for whole box -->
                      <a class="smicon-box-link-2" target="_blank" href="<?php the_permalink(); ?>">
                        <!-- icon position -->
                        <div class="smicon-box icon-top sm-icon-normal">
                          <div class="smicon-icon">
                            <?php the_post_thumbnail(); ?>
                          </div> <!-- smicon-icon -->
                          <!-- header -->
                          <div class="smicon-header">
                            <h3 class="smicon-title">
                              <?php the_title(); ?>
                            </h3>
                          </div> <!-- smicon-header -->
                          <!-- description -->
                          <div class="smicon-description">
                            <?php echo get_the_excerpt(); ?>&nbsp;&nbsp;<i class="fa fa-caret-right"></i>
                          </div> <!-- smicon-description -->
                        </div> <!-- smicon-box -->
                      </a>
                    </div> <!-- smicon-component -->
                  </div>
                </div>
              </div>
              <?php $colnum++; if( ($colnum%3) === 0 && ($colnum !== count($posts)) ) : ?>
                <!-- vc separator (elise) -->
                <div class="wpb_column vc_column_container vc_col-md-12">
                  <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                      <div class="elise_separator wpb_content_element elise_separator_align_center elise_el_width_wide">
                        <div class="elise_sep_wrapper">
                          <div class="elise_sep_container">
                            <span class="elise_sep_holder elise_sep_holder_l"><span class="elise_sep_line"></span></span>
                            <span class="elise_sep_holder elise_sep_holder_r"><span class="elise_sep_line"></span></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- vc separator (elise) end -->
              <?php endif; ?>
              <?php endwhile; 
              else : ?>
                <div class="vc_col-md-12 wpb_column vc_column_container">
                  <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                      <?php get_template_part( 'template-parts/content', 'none' ); ?>
                    </div>
                  </div>
                </div>
              <?php endif; ?>    
          </div>
          <!-- services post grid end -->
    
        </section>
      </div>
    </div>
  </div>

<?php get_footer(); ?>