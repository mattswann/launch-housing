<?php

// INCLUDE THIS BEFORE you load your ReduxFramework object config file.


// You may replace $redux_opt_name with a string if you wish. If you do so, change loader.php
// as well as all the instances below.
$redux_opt_name = "elise_options";

if ( !function_exists( "redux_add_metaboxes" ) ):
    function redux_add_metaboxes($metaboxes) {




    // Post Formats
    // Post Format Video
    $boxSections = array();
    $boxSections[] = array(
        'title' => __('Add Video', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon_class' => 'icon-large',
        'icon' => 'el-icon-video',
        'fields' => array(

            array(
                'id'       => 'opt-format-video-type',
                'type'     => 'select',
                'title'    => __('Video Type', 'Elise'), 
                'select2'  => array( 'allowClear' => false, 'minimumResultsForSearch' => -1),
                // 'subtitle' => __('', 'Elise'),
                //'desc'     => __('This is the description field, again good for additional info.', 'Elise'),
                // Must provide key => value pairs for select options
                'options'  => array(
                    '1' => 'Youtube/Vimeo',
                    '2' => 'Embed Code'
                ),
                'default'  => '1',
            ),

            array(
                'id'        => 'opt-format-video-url',
                'type'      => 'text',
                'required'      => array('opt-format-video-type', "=", 1),
                'title'     => __('URL to Video', 'Elise'),
                'subtitle'  => __('Link to video - Youtube or Vimeo only', 'Elise'),
                'validate'  => 'url',
                'desc'      => __('e.g. <i>https://www.youtube.com/watch?v=I8lttV53XOo</i> or <i>http://vimeo.com/99124075</i>', 'Elise'),
            ),

            array(
                'id'        => 'opt-format-video-embed',
                'type'      => 'textarea',
                'required'      => array('opt-format-video-type', "=", 2),
                'title'     => __('Video Embed', 'Elise'),
                'subtitle'  => __('&lt;iframe&gt; &lt;/iframe&gt; embed here.', 'Elise'),
                'desc'      => __('&lt;iframe&gt; &lt;/iframe&gt; embed here. Sizes will automatically adjusted.', 'Elise'),
                'validate'  => 'html',
            ),
        )
    );

    $boxSections[] = array(
        //'title' => __('General Settings', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        //'icon' => 'el-icon-home',
        'type' => 'divide',
        'fields' => array(  

            array(
                'id'   =>'divider_1',
                //'desc' => __('This is the description field, again good for additional info.', 'Elise'),
                'type' => 'divide'
            )
        )
    );    
  
    $metaboxes[] = array(
        'id' => 'post-format-video',
        'title' => __('Video Post Format', 'Elise'),
        'post_types' => array('post'),
        //'page_template' => array('page-test.php'),
        'post_format' => array('video'),
        'position' => 'normal', // normal, advanced, side
        'priority' => 'high', // high, core, default, low
        'sections' => $boxSections
    );




    // Post Format Audio
    $boxSections = array();
    $boxSections[] = array(
        'title' => __('Add Audio', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon_class' => 'icon-large',
        'icon' => 'el-icon-music',
        'fields' => array(

            array(
                'id'       => 'opt-format-audio-type',
                'type'     => 'select',
                'title'    => __('Audio Type', 'Elise'), 
                'select2'  => array( 'allowClear' => false, 'minimumResultsForSearch' => -1),
                // 'subtitle' => __('', 'Elise'),
                //'desc'     => __('This is the description field, again good for additional info.', 'Elise'),
                // Must provide key => value pairs for select options
                'options'  => array(
                    '1' => 'External Embed (SoundCloud etc.)',
                    '2' => 'Custom Audio'
                ),
                'default'  => '1',
            ),

            array(
                'id'        => 'opt-format-audio-embed',
                'type'      => 'textarea',
                'required'      => array('opt-format-audio-type', "=", 1),
                'title'     => __('Audio Embed', 'Elise'),
                'subtitle'  => __('&lt;iframe&gt; &lt;/iframe&gt; embed here.', 'Elise'),
                'desc'      => __('&lt;iframe&gt; &lt;/iframe&gt; embed here. Sizes will automatically adjusted.', 'Elise'),
                'validate'  => 'html',
            ),

            array(
                'id'        => 'opt-format-audio-mp3',
                'type'      => 'media',
                'url'       => false,
                'required'  => array('opt-format-audio-type', "=", 2),
                'title'     => __('MP3 Audio File - required', 'Elise'),
                'subtitle'  => __('Audio file with .mp3 extension.', 'Elise'),
                'mode'      => 'audio', // toDo
            ),

            array(
                'id'        => 'opt-format-audio-oga',
                'type'      => 'media',
                'url'       => false,
                'required'  => array('opt-format-audio-type', "=", 2),
                'title'     => __('OGA Audio File', 'Elise'),
                'subtitle'  => __('Audio file with .ogg extension.', 'Elise'),
                'mode'      => 'audio', // toDo
            ),

            array(
                'id'        => 'opt-format-audio-author',
                'type'      => 'text',
                'title'     => __('Song Author', 'Elise'),
                'required'  => array('opt-format-audio-type', "=", 2),
                //'subtitle'  => __('Name of the Quote Author', 'Elise'),
                //'desc'      => __('&lt;iframe&gt; &lt;/iframe&gt; embed here. Sizes will automatically adjusted.', 'Elise'),
            ),

            array(
                'id'        => 'opt-format-audio-title',
                'type'      => 'text',
                'title'     => __('Song Title', 'Elise'),
                'required'  => array('opt-format-audio-type', "=", 2),
                //'subtitle'  => __('Name of the Quote Author', 'Elise'),
                //'desc'      => __('&lt;iframe&gt; &lt;/iframe&gt; embed here. Sizes will automatically adjusted.', 'Elise'),
            ),


        )
    );

    $boxSections[] = array(
        //'title' => __('General Settings', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        //'icon' => 'el-icon-home',
        'type' => 'divide',
        'fields' => array(  

            array(
                'id'   =>'divider_1',
                //'desc' => __('This is the description field, again good for additional info.', 'Elise'),
                'type' => 'divide'
            )
        )
    );   
  
    $metaboxes[] = array(
        'id' => 'post-format-audio',
        'title' => __('Audio Post Format', 'Elise'),
        'post_types' => array('post'),
        //'page_template' => array('page-test.php'),
        'post_format' => array('audio'),
        'position' => 'normal', // normal, advanced, side
        'priority' => 'high', // high, core, default, low
        'sections' => $boxSections
    );



    // Post Format Quote
    $boxSections = array();
    $boxSections[] = array(
        'title' => __('Quote', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon_class' => 'icon-large',
        'icon' => 'el-icon-quotes',
        'fields' => array(

            array(
                'id'        => 'opt-format-quote-author',
                'type'      => 'text',
                'title'     => __('Quote Author', 'Elise'),
                'subtitle'  => __('Name of the Quote Author', 'Elise'),
                //'desc'      => __('&lt;iframe&gt; &lt;/iframe&gt; embed here. Sizes will automatically adjusted.', 'Elise'),
            ),
        )
    );

    $boxSections[] = array(
        //'title' => __('General Settings', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        //'icon' => 'el-icon-home',
        'type' => 'divide',
        'fields' => array(  

            array(
                'id'   =>'divider_1',
                //'desc' => __('This is the description field, again good for additional info.', 'Elise'),
                'type' => 'divide'
            )
        )
    );  
  
    $metaboxes[] = array(
        'id' => 'post-format-quote',
        'title' => __('Quote Post Format', 'Elise'),
        'post_types' => array('post'),
        //'page_template' => array('page-test.php'),
        'post_format' => array('quote'),
        'position' => 'normal', // normal, advanced, side
        'priority' => 'high', // high, core, default, low
        'sections' => $boxSections
    );


    // Post Format Link
    $boxSections = array();
    $boxSections[] = array(
        'title' => __('Link Settings', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon_class' => 'icon-large',
        'icon' => 'el-icon-link',
        'fields' => array(

            array(
                'id'        => 'opt-format-link-url',
                'type'      => 'text',
                'title'     => __('URL', 'Elise'),
                'subtitle'  => __('Url to website', 'Elise'),
                'validate'  => 'url',
                //'desc'      => __('&lt;iframe&gt; &lt;/iframe&gt; embed here. Sizes will automatically adjusted.', 'Elise'),
            ),
        )
    );

    $boxSections[] = array(
        //'title' => __('General Settings', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        //'icon' => 'el-icon-home',
        'type' => 'divide',
        'fields' => array(  

            array(
                'id'   =>'divider_1',
                //'desc' => __('This is the description field, again good for additional info.', 'Elise'),
                'type' => 'divide'
            )
        )
    );  
  
    $metaboxes[] = array(
        'id' => 'post-format-link',
        'title' => __('Link Post Format', 'Elise'),
        'post_types' => array('post'),
        //'page_template' => array('page-test.php'),
        'post_format' => array('link'),
        'position' => 'normal', // normal, advanced, side
        'priority' => 'high', // high, core, default, low
        'sections' => $boxSections
    );


    // Post Format Gallery
    $boxSections = array();
    $boxSections[] = array(
        'title' => __('Add Gallery', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon_class' => 'icon-large',
        'icon' => 'el-icon-picture',
        'fields' => array(

            array(
                'id' => 'opt-meta-post-gallery',
                'type' => 'gallery',
                'title' => __('Add/Edit Post Gallery', 'so-panels'),
                'subtitle' => __('Create a new Gallery by selecting existing or uploading new images using the WordPress native uploader', 'so-panels'),
                'desc' => __('This is the description field, again good for additional info.', 'Elise'),
            ),
        )
    );

    $boxSections[] = array(
        //'title' => __('General Settings', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        //'icon' => 'el-icon-home',
        'type' => 'divide',
        'fields' => array(  

            array(
                'id'   =>'divider_1',
                //'desc' => __('This is the description field, again good for additional info.', 'Elise'),
                'type' => 'divide'
            )
        )
    );  
  
    $metaboxes[] = array(
        'id' => 'post-format-gallery',
        'title' => __('Gallery Post Format', 'Elise'),
        'post_types' => array('post'),
        //'page_template' => array('page-test.php'),
        'post_format' => array('gallery'),
        'position' => 'normal', // normal, advanced, side
        'priority' => 'high', // high, core, default, low
        'sections' => $boxSections
    );













    // Portfolio metaboxes 


    $boxSections = array();

    $boxSections[] = array(
        'title' => __('Project Gallery', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        // 'icon' => 'el-icon-home',
        'fields' => array(  

            array(
                'id' => 'opt-meta-project-gallery',
                'type' => 'gallery',
                'title' => __('Add/Edit Project Gallery', 'so-panels'),
                'subtitle' => __('Create a new Gallery by selecting existing or uploading new images using the WordPress native uploader', 'so-panels'),
                'desc' => __('This is the description field, again good for additional info.', 'Elise'),
            ),
        )
    );

    $boxSections[] = array(
        'title' => __('Project Info', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        // 'icon' => 'el-icon-home',
        'fields' => array(  

            array(
                'id'        => 'opt-meta-project-year',
                'type'      => 'text',
                'title'     => __('Project Year', 'Elise'),
                'subtitle'  => __('Write here cutom page title.', 'Elise'),
            ),

            array(
                'id'        => 'opt-meta-project-client',
                'type'      => 'text',
                'title'     => __('Client', 'Elise'),
                'subtitle'  => __('Write here cutom page title.', 'Elise'),
            ),

            array(
                'id'        => 'opt-meta-project-url',
                'type'      => 'text',
                'title'     => __('Project URL', 'Elise'),
                'subtitle'  => __('Url to Project, ex. http://project.com', 'Elise'),
                'validate'  => 'url'
            ),
        )
    );

    $metaboxes[] = array(
        'id' => 'portfolio-metaboxes',
        'title' => __('Portfolio Options', 'Elise'),
        'post_types' => array('portfolio'),
        //'page_template' => array('page-test.php'),
        //'post_format' => array('image'),
        'position' => 'normal', // normal, advanced, side
        'priority' => 'high', // high, core, default, low
        //'sidebar' => false, // enable/disable the sidebar in the normal/advanced positions
        'sections' => $boxSections
    );
    $boxSections = array();




    // Portfolio Page filtering metaboxes 
    $boxSections = array();

    $boxSections[] = array(
        //'title' => __('Portfolio Categories', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        // 'icon' => 'el-icon-home',
        'fields' => array(  
            array(
                'id'        => 'opt-portfolio-categories',
                'type'      => 'select',
                'data'      => 'terms',
                'args'      => array('taxonomies'=>'portfolio_category'),
                'multi'     => true,
                'placeholder' => __('Select category/categories', 'Elise'),
                'title'     => __('Select Portfolio Categories', 'Elise'),
                'subtitle'  => __('Specific categories which will show up on this page.', 'Elise'),
                'desc'      => __('For all categories left this field empty', 'Elise'),
            ),

            array(
                'id'        => 'opt-portfolio-layout',
                'type'      => 'image_select',
                'compiler'  => true,
                'title'     => __('Portfolio Layout', 'Elise'),
                'subtitle'  => __('This Page portfolio layout.', 'Elise'),
                'options'   => array(
                    '1' => array('alt' => '1 Column', 'img' => ReduxFramework::$_url . 'assets/img/portfolio-1col.jpg'),
                    '2' => array('alt' => '2 Columns',  'img' => ReduxFramework::$_url . 'assets/img/portfolio-2col.jpg'),
                    '3' => array('alt' => '3 Columns',  'img' => ReduxFramework::$_url . 'assets/img/portfolio-3col.jpg'),
                    '4' => array('alt' => '4 Columns', 'img' => ReduxFramework::$_url . 'assets/img/portfolio-4col.jpg'),
                    '5' => array('alt' => 'Masonry', 'img' => ReduxFramework::$_url . 'assets/img/portfolio-masonry.jpg')
                ),
            ),

            array(
                'id'        => 'opt-portfolio-fullwidth',
                'type'      => 'switch',
                'title'     => __('Full Width Portfolio', 'Elise'),
                'subtitle'  => __('Enable/Disable Full Width portfolio layout.', 'Elise'),
            ),

            array(
                'id'        => 'opt-portfolio-filtering',
                'type'      => 'switch',
                'title'     => __('Portfolio Filtering', 'Elise'),
                'subtitle'  => __('Enable/Disable portfolio filtering.', 'Elise'),
            ),

            array(
                'id'        => 'opt-portfolio-pagination',
                'type'      => 'switch',
                'title'     => __('Portfolio Pagination', 'Elise'),
                'subtitle'  => __('Paginate Projects on Portfolio page.', 'Elise'),
            ),

            array(
                'id'            => 'opt-portfolio-pper-page',
                'type'          => 'slider',
                'required'      => array('opt-portfolio-pagination', "=", 1),
                'title'         => __('Projects per Page', 'Elise'),
                'subtitle'      => __('Number of projects that will show on Page.', 'Elise'),
                'desc'          => __('Navbar height. Min: 1, max: 20, default value: 5', 'Elise'),
                'min'           => 1,
                'step'          => 1,
                'max'           => 20,
                'display_value' => 'text'
            ),

            array(
                'id'        => 'opt-portfolio-style',
                'type'      => 'image_select',
                'compiler'  => true,
                'title'     => __('Portfolio Items Style', 'Elise'),
                'subtitle'  => __('Portfolio items Layput.', 'Elise'),
                'options'   => array(
                    '1' => array('alt' => 'Portfolio Item Overlay', 'img' => ReduxFramework::$_url . 'assets/img/portfolio-style-overlay.jpg'),
                    '2' => array('alt' => 'Portfolio Item Bottom',  'img' => ReduxFramework::$_url . 'assets/img/portfolio-style-bottom.jpg')
                ),
            ),

            array(
                'id'        => 'opt-porfolio-item-categories',
                'type'      => 'switch',
                'required'      => array('opt-portfolio-style', "=", 1),
                'title'     => __('Categories Name above the Project Title', 'Elise'),
                'subtitle'  => __('Enable/Disale portfolio categories above portfolio title.', 'Elise'),
            ),

            array(
                'id'        => 'opt-quick-view',
                'type'      => 'switch',
                'title'     => __('Quick View Button', 'Elise'),
                'subtitle'  => __('Enable/Disable quick view button on portfolio item.', 'Elise'),
            ),

            array(
                'id'            => 'opt-portfolio-items-gap',
                'type'          => 'slider',
                'title'         => __('Portfolio Items Gap', 'Elise'),                      
                'subtitle'      => __('Set gap between portfolio items', 'Elise'),
                'desc'          => __('Navbar height. Min: 0, max: 30, default value: 30', 'Elise'),
                'min'           => 0,
                'step'          => 2,
                'max'           => 30,
                'display_value' => 'text'
            ),
        )
    );

    $metaboxes[] = array(
        'id' => 'portfolio-categories',
        'title' => __('Portfolio Settings', 'Elise'),
         'post_types' => array('page'),
        'page_template' => array('portfolio.php'),
        //'post_format' => array('image'),
        'position' => 'side', // normal, advanced, side
        'priority' => 'low', // high, core, default, low
        'sidebar' => true, // enable/disable the sidebar in the normal/advanced positions
        'sections' => $boxSections
    );
    $boxSections = array();




    // Portfolio Page filtering metaboxes 
    $boxSections = array();

    $boxSections[] = array(
        //'title' => __('Portfolio Categories', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        // 'icon' => 'el-icon-home',
        'fields' => array(  

            array(
                'id'        => 'opt-side-navigation-position',
                'type'      => 'button_set',
                'title'     => __('Side Navigation Position', 'Elise'),
                'subtitle'  => __('Select side navigation position', 'Elise'),                   
                //Must provide key => value pairs for radio options
                'options'   => array(
                    '1' => __('Left', 'Elise'), 
                    '2' => __('Right', 'Elise')
                ), 
                'default'   => '1'
            ),

            array(
                'id'        => 'opt-side-navigation-title',
                'type'      => 'text',
                'title'     => __('Navigation Title', 'Elise'),
                'subtitle'  => __('Side Nav Title, will show up above the side nav.', 'Elise'),
                //'desc'      => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
                'default'   => __('Pages', 'Elise'),
            ),
        )
    );

    $metaboxes[] = array(
        'id' => 'side-navigation',
        'title' => __('Side Navigation', 'Elise'),
         'post_types' => array('page'),
        'page_template' => array('template-side-navigation.php'),
        //'post_format' => array('image'),
        'position' => 'side', // normal, advanced, side
        'priority' => 'low', // high, core, default, low
        'sidebar' => true, // enable/disable the sidebar in the normal/advanced positions
        'sections' => $boxSections
    );
    $boxSections = array();







    // Page Metaboxes
    $boxSections = array();
    
    $boxSections[] = array(
        'title' => __('Page Title', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon_class' => 'icon-large',
        'icon' => 'el-icon-fontsize',
        'fields' => array(

                array(
                    'id'        => 'opt-title-bar',
                    'type'      => 'switch',
                    'title'     => __('Title Bar', 'Elise'),
                    // 'subtitle'  => __('Showing title bar on every page.', 'Elise'),
                ),

                array(
                    'id'        => 'opt-title-bar-centered',
                    'type'      => 'button_set',
                    'title'     => __('Page Title Position', 'Elise'),
                    // 'subtitle'  => __('Position of page title on title bar.', 'Elise'),
                    'required'  => array('opt-title-bar', "=", 1),
                    //Must provide key => value pairs for radio options
                    'options'   => array(
                        '1' => 'Left', 
                        '2' => 'Center'
                    ), 
                ),

                array(
                    'id'        => 'opt-page-subtitle',
                    'type'      => 'text',
                    'title'     => __('Page Subtitle', 'Elise'),
                    'required'  => array('opt-title-bar', "=", 1),
                    'subtitle'  => __('Your custom page subtitle.', 'Elise'),
                    //'desc'      => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
                    'default'   => '',
                ),

                array(
                    'id'        => 'opt-custom-page-title',
                    'type'      => 'editor',
                    'title'     => __('Custom Page Title Text', 'Elise'),
                    // 'subtitle'  => __('', 'Elise'),
                    // 'desc'      => __('H1 - default title Heading Tag', 'Elise'),
                    'required'  => array('opt-title-bar', "=", 1),
                        'args'   => array(
                            'wpautop'          => false,
                            'textarea_rows'    => 5,
                            'media_buttons'    => false,
                            'teeny'            => false,
                            // 'tinymce'          => array(
                            //         'toolbar1' => 'bold'
                            //     )
                        )
                ),

                // array(
                //     'id'            => 'opt-title-bar-height',
                //     'type'          => 'slider',
                //     'required'      => array('opt-title-bar', "=", 1),
                //     'title'         => __('Title Bar Height', 'Elise'),
                //     // 'subtitle'      => __('Title Bar height in pixels.', 'Elise'),
                //     'desc'          => __('Navbar height. Min: 60, max: 800, default value: 120', 'Elise'),
                //     'min'           => 60,
                //     'step'          => 5,
                //     'max'           => 800,
                //     'display_value' => 'text'
                // ),

                array(
                    'id' => 'section-bread-start',
                    'type' => 'section',
                    'title' => __('Breadcrumbs Bar', 'Elise'),
                    //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
                    'indent' => false 
                ),

                array(
                    'id'        => 'opt-breadcrumbs-bar',
                    'type'      => 'switch',
                    'title'     => __('Show Breadcrumbs', 'Elise'),
                    'subtitle'  => __('Enable/Disable breadcrumbs with Title Bar.', 'Elise'),
                ),

                array(
                    'id'        => 'opt-breadcrumbs-style',
                    'type'      => 'button_set',
                    'title'     => __('Breadcrumbs Style', 'Elise'),
                    'subtitle'  => __('Style of breadcrumbs.', 'Elise'),
                    'required'  => array('opt-breadcrumbs-bar', "=", 1),
                    //Must provide key => value pairs for radio options
                    'options'   => array(
                        '1' => 'Under Title Bar', 
                        '2' => 'On Title Bar'
                    ), 
                ),

                array(
                    'id'        => 'opt-breadcrumbs-share',
                    'type'      => 'switch',
                    'required'  => array(
                        array('opt-breadcrumbs-bar', "=", 1),
                        array('opt-breadcrumbs-style', "=", 1),
                    ),
                    'title'     => __('Show Share Buttons', 'Elise'),
                    'subtitle'  => __('Enable/Disable share buttons on Breadcrumbs Bar.', 'Elise'),
                ),

                array(
                    'id' => 'section-bread-end',
                    'type' => 'section',
                    'indent' => false 
                ),
        )
    );   
    
    $boxSections[] = array(
        'title' => __('Page Title Styling', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon_class' => 'icon-large',
        'icon' => 'el-icon-text-width',
        'fields' => array(

            array(
                'id'       => 'opt-pagetitle-color',
                'type'     => 'color',
                'title'    => __( 'Page Title Text Color', 'Elise' ),
                'required'      => array('opt-title-bar', "=", 1),
                // 'subtitle' => __( 'Pick top bar text color. (default: #ffffff)', 'Elise' ),
                'transparent' => false,
                'validate' => 'color',
            ),

            // array(
            //     'id'        => 'opt-pt-text-bg',
            //     'type'      => 'color_rgba',
            //     'title'    => __( 'Page Title Text BG Color', 'Elise' ),
            //     'required'      => array('opt-title-bar', "=", 1),
            //     'subtitle' => __( 'Pick page title text bg color. (default: transparent)', 'Elise' ),
            //     'options' => array(
            //         // 'allow_empty' => false,
            //         'clickout_fires_change' => true,
            //     ),
                // 'validate'  => 'colorrgba',
            // ),

            array(
                'id'        => 'opt-titletext-bg',
                'type'      => 'color_rgba',
                'title'    => __( 'Page Title Text BG Color', 'Elise' ),
                'required'      => array('opt-title-bar', "=", 1),
                'subtitle' => __( 'Pick page title text bg color. (default: rgba(0, 0, 0, 0))', 'Elise' ),
                // 'default'   => array('color' => '#000000', 'alpha' => 0),
                'options' => array(
                    'allow_empty' => false,
                    'clickout_fires_change' => true,
                ),
                'validate'  => 'colorrgba',
            ),

            array(
                'id'        => 'opt-pt-bg',
                'type'      => 'background',
                'title'     => __('Page Title Text Background', 'Elise'),
                'required'  => array('opt-title-bar', "=", 1),
                // 'subtitle'  => __('Pick Page Title background color or image.', 'Elise'),
                'preview_height' => '110px',
                'preview'   => true,
                'preview_media' => true,
            ),

            array(
                'id'       => 'opt-title-bar-padding',
                'type'     => 'spacing',
                'required'  => array('opt-title-bar', "=", 1),
                // An array of CSS selectors to apply this font style to
                'mode'     => 'padding',
                // absolute, padding, margin, defaults to padding
                'all'      => false,
                // Have one field that applies to all
                //'top'           => false,     // Disable the top
                'right'         => false,     // Disable the right
                //'bottom'        => false,     // Disable the bottom
                'left'          => false,     // Disable the left
                'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
                //'units_extended'=> 'true',    // Allow users to select any type of unit
                // 'display_units' => true,   // Set to false to hide the units if the units are specified
                'title'    => __( 'Title Bar Padding', 'Elise' ),
                'subtitle' => __( 'Title Bar top and bottom padding size. (default: top: 45px, bottom: 45px)', 'Elise' ),
            ),

            array(
                'id'        => 'opt-pt-overlay',
                'type'      => 'switch',
                'title'     => __('Page Title image bg Overlay', 'Elise'),
                'required'  => array('opt-title-bar', "=", 1),
                // 'subtitle'  => __('Show overlay on title bar image background. (default: #000000, alpha: 0.8)', 'Elise'),
            ),

            array(
                'id'        => 'opt-pt-overlay-bg',
                'type'      => 'color_rgba',
                'title'    => __( 'Page Title image bg Overlay Color', 'Elise' ),
                'required'  => array( 
                    array('opt-pt-overlay', "=", 1),
                    array('opt-title-bar', "=", 1)
                ),
                'subtitle' => __( 'Page Title image overlay color.', 'Elise' ),
                // 'default'   => array('color' => '#000000', 'alpha' => '0.8'),
                'transparent' => true,
                'options' => array(
                    'allow_empty' => false,
                    'clickout_fires_change' => true,
                ),
                'validate'  => 'colorrgba',
            ),

            array(
                'id'     => 'opt-titlebar-info',
                'type'   => 'info',
                'notice' => true,
                'style'  => 'info',
                'required' => array('opt-title-bar', "=", 0),
                'icon'   => 'el-icon-info-sign',
                'title'  => __( 'Title Bar is Disabled.', 'Elise' ),
            ),

            array(
                'id' => 'section-breadcrumbs-start',
                'type' => 'section',
                'title' => __('Breadcrumbs Bar', 'Elise'),
                'indent' => false 
            ),

            array(
                'id'       => 'opt-breadcrumbs-background',
                'type'     => 'color',
                'title'    => __( 'Breadcrumbs Bar Background', 'Elise' ),
                'required'  => array('opt-breadcrumbs-bar', "=", 1),
                'transparent' => false,
                'validate' => 'color',
            ),

            array(
                'id'       => 'opt-breadcrumbs-color',
                'type'     => 'color',
                'title'    => __( 'Breadcrumbs Bar Text Color', 'Elise' ),
                'required'  => array('opt-breadcrumbs-bar', "=", 1),
                'transparent' => false,
                'validate' => 'color',
            ),

            array(
                'id'       => 'opt-breadcrumbs-links',
                'type'     => 'link_color',
                'title'    => __( 'Breadcrumbs Bar Links Color', 'Elise' ),
                'required'  => array('opt-breadcrumbs-bar', "=", 1),
                //'regular'   => false, // Disable Regular Color
                //'hover'     => false, // Disable Hover Color
                'active'    => false, // Disable Active Color
                //'visited'   => true,  // Enable Visited Color
            ),

            array(
                'id'        => 'opt-breadcrumbs-border',
                'type'      => 'border',
                'title'     => __('Breadcrumbs Bar Borders', 'Elise'),
                'required'  => array('opt-breadcrumbs-bar', "=", 1),
                'all'       => false,
                'bottom'    => true,
                'top'       => false,
                'left'       => false,
                'right'       => false,
            ),

            array(
                'id'     => 'opt-breadbar-info',
                'type'   => 'info',
                'notice' => true,
                'style'  => 'info',
                'required'  => array('opt-breadcrumbs-bar', "=", 0),
                'icon'   => 'el-icon-info-sign',
                'title'  => __( 'Breadcrumbs Bar is Disabled.', 'Elise' ),
            ),

            array(
                'id' => 'section-breadcrumbs-end',
                'type' => 'section',
                'indent' => false 
            ),
        )
    );   

    $boxSections[] = array(
        'title' => __('Content Padding', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon' => 'el-icon-resize-vertical',
        'fields' => array(  

            array(
                'id'       => 'opt-content-padding',
                'type'     => 'spacing',
                // An array of CSS selectors to apply this font style to
                'mode'     => 'padding',
                // absolute, padding, margin, defaults to padding
                'all'      => false,
                // Have one field that applies to all
                //'top'           => false,     // Disable the top
                'right'         => false,     // Disable the right
                //'bottom'        => false,     // Disable the bottom
                'left'          => false,     // Disable the left
                'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
                //'units_extended'=> 'true',    // Allow users to select any type of unit
                // 'display_units' => true,   // Set to false to hide the units if the units are specified
                'title'    => __( 'Content Padding', 'Elise' ),
                'subtitle' => __( 'Content Padding top and bottom dimensions. (default: top: 55px, bottom: 60px)', 'Elise' ),
            ),
        )
    );    

    $boxSections[] = array(
        // 'title' => __('Content Padding', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon' => 'el-icon-home',
        'type' => 'divide',
        'fields' => array(  

            array(
                'id'   =>'divider_1',
                // 'desc' => __('This is the description field, again good for additional info.', 'Elise'),
                'type' => 'divide'
            )
        )
    );    


    $metaboxes[] = array(
        'id' => 'page-title-options',
        'title' => __('Page Options', 'Elise'),
        'post_types' => array('page','post','portfolio', 'service'),
        //'page_template' => array('page-test.php'),
        //'post_format' => array('image'),
        'position' => 'normal', // normal, advanced, side
        'priority' => 'high', // high, core, default, low
        'sections' => $boxSections
    );







    // Advanced Page Options
    $boxSections = array();

    $boxSections[] = array(
        'title' => __('Info', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon' => 'el-icon-info-sign',
        //'type' => 'divide',
        'fields' => array(  

            array(
                'id'   =>'divider_1',
                'desc' => __('Advanced Page Options.', 'Elise'),
                'type' => 'divide'
            )
        )
    );    

    $boxSections[] = array(
        'title' => __('Custom Logo', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon' => 'el-icon-home-alt',
        'fields' => array(  

            array(
                'id'        => 'opt-logo',
                'type'      => 'media',
                'url'       => true,
                'title'     => __('Logo Image', 'Elise'),
                'subtitle'  => __('Logo Image', 'Elise'),
                'mode'      => 'image',
            ),

            array(
                'id'        => 'opt-logo-stickyh',
                'type'      => 'media',
                'url'       => true,
                'required'  => array('opt-show-sticky-header', "=", 1),
                'title'     => __('Sticky Header Logo Image', 'Elise'),
                'subtitle'  => __('Sticky Header Custom Logo Image (overwrite main logo).', 'Elise'),
                'mode'      => 'image',
            ),

            array(
                'id'        => 'opt-logo-mobile',
                'type'      => 'media',
                'url'       => true,
                'title'     => __('Mobile Navigation Logo Image', 'Elise'),
                'subtitle'  => __('Mobile/Full Width Custom Logo Image (overwrite main logo).', 'Elise'),
                'mode'      => 'image',
            ),
        )
    );       

    $boxSections[] = array(
        'title' => __('Header/Footer', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon' => 'el-icon-minus',
        'fields' => array(  

            array(
                'id'        => 'opt-show-header',
                'type'      => 'switch',
                // 'required'      => array('opt-navbar-style','!=','4'),
                'title'     => __('Enable/Disable Header', 'Elise'),
                'default'   => true,
                'subtitle'  => __('Enable or Disable all Header elements.', 'Elise'),
            ),

            array(
                'id'        => 'opt-show-footer',
                'type'      => 'switch',
                'default'   => true,
                'title'     => __('Enable/Disable Footer', 'Elise'),
                'subtitle'  => __('Enable or Disable all Footer elements.', 'Elise'),
            ),


            array(
                'id'        => 'opt-show-top-bar',
                'type'      => 'switch',
                'title'     => __('Show Top Bar', 'Elise'),
                'subtitle'  => __('Enable/Disable bar above header.', 'Elise'),
            ),

            array(
                'id'        => 'opt-show-sticky-header',
                'type'      => 'switch',
                // 'required'  => array('opt-title-bar', "=", 1),
                'title'     => __('Show Sticky Header', 'Elise'),
            ),

            array(
                'id'        => 'opt-footer-widget-area',
                'type'      => 'switch',
                'title'     => __('Show Widget Area', 'Elise'),
                'subtitle'  => __('Enable/Disable Widget Area in Footer.', 'Elise'),
            ),
        )
    );       


    $boxSections[] = array(
        'title' => __('Custom Header', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon' => 'el-icon-chevron-up',
        'fields' => array( 

            array(
                'id'        => 'opt-meta-custom-menu',
                'type'      => 'select',
                'data'      => 'menus',
                'title'     => __('Custom Menu', 'Elise'),
                'subtitle'      => __('This Menu will be visible only on this page.', 'Elise'),
            ), 

            array(
                'id'        => 'opt-navbar-style',
                'type'      => 'image_select',
                'compiler'  => true,
                'title'     => __('Navbar Style', 'Elise'),
                'subtitle'  => __('Main Navbar style.', 'Elise'),
                'options'   => array(
                    '1' => array('alt' => 'Header Standard',       'img' => ReduxFramework::$_url . 'assets/img/elise-hs.png'),
                    // '2' => array('alt' => 'Header Full Width',  'img' => ReduxFramework::$_url . 'assets/img/elise-hfw.png'),
                    '2' => array('alt' => 'Header Centered', 'img' => ReduxFramework::$_url . 'assets/img/elise-hlt.png'),
                    '3' => array('alt' => 'Header Shop','img' => ReduxFramework::$_url . 'assets/img/elise-hsh.png'),
                    '4' => array('alt' => 'Header Bar',  'img' => ReduxFramework::$_url . 'assets/img/elise-hb.png')
                ),
            ),

            array(
                'id'        => 'opt-nav-full-width',
                'type'      => 'switch',
                'required'      => array('opt-navbar-style','!=','4'),
                'title'     => __('Nav Full Width', 'Elise'),
                'subtitle'  => __('Enable/Disable Full Width navbar.', 'Elise'),
            ),

            array(
                'id'        => 'opt-navbar-transparent',
                'type'      => 'switch',
                'required' => array( 
                                array('opt-navbar-style','!=','3'), 
                                array('opt-navbar-style','!=','4') 
                ),      
                'title'     => __('Transparent Navbar background', 'Elise'),
                'subtitle'  => __('Enable/Disable transparent navbar background.', 'Elise'),
            ),

            array(
                'id'        => 'opt-menu-position',
                'type'      => 'button_set',
                'title'     => __('Menu Position', 'Elise'),
                'subtitle'  => __('Menu position on navbar.', 'Elise'),                   
                //Must provide key => value pairs for radio options
                'options'   => array(
                    '1' => 'Left', 
                    '2' => 'Center', 
                    '3' => 'Right'
                ), 
            ),

            array(
                'id'            => 'opt-navbar-height',
                'type'          => 'slider',
                'title'         => __('Navbar Height', 'Elise'),
                'required' => array( 
                                array('opt-navbar-style','!=','2'), 
                                array('opt-navbar-style','!=','3'), 
                                array('opt-navbar-style','!=','4') 
                ),                        
                'subtitle'      => __('Navbar height in pixels.', 'Elise'),
                'desc'          => __('Navbar height. Min: 50, max: 300, default value: 90', 'Elise'),
                'min'           => 50,
                'step'          => 5,
                'max'           => 300,
                'display_value' => 'text'
            ),

            array(
                'id'            => 'opt-header-height',
                'type'          => 'slider',
                'required'      => array('opt-navbar-style','=','2'),
                'title'         => __('Header Height', 'Elise'),
                'subtitle'      => __('Header height in pixels.', 'Elise'),
                'desc'          => __('Navbar height. Min: 120, max: 500, default value: 150', 'Elise'),
                'min'           => 120,
                'step'          => 5,
                'max'           => 500,
                'display_value' => 'text'
            ),

            array(
                'id'        => 'opt-hover-style',
                'type'      => 'image_select',
                'compiler'  => true,
                'required' => array( 
                                array('opt-navbar-style','!=','3'), 
                                array('opt-navbar-style','!=','4'),
                ),          
                'title'     => __('Hover Style', 'Elise'),
                'subtitle'  => __('Main Menu hover style.', 'Elise'),
                'options'   => array(
                            '1' => array('alt' => 'Hover Block',       'img' => ReduxFramework::$_url . 'assets/img/hover-block.jpg'),
                            '2' => array('alt' => 'Hover Boxed',  'img' => ReduxFramework::$_url . 'assets/img/hover-boxed.jpg')
                ),
            ),

            array(
                'id'        => 'opt-nav-search',
                'type'      => 'switch',
                'title'     => __('Nav search', 'Elise'),
                'subtitle'  => __('Search icon in Navbar', 'Elise'),
            ),

            array(
                'id'        => 'opt-nav-subarrow',
                'type'      => 'switch',
                'title'     => __('Submenu Arrow', 'Elise'),
                'subtitle'  => __('Enable/Disable menu item arrows if menu item has submenu.', 'Elise'),
            ),
        )
    );   

    $boxSections[] = array(
        'title' => __('Custom Header Styling', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon' => 'el-icon-magic',
        'fields' => array(  

            array(
                'id'       => 'opt-navigation-color-bg',
                'type'     => 'color',
                'title'    => __( 'Navigation Background Color', 'Elise' ),
                'transparent' => false,
                'validate' => 'color',
            ),

            array(
                'id'        => 'opt-navigation-shadow',
                'type'      => 'switch',
                'title'     => __('Navigation Bar Shadow', 'Elise'),
            ),

            array(
                'id'        => 'opt-nav-transparent-bg',
                'type'      => 'color_rgba',
                'required'      => array('opt-navbar-transparent','=','1'),
                'title'    => __( 'Transparent Navigation Background', 'Elise' ),
                'options' => array(
                    'allow_empty' => false,
                    'clickout_fires_change' => true,
                ),
                'validate'  => 'colorrgba',
            ),

            array(
                'id'        => 'opt-navbar-border',
                'type'      => 'color_rgba',
                'title'    => __( 'Navigation Bar Bottom Border Color', 'Elise' ),
                'options' => array(
                    'allow_empty' => false,
                    'clickout_fires_change' => true,
                ),
                'validate'  => 'colorrgba',
            ),

            array(
                'id'       => 'opt-navigation-color',
                'type'     => 'link_color',
                'title'    => __( 'Menu Color', 'Elise' ),
                //'regular'   => false, // Disable Regular Color
                //'hover'     => false, // Disable Hover Color
                'active'    => false, // Disable Active Color
                //'visited'   => true,  // Enable Visited Color
            ),

            array(
                'id'       => 'opt-navigation-submenu-color-bg',
                'type'     => 'color',
                'title'    => __( 'Submenu Background Color', 'Elise' ),
                'transparent' => false,
                'validate' => 'color',
            ),

            array(
                'id'       => 'opt-navigation-submenu-color',
                'type'     => 'link_color',
                'title'    => __( 'Submenu Links Color', 'Elise' ),
                //'regular'   => false, // Disable Regular Color
                //'hover'     => false, // Disable Hover Color
                'active'    => false, // Disable Active Color
                //'visited'   => true,  // Enable Visited Color
            ),

            array(
                'id'        => 'opt-navigation-submenu-border',
                'type'      => 'border',
                'title'     => __('Submenu Bottom Border Line', 'Elise'),
                'all'       => false,
                'bottom'    => true,
                'top'       => false,
                'left'       => false,
                'right'       => false,
            ),

            // array(
            //     'id'       => 'opt-nav-arrows',
            //     'type'     => 'color',
            //     'title'    => __( 'Submenu Arrow Icon', 'Elise' ),
            //     'transparent' => false,
            //     'validate' => 'color',
            // ),

            array(
                'id'       => 'opt-navigation-icons-color',
                'type'     => 'color',
                'title'    => __( 'Nav Icons Color', 'Elise' ),
                'transparent' => false,
                'validate' => 'color',
            ),

            array(
                'id'        => 'opt-navigation-menu-border',
                'type'      => 'border',
                'title'     => __('Menu top border', 'Elise'),
                'all'       => false,
                'bottom'    => false,
                'top'       => true,
                'left'       => false,
                'right'       => false,
            ),

                    // Sticky Header -------------------------------------- /
                    array(
                        'id' => 'section-sticky-h-start',
                        'type' => 'section',
                        'title' => __('Sticky Header', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'        => 'opt-stickyh-settings',
                        'type'      => 'button_set',
                        'title'     => __('Sticky Header Colors', 'Elise'),
                        'required'  => array('opt-show-sticky-header','=','1'),
                        // 'subtitle'  => __('Select overlay backgroud color style.', 'Elise'),
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => __('Same As Navigation Bar', 'Elise'), 
                            '2' => __('Custom Colors', 'Elise')
                        ), 
                    ),

                    array(
                        'id'        => 'opt-stickyheader-bg',
                        'type'      => 'color_rgba',
                        'required'      => array(
                            array('opt-stickyh-settings','=','2'),
                            array('opt-show-sticky-header','=','1'),
                        ),
                        'title'    => __( 'Sticky Header Background', 'Elise' ),
                        'subtitle' => __( 'Pick color settings for Sticky Header BG. (default: #ffffff, 0.96)', 'Elise' ),
                        'options' => array(
                            'allow_empty' => false,
                            'clickout_fires_change' => true,
                        ),
                        'validate'  => 'colorrgba',
                    ),

                    array(
                        'id'       => 'opt-stickyh-color',
                        'type'     => 'link_color',
                        'title'    => __( 'Sticky Header Menu Colors', 'Elise' ),
                        'required'      => array(
                            array('opt-stickyh-settings','=','2'),
                            array('opt-show-sticky-header','=','1'),
                        ),
                        'subtitle' => __( 'Pick menu link colors. (default: #262626/#8dc73f)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                    ),

                    array(
                        'id'       => 'opt-stickyh-nav-icons',
                        'type'     => 'color',
                        'title'    => __( 'Nav Icons Color', 'Elise' ),
                        'subtitle' => __( 'Pick sticky header nav icons color. (default: #888888)', 'Elise' ),
                        // 'default'  => '#888888',
                        'required'      => array(
                            array('opt-stickyh-settings','=','2'),
                            array('opt-show-sticky-header','=','1'),
                        ),
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'     => 'opt-sticky-info',
                        'type'   => 'info',
                        'notice' => true,
                        'style'  => 'info',
                        'required'  => array('opt-show-sticky-header','=','0'),
                        'icon'   => 'el-icon-info-sign',
                        'title'  => __( 'Sticky Header is Disabled.', 'Elise' ),
                        'desc'   => __( 'Go to Header -> Sticky Header Section to Enabled Sticky Header.', 'Elise' )
                    ),

                    array(
                        'id' => 'section-sticky-h-end',
                        'type' => 'section',
                        'indent' => false 
                    ),


                    // Top Bar ------------------------------------------- /
                    array(
                        'id' => 'section-topbar-start',
                        'type' => 'section',
                        'title' => __('Top Bar', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'       => 'opt-topbar-background',
                        'type'     => 'color',
                        'title'    => __( 'Top Bar Background', 'Elise' ),
                        'required'  => array('opt-show-top-bar', "=", 1),
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-topbar-color',
                        'type'     => 'color',
                        'title'    => __( 'Top Bar Text Color', 'Elise' ),
                        'required'  => array('opt-show-top-bar', "=", 1),
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-topbar-links',
                        'type'     => 'link_color',
                        'title'    => __( 'Top Bar Links', 'Elise' ),
                        'required'  => array('opt-show-top-bar', "=", 1),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                    ),

                    array(
                        'id'       => 'opt-topbar-social',
                        'type'     => 'color',
                        'title'    => __( 'Top Bar Social Icons Color', 'Elise' ),
                        'required'  => array('opt-show-top-bar', "=", 1),
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'        => 'opt-topbar-border',
                        'type'      => 'border',
                        'title'     => __('Top Bar Borders', 'Elise'),
                        'required'  => array('opt-show-top-bar', "=", 1),
                        'all'       => false,
                        'bottom'    => true,
                        'top'       => false,
                        'left'       => false,
                        'right'       => false,
                    ),

                    array(
                        'id'     => 'opt-topbar-info',
                        'type'   => 'info',
                        'notice' => true,
                        'style'  => 'info',
                        'required'  => array('opt-show-top-bar', "=", 0),
                        'icon'   => 'el-icon-info-sign',
                        'title'  => __( 'Top Bar is Disabled.', 'Elise' ),
                        'desc'   => __( 'Go to Header -> Top Bar Section to Enabled Top Bar', 'Elise' )
                    ),

                    array(
                        'id' => 'section-topbar-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

        )
    ); 

    $boxSections[] = array(
        'title' => __('Basic Color Styling', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon' => 'el-icon-tint',
        'fields' => array(  

            array(
                'id'       => 'opt-color-body-bg',
                'type'     => 'color',
                'title'    => __( 'Body Background Color', 'Elise' ),
                'subtitle' => __( 'Pick a body background color (default: #ffffff)', 'Elise' ),
                'transparent' => false,
                'validate' => 'color',
            ),

            array(
                'id'       => 'opt-color-body-text',
                'type'     => 'color',
                'title'    => __( 'Body Text Color', 'Elise' ),
                'subtitle' => __( 'Pick a body text color (default: #747474)', 'Elise' ),
                'transparent' => false,
                'validate' => 'color',
            ),

            array(
                'id'       => 'opt-color-headings',
                'type'     => 'color',
                'title'    => __( 'Headings Color', 'Elise' ),
                'subtitle' => __( 'Pick a headings color (default: #262626)', 'Elise' ),
                'transparent' => false,
                'validate' => 'color',
            ),


            array(
                'id'       => 'opt-link-colors',
                'type'     => 'link_color',
                'title'    => __( 'Link Colors', 'Elise' ),
                'subtitle' => __( 'Pick link colors. (default: #8dc73f/#8dc73f)', 'Elise' ),
                //'regular'   => false, // Disable Regular Color
                //'hover'     => false, // Disable Hover Color
                'active'    => false, // Disable Active Color
                //'visited'   => true,  // Enable Visited Color
            ),
        )
    );  

    $boxSections[] = array(
        'title' => __('Custom Layout', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon' => 'el-icon-website',
        'fields' => array(  

            array(
                'id'        => 'opt-layout',
                'type'      => 'image_select',
                'compiler'  => true,
                'title'     => __('Layout', 'Elise'),
                'subtitle'  => __('Theme layout.', 'Elise'),
                'options'   => array(
                    '1' => array('title'=>'Wide', 'alt' => 'Wide',       'img' => ReduxFramework::$_url . 'assets/img/layout-wide.jpg'),
                    '2' => array('title'=>'Boxed', 'alt' => 'Boxed', 'img' => ReduxFramework::$_url . 'assets/img/layout-boxed.jpg'),
                    '3' => array('title'=>'Bordered', 'alt' => 'Bordered','img' => ReduxFramework::$_url . 'assets/img/layout-bordered.jpg'),
                ),
            ),

            array(
                'id'            => 'opt-boxed-gap',
                'type'          => 'slider',
                //'required'      => array('opt-title-bar', "=", 1),
                'title'         => __('Boxed Container Gap', 'Elise'),
                'required'      => array('opt-layout','=','2'),
                'subtitle'      => __('Gap between content and background.', 'Elise'),
                'desc'          => __('Min: 30, max: 150, default value: 60', 'Elise'),
                'min'           => 30,
                'step'          => 5,
                'max'           => 150,
                'display_value' => 'text'
            ),

            array(
                'id'            => 'opt-border-size',
                'type'          => 'slider',
                //'required'      => array('opt-title-bar', "=", 1),
                'title'         => __('Layout Border Size', 'Elise'),
                'required'      => array('opt-layout','=','3'),
                'subtitle'      => __('Bordered layout - border padding.', 'Elise'),
                'desc'          => __('Min: 10, max: 60, default value: 30', 'Elise'),
                'min'           => 10,
                'step'          => 5,
                'max'           => 60,
                'display_value' => 'text'
            ),

            array(
                'id'        => 'opt-body-bg',
                'type'      => 'background',
                'title'     => __('Body Background', 'Elise'),
                'required'      => array('opt-layout','!=','1'),
                'subtitle'  => __('Body background color or image.', 'Elise'),
                //'preview_height' => '100px',
                'preview'   => true,
                'preview_media' => true,
                'transparent' => false,
            ),

            array(
                'id'        => 'opt-layout-wrapper-shadow',
                'type'      => 'switch',
                'required'      => array('opt-layout','!=','1'),
                'title'     => __('Drop Shadow', 'Elise'),
                'subtitle'  => __('Layout Drops Shadow.', 'Elise'),
            ),
        )
    ); 

    $boxSections[] = array(
        'title' => __('Custom CSS', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon' => 'el-icon-css',
        'fields' => array(  

            array(
                'id'        => 'opt-meta-ace-editor-css',
                'type'      => 'ace_editor',
                'title'     => __('Custom CSS Code', 'Elise'),
                'subtitle'  => __('Add your CSS code here.', 'Elise'),
                'mode'      => 'css',
                'theme'     => 'monokai',
                'default'   => ""
            ),

            // array(
            //     'id'        => 'opt-meta-ace-editor-js',
            //     'type'      => 'ace_editor',
            //     'title'     => __('Custom JS Code', 'Elise'),
            //     'subtitle'  => __('Add your JS code here.', 'Elise'),
            //     'mode'      => 'javascript',
            //     'theme'     => 'monokai',
            //     'default'   => ""
            // ),

            // array(
            //     'id'        => 'opt-ace-editor-head',
            //     'type'      => 'ace_editor',
            //     'title'     => __('&lt;head&gt Code', 'Elise'),
            //     'subtitle'  => __('Add code before &lt;/head&gt tag.', 'Elise'),
            //     'mode'      => 'html',
            //     'theme'     => 'chrome',
            // ),

            // array(
            //     'id'        => 'opt-ace-editor-footer',
            //     'type'      => 'ace_editor',
            //     'title'     => __('Footer Code', 'Elise'),
            //     'subtitle'  => __('Add code before &lt;/body&gt tag.', 'Elise'),
            //     'mode'      => 'html',
            //     'theme'     => 'chrome',
            // ),
        )
    ); 



    $metaboxes[] = array(
        'id' => 'advanced-page-options',
        'title' => __('Advanced Page Options', 'Elise'),
        'post_types' => array('page','post','portfolio','service'),
        //'page_template' => array('page-test.php'),
        //'post_format' => array('image'),
        'position' => 'normal', // normal, advanced, side
        'priority' => 'high', // high, core, default, low
        'sections' => $boxSections
    );












    // layout section
    $boxSections = array();
    $boxSections[] = array(
        //'title' => __('Home Settings', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon_class' => 'icon-large',
        'icon' => 'el-icon-home',
        'fields' => array(



                    array(
                        'id'        => 'opt-project-layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Project page Layout', 'Elise'),
                        'subtitle'  => __('Single project page layout.', 'Elise'),
                        'options'   => array(
                                '1' => array('title' => 'Large Images', 'alt' => 'Large',       'img' => ReduxFramework::$_url . 'assets/img/project-large.jpg'),
                                '2' => array('title' => 'Medium Images', 'alt' => 'Medium',  'img' => ReduxFramework::$_url . 'assets/img/project-medium.jpg'),
                                '3' => array('title' => 'Wide Images', 'alt' => 'Wide',  'img' => ReduxFramework::$_url . 'assets/img/project-wide.jpg'),
                                '4' => array('title' => 'Clean Page', 'alt' => 'Clean',  'img' => ReduxFramework::$_url . 'assets/img/project-clean.jpg'),
                            ),
                    ),

                    array(
                        'id'        => 'opt-project-image-gallery',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'required' => array(
                                array('opt-project-layout', "!=", 3),
                                array('opt-project-layout', "!=", 4)
                            ),
                        'title'     => __('Project Image Gallery', 'Elise'),
                        'subtitle'  => __('Project images gallery Layout.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Inline',  'img' => ReduxFramework::$_url . 'assets/img/project-gallery-slider.jpg'),
                            '2' => array('alt' => 'Slider',       'img' => ReduxFramework::$_url . 'assets/img/project-gallery-inline.jpg')
                        ),
                    ),

                    array(
                        'id'            => 'opt-gallery-wide-height',
                        'type'          => 'slider',
                        'title'         => __('Gallery Images Height', 'Elise'),
                        'required'      => array('opt-project-layout', "=", 3),                      
                        'subtitle'      => __('Wide Gallery images height.', 'Elise'),
                        'desc'          => __('Min: 300, max: 1000, default value: 700', 'Elise'),
                        'min'           => 300,
                        'step'          => 5,
                        'max'           => 1000,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'       => 'opt-project-desc-position',
                        'type'     => 'select',
                        'required' => array('opt-project-layout', "=", 1),
                        'title'    => __('Project Description Position', 'Elise'), 
                        'select2'  => array( 'allowClear' => false, 'minimumResultsForSearch' => -1),
                        'subtitle' => __('Choose position where project description and meta will show up.', 'Elise'),
                        //'desc'     => __('This is the description field, again good for additional info.', 'Elise'),
                        // Must provide key => value pairs for select options
                        'options'  => array(
                            '1' => 'Above the Project Gallery',
                            '2' => 'Below the Project Gallery'
                        ),
                    ),

                    array(
                        'id'        => 'opt-project-desc-style',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'required' => array(
                                array('opt-project-layout', "!=", 2),
                                array('opt-project-layout', "!=", 4)
                            ),
                        'title'     => __('Project Description Style', 'Elise'),
                        'subtitle'  => __('Choose style of description.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Description Full Width',       'img' => ReduxFramework::$_url . 'assets/img/project-desc-full.jpg'),
                            '2' => array('alt' => 'Description with Sidebar',  'img' => ReduxFramework::$_url . 'assets/img/project-desc-sidebar.jpg')
                        ),
                    ),

                    array(
                        'id'       => 'opt-portfolio-project-wide',
                        'type'     => 'color',
                        'title'    => __( 'Project Style Wide BG', 'Elise' ),
                        'subtitle' => __( 'Pick project style wide gallery background. (default: #333333)', 'Elise' ),
                        'transparent' => false,
                        'required'  => array('opt-project-layout','=','3'),
                        'validate' => 'color',
                    ),
        )
    );
  
    $metaboxes[] = array(
        'id' => 'project-layout',
        //'title' => __('Cool Options', 'Elise'),
        'post_types' => array('portfolio'),
        //'page_template' => array('page-test.php'),
        //'post_format' => array('image'),
        'position' => 'side', // normal, advanced, side
        'priority' => 'high', // high, core, default, low
        'sections' => $boxSections
    );


    // thumb size select for single portfolio
    $boxSections = array();
    $boxSections[] = array(
        //'title' => __('Home Settings', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon_class' => 'icon-large',
        'icon' => 'el-icon-home',
        'fields' => array(

            array(
                'id'       => 'opt-masonry-thumb-size',
                'type'     => 'select',
                'title'    => __('Masonry Thumb Size', 'Elise'), 
                'select2'  => array( 'allowClear' => false, 'minimumResultsForSearch' => -1),
                'subtitle' => __('Thumbnail size when Portfolio Page layout is masonry', 'Elise'),
                //'desc'     => __('This is the description field, again good for additional info.', 'Elise'),
                // Must provide key => value pairs for select options
                'options'  => array(
                    '1' => 'Standard',
                    '2' => 'Wide',
                    '3' => 'Tall',
                    '4' => 'Big'
                ),
                'default'  => '1',
            )
        )
    );
  
    $metaboxes[] = array(
        'id' => 'masonry-thumb',
        //'title' => __('Cool Options', 'Elise'),
        'post_types' => array('portfolio'),
        //'page_template' => array('page-test.php'),
        //'post_format' => array('image'),
        'position' => 'side', // normal, advanced, side
        'priority' => 'low', // high, core, default, low
        'sections' => $boxSections
    );





    // sidebar selection for template-sidebar.php
    $page_options = array();
    $page_options[] = array(
        //'title'         => __('General Settings', 'Elise'),
        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-home',
        'fields'        => array(
            array(
                'id' => 'opt-meta-select-sidebar',
                'title' => __( 'Sidebar', 'Elise' ),
                'desc' => 'Please select the sidebar you would like to display on this page. Note: You must first create the sidebar under Appearance > Widgets.',
                'type' => 'select',
                'data' => 'sidebars',
                'default' => 'None',
            ),

            array(
                'id'        => 'opt-meta-template-sidebar-position',
                'type'      => 'button_set',
                'title'     => __('Sidebar position', 'Elise'),
                'subtitle'  => __('Select sidebar position', 'Elise'),                   
                //Must provide key => value pairs for radio options
                'options'   => array(
                    '1' => __('Left', 'Elise'), 
                    '2' => __('Right', 'Elise')
                ), 
                'default'   => '2'
            ),
        ),
    );

    $metaboxes[] = array(
        'id'            => 'sidebar-selection',
        'title'         => __( 'Select Sidebar', 'Elise' ),
        'post_types'    => array( 'page'),
        'page_template' => array('template-page-sidebar.php'),
        //'post_format' => array('image'),
        'position'      => 'side', // normal, advanced, side
        'priority'      => 'low', // high, core, default, low
        'sidebar'       => true, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $page_options,
    );





    // single post layout section
    $boxSections = array();
    $boxSections[] = array(
        //'title' => __('Home Settings', 'Elise'),
        //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
        'icon_class' => 'icon-large',
        'icon' => 'el-icon-home',
        'fields' => array(

            array(
                'id'        => 'opt-blog-page-style-custom',
                'type'      => 'switch',
                'on'        => __('Yes', 'Elise'),
                'off'        => __('No', 'Elise'),
                'title'     => __('Custom Single Post Style', 'Elise'),
                'subtitle'  => __('Single Post Style different than Blog Style', 'Elise'),
            ),

            array(
                'id'        => 'opt-blog-page-style',
                'type'      => 'image_select',
                'compiler'  => true,
                'required' => array('opt-blog-page-style-custom','=','1'),
                'title'     => __('Blog Style', 'Elise'),
                'subtitle'  => __('Select blog page style.', 'Elise'),
                'options'   => array(
                            '1' => array('alt' => 'Sidebar Right',  'img' => ReduxFramework::$_url . 'assets/img/single-sidebar-right.jpg'),
                            '2' => array('alt' => 'Sidebar Left', 'img' => ReduxFramework::$_url . 'assets/img/single-sidebar-left.jpg'),
                            '3' => array('alt' => 'Full Width', 'img' => ReduxFramework::$_url . 'assets/img/single-fullwidth.jpg'),
                            '4' => array('alt' => 'Full Width Thin', 'img' => ReduxFramework::$_url . 'assets/img/single-wide.jpg')
                ),
            ),
        )
    );
  
    $metaboxes[] = array(
        'id' => 'single-post-layout',
        'title' => __('Post Layout', 'Elise'),
        'post_types' => array('post'),
        //'page_template' => array('page-test.php'),
        //'post_format' => array('image'),
        'position' => 'side', // normal, advanced, side
        'priority' => 'low', // high, core, default, low
        'sections' => $boxSections
    );





    // vc full width
    // $boxSections = array();
    // $boxSections[] = array(
    //     //'title' => __('Home Settings', 'Elise'),
    //     //'desc' => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'Elise'),
    //     'icon_class' => 'icon-large',
    //     'icon' => 'el-icon-home',
    //     'fields' => array(

    //         array(
    //             'id'        => 'opt-meta-vc-fullwidth',
    //             'type'      => 'switch',
    //             'title'     => __('Visual Composer Full Width Page.', 'Elise'),
    //             'subtitle'  => __('Enable/Disable Full Width Content for Visual Composer.', 'Elise'),
    //             'default'   => false,
    //         ),

    //     )
    // );
  
    // $metaboxes[] = array(
    //     'id' => 'vc-full-width-content',
    //     'title' => __('VC Full Width.', 'Elise'),
    //     'post_types' => array('page'),
    //     //'page_template' => array('page-test.php'),
    //     //'post_format' => array('image'),
    //     'position' => 'side', // normal, advanced, side
    //     'priority' => 'low', // high, core, default, low
    //     'sections' => $boxSections
    // );


    // Kind of overkill, but ahh well.  ;)
    //$metaboxes = apply_filters( 'your_custom_redux_metabox_filter_here', $metaboxes );

    return $metaboxes;
  }
  add_action('redux/metaboxes/'.$redux_opt_name.'/boxes', 'redux_add_metaboxes');
endif;





// The loader will load all of the extensions automatically based on your $redux_opt_name
// changed file path to redirect to force overwrite parent theme
require_once(get_template_directory().'/eliseCore/ReduxCore/loader.php');