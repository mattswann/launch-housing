<?php
/**
 * Template Name: Services Page Template
 * This template is used for anything related to the Service post type
*/
wp_reset_postdata(); 
get_header();
global $post;
global $elise_options; 
 ?>

      <div class="content section">
        <div class="container">
          <div class="row">
            <?php if( has_term( 80, 'service_category' ) || is_page( 2235 ) ) : ?>
              <section class="col-md-12">
            <?php else : ?>  
              <section class="col-md-9">
            <?php endif; 
            get_template_part( 'template-parts/content', 'searchbar' ); 
            ?>

              <?php
              if(have_posts()) : while(have_posts()) : the_post();
                the_content();  
              endwhile; wp_reset_postdata(); else : 
                get_template_part( 'template-parts/content', 'none' ); 
              endif; ?>

            </section>
            
            <?php 
            // only single service pages will have a sidebar
            if( !has_term( 80, 'service_category' ) && !is_page( 2235 ) ) : ?>
              <!-- sidebar -->
              <aside class="col-md-3 sidebar-wrap">
                <?php 
                // shortcode from widget: service sidebar
                echo do_shortcode( '[stag_sidebar id="service-sidebar"]' ); ?>
                <!-- term link -->
                <section id="return">
                  <div class="return-title">RETURN TO</div>
                  <div class="return-btn">
                    <?php $term = get_the_terms($post->ID,'service_category')[0]; ?>
                    <a href="<?php echo get_bloginfo('url').'/service/'.$term->slug; ?>"><?php echo $term->name; ?></a>
                  </div>
                </section>
              </aside>
            <?php endif; ?>

          </div>
        </div>
      </div>


<?php get_footer(); ?>