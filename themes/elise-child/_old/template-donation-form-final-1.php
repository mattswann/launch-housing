<?php
/*
Template Name: Donation Form Template-2
*/
session_start();
// Turn off error reporting
error_reporting(0);
?>
<?php get_header(); 
require_once 'src/autoload.php';
?>

<?php
// Register API keys at https://www.google.com/recaptcha/admin
$siteKey = '6Ld1Nw4TAAAAAHbojD60ZL76eE8w_-t3FSM94-JG';
$secret = '6Ld1Nw4TAAAAALUqlrzmiuOHvVbjrvLWCr_hC8SW';


/*if (!empty($_REQUEST['captcha'])) {

    if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['captcha'])) != $_SESSION['captcha']) 
	{
        $get_data = '';
    	$captcha_value='wrong';
    	$captcha_message = "Invalid captcha";
        //$style = "background-color: #FF606C";
    } 
	else 
	{
		if(isset($_GET['pageid']))
		{
    		$get_data = $_GET['pageid'];
    	}
		else
		{
			$get_data = '';
		}
    	$captcha_value='true';
       // $captcha_message = "Valid captcha";
        //$style = "background-color: #CCFF99";
    }
  

    $request_captcha = htmlspecialchars($_REQUEST['captcha']);
    unset($_SESSION['captcha']);
}else{
  $captcha_value='nill';
  }
  
  */
//echo 'aayush'.$_SESSION['securimage_code_value'];
$c_error = '';
if (isset($_POST['g-recaptcha-response']))
{
    
    $recaptcha = new \ReCaptcha\ReCaptcha($secret);

    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

    if ($resp->isSuccess())
	{
		//echo "Success"; exit;
	}
    else
	{
			$captcha_value='nill';
			//$get_data = '';
		    //$captcha_value='wrong';
    		//$captcha_message = "Invalid captcha";
            foreach ($resp->getErrorCodes() as $code) {
                //echo '<tt>' , $code , '</tt> '; exit;
				$c_error = $code;
				
            }
			
			//header('Location : http://www.launchhousing.org.au/donate-now/?error='+captcha);
			//exit;
			

    }
}
if(isset($_REQUEST['amount'])){
  $amt_ = (int) $_REQUEST['amount'];
}else{
  $amt_ = 0;
}

define('SECUREPAY_TXN_ANTIFRAUD_PAY', 21);
define('SECUREPAY_TXN_ANTIFRAUD_CHECK', 22);

?>
<?php 
//if(isset($_GET['test']))
//{
//    $testing = 'yes';
//}
//else
//{
  $testing = 'no';
//}

 //echo $_SERVER['DOCUMENT_ROOT'] ;
 

?>
<!-- container starts -->
<style>
tr{float:left; padding:10px 0px;}
.full-width-page{padding:0px!important;}
.page-title { display:none !important; }
</style>
<div class="content full-width-page">
  <section class="section" style='padding:0px;'>
    <div class="container" style='margin: 0px;padding: 0px;width: 100%;'>
      <div class="row">
        <div class="col-md-12">
<?php if (have_posts()) : $count = 0; ?>
<?php while (have_posts()) : the_post(); $count++; 
    $pid = get_the_ID();
    ?>
<!-- col-mid starts -->

<?php 
                //$video = woo_get_embed('embed',561,325);
                //$image = woo_image('key=image&return=true&width=561&height=325&class=corner iradius5');
                //if(!empty($video)) { echo $video;}
                //elseif(!empty($image)) { echo '<div class="img-holder">'.$image.'</div>'; }?>
<!-- img-holder ends-->



<!-- top-bottom ends -->

<!--h2>
  <?php //the_title(); ?>
</h2-->
<p class="hr1">&nbsp;</p>
<?php
//echo $c_error;
if($c_error == '')
{
	if(isset($_GET['pageid']))
	{
    	$get_data = $_GET['pageid'];
    }
	else
	{
		$get_data = '';
	}
}
else
{
	$get_data = ''; 
	//echo $_POST['fname']; exit;
}
//echo $get_data."millan"; exit;	
	
        switch ($get_data)
    {
      case "":
        ?>
        <div id="test">
<?php the_content(); ?>
</div>

<script type="text/javascript">



    <!--


      

      // Save typing document.getElementById repeatedly.
      var id = function(elementid)
      {
        return document.getElementById(elementid);
      };

      // Hide Tables when page first loads.
      function setTables()
      {
        id("paymenttable").style.display = 'none';
        id("periodictable").style.display = <?php if($frequency == '3' || '6'){ echo 'block'; } else {echo 'display:none'; } ?>;
        id("creditcardtable").style.display = 'block';
        id("directentrytable").style.display = 'none';
        //id("creditflag").style.display = 'none';
        id("submittable").style.display = 'block';
        id("periodicpaymenttype").style.display = 'none';
        id("periodictype").style.display = 'none';
        id("startdate").style.display = 'none';
        id("paymentinterval").style.display = 'none';
        id("numberofpayments").style.display = 'none';
        id("visarecurring").style.display = 'none';

      }

      // Show request type table.
      function showRequestType(selection)
      { 
        switch(selection)
        {
        case "payment":
        id("paymenttable").style.display = 'block';
        id("periodictable").style.display = 'none';
        id("creditcardtable").style.display = 'block';
        id("directentrytable").style.display = 'none';
        id("submittable").style.display = 'none';
           break;
        case "periodic":
        id("periodictable").style.display = 'block';
        id("paymenttable").style.display = 'none';
        id("creditcardtable").style.display = 'block';
        id("directentrytable").style.display = 'none';
        id("submittable").style.display = 'block';
          break;
        case "echo":
        id("paymenttable").style.display = 'none';
        id("periodictable").style.display = 'none';
        id("creditcardtable").style.display = 'block';
        id("directentrytable").style.display = 'none';
          break;
        }
      }

      // Show payment type table.
      function showPaymentType(selection)
      {
        if (selection == "0" || selection == "4" || selection == "6" || selection == "10" || selection == "11")
        {
          id("creditcardtable").style.display = 'block';
          id("directentrytable").style.display = 'none';
        }
        else
        {
          id("creditcardtable").style.display = 'block';
          id("directentrytable").style.display = 'none';
        }
      }

      // Show the credit flag row.
      function showCreditFlag(selection)
      {
        if (selection == "yes")
        {
          //id("creditflag").style.display = 'block';
        }
        else
        {
          id("creditflag").style.display = 'none';
        }
      }

      // Show periodic Add table.
      function showAddOptions(selection)
      {
        if (selection == "yes")
        {
          id("periodicpaymenttype").style.display = 'block';
          id("periodictype").style.display = 'block';
          id("startdate").style.display = 'block';
          id("paymentinterval").style.display = 'block';
          id("numberofpayments").style.display = 'block';
          id("visarecurring").style.display = 'block';
          id("submittable").style.display = 'none';
        }
        else
        {
          id("periodicpaymenttype").style.display = 'none';
          id("periodictype").style.display = 'none';
          id("startdate").style.display = 'none';
          id("paymentinterval").style.display = 'none';
          id("numberofpayments").style.display = 'none';
          id("visarecurring").style.display = 'none';
          id("creditcardtable").style.display = 'block';
          id("directentrytable").style.display = 'none';
          showCreditFlag('no');
        }
      }

      // Show Submit table.
      function showSubmitTable()
      {
        id("submittable").style.display = 'block';
      }
      
      // Show Credit Card table.
      function showCreditCardTable()
      {
        id("creditcardtable").style.display = 'block';
        id("periodictable").style.display = 'none';
      }
      //-->
    </script>
<script Language="JavaScript" Type="text/javascript"><!--
function validateForm(theForm)
{
  
   

  if (theForm.title.value == "")
  {
    alert("Please enter a value for the \"Title\" field.");
    theForm.title.focus();
    return (false);
  }

  if (theForm.title.value.length < 2)
  {
    alert("Please enter at least 2 characters in the \"Title\" field.");
    theForm.title.focus();
    return (false);
  }
  
  if (theForm.fname.value == "")
  {
    alert("Please enter a value for the \"First Name\" field.");
    theForm.fname.focus();
    return (false);
  }

  if (theForm.fname.value.length < 2)
  {
    alert("Please enter at least 2 characters in the \"First Name\" field.");
    theForm.fname.focus();
    return (false);
  }
  
  if (theForm.last_name.value == "")
  {
    alert("Please enter a value for the \"Last Name\" field.");
    theForm.last_name.focus();
    return (false);
  }

  if (theForm.last_name.value.length < 2)
  {
    alert("Please enter at least 2 characters in the \"Last Name\" field.");
    theForm.last_name.focus();
    return (false);
  }
  
  if (theForm.han_email.value == "")
  {
    alert("Please enter a value for the \"Email\" field.");
    theForm.han_email.focus();
    return (false);
  }

  if (theForm.han_email.value.length < 8)
  {
    alert("Please enter at least 8 characters in the \"Email\" field.");
    theForm.han_email.focus();
    return (false);
  }
  
  if (theForm.captcha.value == "")
  {
    alert("Please enter a value for the \"captcha\" field.");
    theForm.fname.focus();
    return (false);
  }
  
  <!---aayush captch nill validation--->
   /*if (theForm.captcha.value == "")
  {
    alert("Please enter a value for the \"captcha\" field.");
    theForm.fname.focus();
    return (false);
  }
 
   var loggedIn = <?php //echo isset($_SESSION['securimage_code_value']) && $_SESSION['securimage_code_value'] == $_POST['captcha'] ? 'true' : 'false'; ?>;
   //alert(loggedIn);
   //var captcha = <?php //echo $_SESSION['securimage_code_value'];?>;
   //alert(captcha);
   
   if(loggedIn==false){
     
   alert(loggedIn);
     return (false);
     }*/
  
   <!--captcha validation ends here-->
  
  if (theForm.phone.value == "")
  {
    alert("Please enter a value for the \"Daytime Phone\" field.");
    theForm.phone.focus();
    return (false);
  }

  if (theForm.phone.value.length < 5)
  {
    alert("Please enter at least 5 characters in the \"Daytime Phone\" field.");
    theForm.phone.focus();
    return (false);
  }

  if (theForm.street.value == "")
  {
    alert("Please enter a value for the \"Street\" field.");
    theForm.street.focus();
    return (false);
  }

  if (theForm.street.value.length < 3)
  {
    alert("Please enter at least 3 characters in the \"Street\" field.");
    theForm.street.focus();
    return (false);
  }
  
  if (theForm.city.value == "")
  {
    alert("Please enter a value for the \"Suburb\" field.");
    theForm.city.focus();
    return (false);
  }

  if (theForm.city.value.length < 3)
  {
    alert("Please enter at least 3 characters in the \"Suburb\" field.");
    theForm.city.focus();
    return (false);
  }
  
  if (theForm.state.value == "")
  {
    alert("Please enter a value for the \"State\" field.");
    theForm.state.focus();
    return (false);
  }

  if (theForm.state.value.length < 2)
  {
    alert("Please enter at least 2 characters in the \"State\" field.");
    theForm.state.focus();
    return (false);
  }
  
  if (theForm.postcode.value == "")
  {
    alert("Please enter a value for the \"Postcode\" field.");
    theForm.postcode.focus();
    return (false);
  }

  if (theForm.postcode.value.length < 4)
  {
    alert("Please enter at least 4 characters in the \"Postcode\" field.");
    theForm.postcode.focus();
    return (false);
  }

  if (theForm.payment_amount.value == 0)
  {
    alert("Please enter an amount to donate.");
    theForm.payment_amount.focus();
    return (false);
  }
  if(isNaN(theForm.payment_amount.value))
  {
  alert("Amount must contain only numbers.");
    theForm.payment_amount.focus();
    return (false);
  }
  
 // alert(theForm.request_type[0].value);
  if(theForm.request_type[0].checked)
  {
    if(theForm.card_number.value == "")
    {
      alert("Please enter your card number.");
      theForm.card_number.focus();
      return (false);
    }
    if(theForm.card_cvv.value=="")
    {
      alert("Please enter your card cvv number.");
      theForm.card_cvv.focus();
      return (false); 
    }   
  } 
  //
  if(theForm.request_type[1].checked)
  {
    if(theForm.start_date.value == "")
    {
      alert("Please enter start date.");
      theForm.start_date.focus();
      return (false);
    }
    //alert(theForm.payment_interval.value);
    if(theForm.payment_interval.value=="-- Choose payment interval --")
    {
      alert("Please select a payment interval.");
      theForm.payment_interval.focus();
      return (false); 
    }
    if(theForm.number_of_payments.value=="")
    {
      alert("Please enter number of payments.");
      theForm.number_of_payments.focus();
      return (false);
    }
    if(theForm.number_of_payments.value < 2)
    {
      alert("Number of payment must not be less than 2.");
      theForm.number_of_payments.focus();
      return (false);
    }
    if(theForm.card_number.value == "")
    {
      alert("Please enter your card number.");
      theForm.card_number.focus();
      return (false);
    }
    
    if(theForm.card_cvv.value=="")
    {
      alert("Please enter your card cvv number.");
      theForm.card_cvv.focus();
      return (false); 
    }
    
      
  }
  /*if(theForm.request_type.value == "")
  {
    alert("Please select a donation type.");
    theForm.request_type.focus();
    return (false); 
  }*/

  return (true);
}

  function checkOption(name) {
    len = document.getElementById(name).length;
    i = 0;
    chosen = "none";
    
    for (i = 0; i < len; i++) {
    if (document.getElementById(name)[i].selected) {
    chosen = document.getElementById(name)[i].value;
    }
    }
    if (chosen == 'other') {
      other_box = name + "_other";
      document.getElementById(other_box).style.display = 'block';
    } else {
      other_box = name + "_other";
      document.getElementById(other_box).style.display = 'none';
    }
  
  } 

//--></script>

<body onLoad="setTables();">
<?php 

if ($testing == 'yes') {
  require ('customfiles/config-test.php');
} else {
  require ('customfiles/config.php');
}

    
?>

<style>
/*.vc_col-md-3 {width:19% !important;}*/

.vc_custom_1440733794249
{
  padding-top: 3%;
  padding-bottom: 3%;
  margin:0px;
}

.vc_custom_1442206671959
{
    background-color: #fff !important;
    padding-top: 6% ;
    margin: 0;
    padding-bottom: 3%;
}
#contactForm p
{
    clear: both;
    margin-bottom: 10px;
    float: left;
    width: 92%;
}
#contactForm label
{
   float:left;
}

#contactForm select
{
   float:right;
}

#contactForm input
{
   float:right;
}

#contactForm img
{
   float:right;
}


</style>




<form method="post" onSubmit="return validateForm(this)" name="form" id="contactForm" action="<?php bloginfo('url');?>/donate-now/?pageid=process" style="float: left;
    width: 100%;
    background-color: #f8f8f8;">
    <div class="form-colour">
      <div class="container">
    <div class="col-md-offset-3 col-md-4 form">
                
        <?php if($captcha_value=='wrong'){ ?>
            <p style="color:red">* Captcha you entered is wrong.Please try again.</p>
            <?php
             }elseif(isset($_GET['pageid']) && $_GET['pageid']=='process' && $captcha_value=='nill'){ ?>
                    <span style="color:red;font-size:16px;font-weight:bold;width:455px; vertical-align:top;">
                    *Please enter captcha value.
                    </span>
                 <?php } ?>
           
        <p style="margin-bottom: 55px;
    float: left;
    font-weight: 400;
    font-size: 18px;
    color: #262626;
    font-family: 'Raleway', Arial, sans-serif;font-weight: normal;">Personal Details</td></p>

        <p><label for="title title">Title: </label><span style="color: #f00;">*</span>
        <?php
			$title_val = $_REQUEST['title'];
		?>
        <select name="title" id="title" required >
            <option value="">Please select title</option>
            <option value="Mr" <?php if($title_val == 'Mr') { echo "selected='selected'"; } ?>>Mr</option>
            <option value="Mrs" <?php if($title_val == 'Mrs') { echo "selected='selected'"; } ?>>Mrs</option>
            <option value="Mr & Mrs" <?php if($title_val == 'Mr & Mrs') { echo "selected='selected'"; } ?>>Mr & Mrs</option>
            <option value="Ms" <?php if($title_val == 'Ms') { echo "selected='selected'"; } ?>>Ms</option>
            <option value="Mids" <?php if($title_val == 'Mids') { echo "selected='selected'"; } ?>>Miss</option>
            <option value="Dr" <?php if($title_val == 'Dr') { echo "selected='selected'"; } ?>>Dr</option>
        </select></p>
        <?php
        if(isset($_REQUEST['firstname'])){$firstname = $_REQUEST['firstname'];}else{$firstname = '';}
        if(isset($_REQUEST['lastname'])){$lastname = $_REQUEST['lastname'];}else{$lastname = '';}
        if(isset($_REQUEST['email'])){$email = $_REQUEST['email'];}else{$email = '';}
		if(isset($_REQUEST['fname'])){$firstname = $_REQUEST['fname'];}else{$firstname = '';}
        if(isset($_REQUEST['last_name'])){$lastname = $_REQUEST['last_name'];}else{$lastname = '';}
        if(isset($_REQUEST['han_email'])){$email = $_REQUEST['han_email'];}else{$email = '';}
        if(isset($_REQUEST['country'])){$country = $_REQUEST['country'];}else{$country = '';}
        if(isset($_REQUEST['amount'])){$amt_ = $_REQUEST['amount'];}else{$amount = '';}
		if(isset($_REQUEST['payment_amount'])){$amt_ = $_REQUEST['payment_amount']; }else{$amount = '';}
        if(isset($_REQUEST['currency'])){$currency = $_REQUEST['currency'];}else{$currency = '';}
        if(isset($_REQUEST['frequency'])){$frequency = $_REQUEST['frequency'];}else{$frequency = '';}
		if(isset($_REQUEST['request_type'])){ $frequency = $_REQUEST['request_type'];}else{$frequency = '';}
		
        ?>

        <p><label for="fname">First Name: <span style="color: #f00;">*</span></label><input type="text" name="fname" id="fname" size="40" class="inputbox" value="<?php echo $firstname; ?>" required /></p>

        <p><label for="last_name">Last Name: <span style="color: #f00;">*</span></label><input type="text" name="last_name" id="last_name" size="40" class="inputbox" value="<?php echo $lastname; ?>" required /></p>


        <p><label for="position">Position:</label><input type="text" name="position" id="position" size="40" class="inputbox" value="<?php echo $_REQUEST['position']; ?>" /></p>


        <p><label for="organisation">Organisation:</label><input type="text" name="organisation" id="organisation" size="40" class="inputbox" value="<?php echo $_REQUEST['organisation']; ?>" /></p>

        <p><label for="han_email">Email address: </label><span style="color: #f00;">*</span><input type="email" name="han_email" id="han_email" size="40" class="inputbox" value="<?php echo $email; ?>" required /></p>


        <p><label for="phone">Daytime Phone: </label>
        <input type="text" name="phone" id="phone" size="40" class="inputbox" value="<?php echo $_REQUEST['phone']; ?>" /></p>

        <p><label for="street">Street: </label><span style="color: #f00;">*</span><input type="text" name="street" id="street" size="40" class="inputbox" value="<?php echo $_REQUEST['street']; ?>" required /></p>

        <p><label for="city">Suburb: </label><span style="color: #f00;">*</span><input type="text" name="city" id="city" size="40" class="inputbox" value="<?php echo $_REQUEST['city']; ?>" required /></p>

        <p><label for="state">State: </label><span style="color: #f00;">*</span>
        	<?php
				$state_val = $_REQUEST['state'];
			?>
            <select id="state" name="state" required>
                <option value="">Please select region, state or province</option>
                <option value="Australian Capital Territory" title="Australian Capital Territory" <?php if($state_val == 'Australian Capital Territory') { echo "selected='selected'"; } ?>>Australian Capital Territory</option>
                <option value="New South Wales" title="New South Wales" <?php if($state_val == 'New South Wales') { echo "selected='selected'"; } ?>>New South Wales</option>
                <option value="Northern Territory" title="Northern Territory" <?php if($state_val == 'Northern Territory') { echo "selected='selected'"; } ?>>Northern Territory</option>
                <option value="Queensland" title="Queensland" <?php if($state_val == 'Queensland') { echo "selected='selected'"; } ?>>Queensland</option>
                <option value="South Australia" title="South Australia" <?php if($state_val == 'South Australia') { echo "selected='selected'"; } ?>>South Australia</option>
                <option value="Tasmania" title="Tasmania" <?php if($state_val == 'Tasmania') { echo "selected='selected'"; } ?>>Tasmania</option>
                <option value="Victoria" title="Victoria" <?php if($state_val == 'Victoria') { echo "selected='selected'"; } ?>>Victoria</option>
                <option value="Western Australia" title="Western Australia" <?php if($state_val == 'Western Australia') { echo "selected='selected'"; } ?>>Western Australia</option>
            </select>
        </p>

        <p><label for="postcode">Postcode: </label><span style="color: #f00;">*</span><input type="text" name="postcode" id="postcode" size="40" class="inputbox" value="<?php echo $_REQUEST['postcode']; ?>" required /></p>

        <p><input class="roundedOne" type="checkbox" name="signup" value="yes" checked style="width:5%;" /> <span style="width: 96%;float: left;font-size: 12px;margin-top: -18px;line-height: 15px;">Please send me informational material about Launch Housing</span></p>


        <!--<img src="<?php //bloginfo('template_directory'); ?>/captcha/securimage_show.php" id="captcha" style="display:block;" />
        <?php /* ?><a href="javascript:;" onclick="document.getElementById('captcha').src='<?php bloginfo('template_directory');?>/captcha/securimage_show.php?'+Math.random(); document.getElementById('security_code').focus();" id="change-image">Not readable? Change text.</a><?php */ ?>
            
        <input name="captcha" id="captcha" required title="Security Code" type="text" value="" class="mlfmrInpTxt requiredField" style="width:164px; margin-top:10px;" />-->
        <p><!--<img src="<?php bloginfo('template_url');?>/captcha_p3l/captcha.php" id="captcha" style="float:left;" /><input type="text" name="captcha" id="captcha-form" autocomplete="off" required />-->
          <p style="font-size: 12px;">Please check the box below to secure this page.</p>
        <fieldset style="float:left;">
            <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
            <script type="text/javascript"
                    src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>">
            </script>
            <p>What is <a href="https://support.google.com/recaptcha/?hl=en#6080904" target="_blank">reCAPTCHA</a>?</p>
        </fieldset>
        </p>
        <p><?php echo $c_error; ?></p>

        <!-- CHANGE TEXT LINK -->
        <!--<p style="padding-bottom:50px;"><a href="#!" onClick="
            document.getElementById('captcha').src='<?php bloginfo('template_url');?>/captcha_p3l/captcha.php?'+Math.random();
            document.getElementById('captcha-form').focus();"
            id="change-image">Not readable? Change text.</a></p>-->


        <div id="directentrytable" style="display:none;">
            <p id="directentrytable"><strong>Direct Entry Details</strong></p>

            <p id="directentrytable">BSB Number: <input type="text" name="bsb_number" size="7" maxlength="6" value="123456" /></p>


            <p id="directentrytable">Account Number:<input type="text" name="account_number" size="10" maxlength="9" value="123456789" /></p>

            <p id="directentrytable">Account Name:<input type="text" name="account_name" size="34" maxlength="32" value="Test" /></p>
        </div>

    </div>

    <div class="col-md-4 form">
        <p style="font-size: 18px;margin-top: -17px; color: #262626;font-family: 'Raleway', Arial, sans-serif;font-weight: normal; margin-bottom:47px;"><span style="margin-top: 19px;float: left;">Donation Details</span><img class="secure-pay-logo" src="<?php bloginfo('template_url');?>/img/securepay-logo.gif" alt="Securepay" style="float:right;     height: 52px; margin-top: 3px;"></p>

    
         <?php
            if($testing == 'yes') {
                ?>
                <input type="hidden" name="server" value="live" />
                <?php
            }
            else {
                ?>
                <input type="hidden" name="server" value="live" />
                <?php
            }
        ?>
        <input type="hidden" name="currency" value="<?php echo $currency; ?>" />
        <?php if($amt_){
                $orange =   "style='background:orange; font-size:15px;'";
            }
        ?>
                                
        <p>Amount:  <span style="color: #f00;">*</span><span class=“currency_type” style="float:right;"><?php echo $currency; ?></span><input type="text" name="payment_amount" class="amount" <?php echo @$orange ?> size="8" value="<?php echo $amt_; ?>" style="font-size:15px;" required /></p>

        <!--
        <tr>
            <td width="160">Put my donation towards:</td>
            <td>
                <select name="preference" style="width: 260px">
                    <option value="Where it is most needed">Where it is most needed</option>
                    <option value="Family Support">Family Support</option>
                    <option value="Children and youth support">Children and youth support</option>
                    <option value="School childrens education assistance">School children's education assistance</option>
                    <option value="emergency_relief">Emergency relief and material aid</option>
                    <option value="Outreach - sleeping rough">Outreach - "sleeping rough"</option>                          
                    <option value="Adult education and training">Adult education and training</option>
                </select>
            </td>
        </tr>
        <tr>
            <td width="160" valign="top">Which campaign/appeal are you donating to?</td>
            <td valign="top">
                <select name="campaign" id="campaign" onChange="checkOption('campaign')" style="width: 260px">
                    <option value="General Donations">General Donations</option>
                    <option value="Christmas Appeal">Christmas Appeal</option>
                    <option value="Launch Housingview - Autumn">Launch Housingview - Autumn</option>
                    <option value="Launch Housingview - Spring">Launch Housingview - Spring</option>
                    <option value="Winter Appeal">Winter Appeal</option>
                    <option value="other">Other - please specify</option>                           
                </select>
                &nbsp;<textarea rows="2" cols="20" name="campaign_other" id="campaign_other" style="display: none; width: 250px"></textarea>
            </td>
        </tr>
        -->
        <p><label for="prompt">What prompted you to make this donation?</label>

		<?php
			$prompt_val = $_REQUEST['prompt'];
		?>	
        <select name="prompt" id="prompt" onChange="checkOption('prompt')" style="width:100%;" >
            <option value disabled selected> -- select an option -- </option>
            <option value="I received a Launch Housing appeal" <?php if($prompt_val == 'I received a Launch Housing appeal') { echo "selected='selected'";} ?>>I received the Launch Housing appeal</option>
            <option value="I received the Newsletter" <?php if($prompt_val == 'I read the Autumn Newsletter') { echo "selected='selected'";} ?>>I received the Newsletter</option>
            <option value="I saw Launch Housing in the media" <?php if($prompt_val == 'I saw Launch Housing in the media') { echo "selected='selected'";} ?>>I saw Launch Housing in the media</option>
            <option value="I saw a Launch Housing staff member speak at an event" <?php if($prompt_val == 'I saw a Launch Housing staff member speak at an event') { echo "selected='selected'";} ?>>I saw a Launch Housing staff member speak at an event</option>
            <option value="I saw Launch Housing Community Service Announcement" <?php if($prompt_val == 'I saw Launch Housing Community Service Announcement') { echo "selected='selected'";} ?>>I saw Launch Housing's Community Service Announcement</option>
            <option value="I was referred to Launch Housing by a friend" <?php if($prompt_val == 'I was referred to Launch Housing by a friend') { echo "selected='selected'";} ?>>I was referred to Launch Housing by a friend</option>
            <option value="I Googled homelessness" <?php if($prompt_val == 'I Googled homelessness') { echo "selected='selected'";} ?>>I Googled "homelessness"</option>
            <option value="client" <?php if($prompt_val == 'client') { echo "selected='selected'";} ?>>I met a Launch Housing client</option>
            <option value="Launch Housing has supported my friends or family in the past" <?php if($prompt_val == 'Launch Housing has supported my friends or family in the past') { echo "selected='selected'";} ?>>Launch Housing has supported my friends or family in the past</option>
            <option value="I saw a Launch Housing advertisement" <?php if($prompt_val == 'I saw a Launch Housing advertisement') { echo "selected='selected'";} ?>>I saw a Launch Housing advertisement</option>
            <option value="I attended the mooting competition" <?php if($prompt_val == 'I attended the mooting competition') { echo "selected='selected'";} ?>>I attended the mooting competition</option>
            <option value="I saw Launch Housing on social media" <?php if($prompt_val == 'I saw Launch Housing on social media') { echo "selected='selected'";} ?>>I saw Launch Housing on social media</option>
            <option value="other" <?php if($prompt_val == 'other') { echo "selected='selected'";} ?>>Other - please specify</option>
        </select></p>

        &nbsp;<textarea rows="2" cols="20" name="prompt_other" id="prompt_other" style="display: none; width: 250px"><?php echo $_REQUEST['prompt_other']; ?></textarea>
        <p><label for="prompt">Type of donation <span style="color: #f00;">*</span></label> </p>

        <p>
          <span style="width: 50%;float: left;"><input type="radio" name="request_type" value="payment" onClick="showCreditCardTable(); showSubmitTable();"  <?php if($request_type != '3' || '6'){echo "checked";} ?> style="width: 10%;float: left;"/> <label style="margin-left: 10px;">One time donation </label></span>
            <span style="width: 50%;float: left;">
             <input type="radio" name="request_type" value="periodic" onClick="showRequestType('periodic'); showAddOptions('yes'); showCreditCardTable(); showSubmitTable();"  <?php if($frequency == '3'){echo "checked";} ?><?php if($frequency == '6'){echo "checked";} ?> style="width: 10%;float: left;" /><label style="margin-left: 10px;">Periodic donation</label>
            </span>
            </p>
        

        <div id="paymenttable" <?php if($request_type != 'OneTime' && $request_type != 'payment'){echo "style='display:none;'";} ?>>
            <strong style="float: left; margin-bottom: 15px;">Payment Details</strong>
            <p>Payment Type: <input type="hidden" name="payment_type" value="0"></p>

            <p>Payment Reference / Purchase Order Number: <br />
                                            <font size="2">
                                                &nbsp;&nbsp;-Refunds/Reversals/Completes: must match original payments ref.<br />
                                                &nbsp;&nbsp;-Direct Entry: 18 chars max.
                                            </font>
            <input type="text" name="payment_reference" size="20" maxlength= "18" value="referenceNumHere" /></p>

            <p>Transaction ID:<br />
            <font size="2">
                &nbsp;&nbsp;-Refund/Reversal payments only.
            </font>
            <input type="text" name="txnid" size="16" value="" /></p>
        </div>

        <div id="periodictable" <?php if($frequency == '3' || $frequency == '6'){echo "style='display:block'";}else{echo "style='display:none'";} ?>
           >
            <strong>Periodic Details <?php echo $frequency ?></strong>
                                        <input type="hidden" name="action_type" value="add" />
                                        <input type="hidden" name="periodic_payment_type" value="creditcard" />
                                        <input type="hidden" name="periodic_type" value="3" />

            <p><span style="font-size: 13px;float: left; width:39%;"><label for="start_date">Start Date: (dd/mm/yyy)</span><span style="color: #f00;">*</label></span><input type="text" name="start_date" id="start-date" class="date-pick"/></p>


            <p><span style="float: left; width: 39%; font-size:13px;"><label for="payment_interval">Payment Interval:</span><span style="color: #f00;">*</label></span>
                <select name="payment_interval" id="payment_interval"> 
                    <option selected>-- Choose payment interval --</option>
                    <option value="1">Weekly</option>
                    <option value="2">Fortnightly</option>
                    <option value="3" <?php if($frequency == '3'){echo "selected";} ?>>Monthly</option>
                    <option value="4">Quarterly</option>
                    <option value="5">Half Yearly</option>
                    <option value="6" <?php if($frequency == '6'){echo "selected";} ?>>Annually</option>
                </select>
            </p>


            <p><span style="float: left;width: 39%; font-size:13px;">Number of Payments:</span> <span style="color: #f00;">*</span>
            <input type="text" name="number_of_payments" size="5" maxlength="3" />
        </div>

        <div id="creditcardtable">
            <p><strong style="float: left; margin-bottom: 15px;">Credit Card Details</strong>
            <label for="number_of_payments" style="float: left;clear: both;">Card No:<span style="color: #f00;float: left;">*</label></span>
            <input name="card_number" type="text" size="21" maxlength="19"  /><br/>
            <span style="font-size: 10px;float: left;width: 100%;clear: both;text-align: right;">Please note we only accept Visa and Mastercard</span></p>

            <p><label for="card_CVV">CVV No: </label><span style="color: #f00;">*</span>
            <input type="text" name="card_cvv" size="5" maxlength="4" /></p>

            <p>
              <label for="card_expiry_month" style="width: 100%;float: left;"><span style="float: left;width: 40%;">Exp:</span>
        <span>
                 <select name="card_expiry_year" value="Year" class="card_exp_year" style="width: 26% !important;"> 
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                </select>


                <select name="card_expiry_month" value="Month" class="card_exp_month" style="width: 25%;"> 
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>

                </span>

            </p>
        </div>

        <div id="submittable" style="float: left;width: 100%;">
            <p><input type="submit" value="Submit Donation" name="submit" class="submit plainButton" style="width:100%;" />
            </p>

            <p>Click the submit button ONCE only. <br />Your donation may take a few moments to process.</p>
        </div>

        <!--tr>
                <td colspan="2" style="font-style:italic;">If you are donating using a Visa card and receive the error message 'Expired Card-Pick Up', please contact Andy Grant, Fundraising Specialist on 1800 720 660, he can securely process your payment over the phone. It is a known issue with Visa and we are working with them to fix it.</td>
        </tr-->
    </div>
    </div>
    </div>
</form>

  
<?php 
    echo do_shortcode("[stag_sidebar id='donation-form-text']") ;

        break;

    case "process":

    
        if ($testing == 'yes') {
      require ('customfiles/config-test.php');
    } else {
      require ('customfiles/config.php');
    }

        $messageID = generateMessageID();  // random generated unique messageID to pass to securepay
        $timestamp = getGMTtimestamp();
        $transact_id = 'W-'.substr($_POST['last_name'], 0, 2) . substr($_POST['fname'], 0, 2) .
            substr($timestamp, 0, 12);// Make the transact_id timestamp the unique reference number to match with securepay transactions

        $merchantID7Char = $merchant_id;
        $merchantID5Char = substr($merchant_id, 0, 5);
 
        $start_date = substr($_POST['start_date'], 6, 4) . substr($_POST['start_date'], 3, 2) . substr($_POST['start_date'], 0, 2);


        if ($_POST["visa_recurring"] == "on")
        {
            $visaRecurring = "yes";
        }
        else
        {
            $visaRecurring = "no";
        }

        switch ($_POST["request_type"]) // Select echo, payment or periodic.
        {
        case "payment":
    
      $host = "www.securepay.com.au/xmlapi/payment";
      //$host = "https://api.securepay.com.au/xmlapi/payment";
      $xmlmessage = setPaymentCreditCard($merchantID7Char, $timestamp, $transaction_password, $transact_id);
        //echo  "millan".$xmlmessage; exit;
      
      $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/donate/".$transact_id.".txt","w");
      fwrite($fp,$xmlmessage);
      fclose($fp);
      
         /*   if ($_POST["server"] == "live")
            {

                    $host = "www.securepay.com.au/xmlapi/payment";
          //$host = "https://api.securepay.com.au/xmlapi/payment";
                    $xmlmessage = setPaymentCreditCard($merchantID7Char, $timestamp, $transaction_password, $transact_id);
                //  echo  $xmlmessage;
        
          $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/donate/".$transact_id.".txt","w");
          fwrite($fp,$xmlmessage);
          fclose($fp);
            }
            else // Test Server.
            {
        $host = "www.securepay.com.au/test/payment";
        //$host = "http://test.securepay.com.au/xmlapi/payment";
        $xmlmessage = setPaymentCreditCard($merchantID7Char, $timestamp, $transaction_password, $transact_id);
        echo  $xmlmessage."<br/><br/>";
          
        //$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "clients/launchhousing/wp-content/uploads/donate/".$transact_id.".txt","w");
        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/donate/".$transact_id.".txt","w");
        fwrite($fp,$xmlmessage);
        fclose($fp);
        
        
        
            }*/
            break;

        case "periodic":
      $host = "www.securepay.com.au/xmlapi/periodic";
    /*        if ($_POST["server"] == "live")
            {
                $host = "www.securepay.com.au/xmlapi/periodic";
            }
            else // Test Server.
            {
                $host = "www.securepay.com.au/test/periodic";
            }
*/
            switch ($_POST["action_type"])
            {
            case "add":
                switch ($_POST["periodic_type"])
                {
                    case "3"://Calendar Based Periodic Payment
                        if ($_POST["periodic_payment_type"] == "creditcard")
                        {
                            $xmlmessage = setPeriodicCreditCardPayment($merchantID7Char, $timestamp, $visaRecurring,
                            $transaction_password, $start_date, $transact_id);
                        }
                        else //Direct Entry
                        {
                            $xmlmessage = setPeriodicDirectEntryPayment($merchantID5Char, $timestamp, $transaction_password);
                        }
                        break;

                }
                break;

            }
            break;

        }

        $response = openSocket($host, $xmlmessage);
//print_r($response); exit;
        $xmlres = array();
        $xmlres = makeXMLTree($response);
    
    //var_dump($xmlres);
    
        switch ($_POST["request_type"])
            //Print to screen the XML response from SecurePay.
        {
            case "payment":
     
                $messageID = trim($xmlres[SecurePayMessage][MessageInfo][messageID]);
                if ($messageID != null)
                {
                    if ($testing == 'yes') {echo "<p>messageID = " . $messageID . "</p>";}
                }

                $messageTimestamp = trim($xmlres[SecurePayMessage][MessageInfo][messageTimestamp]);
                if ($messageTimestamp != null)
                {
                  if ($testing == 'yes') {echo "<p>messageTimestamp = " . $messageTimestamp . "</p>";}
                }

                $apiVersion = trim($xmlres[SecurePayMessage][MessageInfo][apiVersion]);
                if ($apiVersion != null)
                {
                   if ($testing == 'yes') {echo "<p>apiVersion = " . $apiVersion . "</p>";}
                }

                $RequestType = $_POST["request_type"]; //trim($xmlres[SecurePayMessage][RequestType]);
                if ($RequestType != null)
                {
                   if ($testing == 'yes') {echo "<p>RequestType = " . $RequestType . "</p>";}
                }

                $merchantID = trim($xmlres[SecurePayMessage][MerchantInfo][merchantID]);
                if ($merchantID != null)
                {
                  if ($testing == 'yes') {echo "<p>merchantID = " . $merchantID . "</p>";}
                }

                $statusCode = trim($xmlres[SecurePayMessage][Status][statusCode]);
                if ($statusCode != null)
                {
                  if ($testing == 'yes') { echo "<p>statusCode = " . $statusCode . "</p>";}
                }

                $statusDescription = trim($xmlres[SecurePayMessage][Status][statusDescription]);
                if ($statusDescription != null)
                {
                  if ($testing == 'yes') {echo "<p>statusDescription = " . $statusDescription . "</p>";}
                }

                $txnType = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][txnType]);
                if ($txnType != null || $txnType != SECUREPAY_TXN_ANTIFRAUD_PAY || $txnType != SECUREPAY_TXN_ANTIFRAUD_CHECK)
                {
                    if ($testing == 'yes') {echo "<p>txnType = " . $txnType . "</p>";}
                }


                $txnSource = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][txnSource]);
                if ($txnSource != null)
                {
                   if ($testing == 'yes') {echo "<p>txnSource = " . $txnSource . "</p>";}
                }

                $amount = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][amount]);
                if ($amount != null)
                {
                   if ($testing == 'yes') {echo "<p>amount = " . $amount . "</p>";}
                }

                $currency = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][currency]);
                if ($currency != null)
                {
                   if ($testing == 'yes') {echo "<p>currency = " . $currency . "</p>";}
                }

                $purchaseOrderNo = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][purchaseOrderNo]);
                if ($purchaseOrderNo != null)
                {
                   if ($testing == 'yes') {echo "<p>purchaseOrderNo = " . $purchaseOrderNo . "</p>";}
                }

                $approved = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][approved]);
                if ($approved != null)
                {
                   if ($testing == 'yes') {echo "<p>approved = " . $approved . "</p>";}//else{echo "<p>approved = " . $approved . "</p>";}
                }
        
        		if ($txnType != null || $txnType != SECUREPAY_TXN_ANTIFRAUD_PAY || $txnType != SECUREPAY_TXN_ANTIFRAUD_CHECK) {

                  $responseCode = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][responseCode]);
        
        		}
        		else 
          			$responseCode = null;
                if ($responseCode != null)
                {
                    if ($testing == 'yes') {echo "<p>responseCode = " . $responseCode . "</p>";}
                }

                $responseText = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][responseText]); 
                if ($responseText != null)
                {
                  if ($testing == 'yes') {echo "<p>responseText = " . $responseText . "</p>";}
                }

                $settlementDate = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][settlementDate]);
                if ($settlementDate != null)
                {
                   if ($testing == 'yes') {echo "<p>settlementDate = " . $settlementDate . "</p>";}
                }

                $txnID = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][txnID]);
                if ($txnID != null)
                {
                    if ($testing == 'yes') {echo "<p>txnID = " . $txnID . "</p>";}
                }

                $preauthID = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][preauthID]);
                if ($preauthID != null)
                {
                   if ($testing == 'yes') {echo "<p>preauthID = " . $preauthID . "</p>";}
                }

                $pan = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][CreditCardInfo][pan]);
                if ($pan != null)
                {
                  if ($testing == 'yes') {echo "<p>pan = " . $pan . "</p>";}
                }

                $expiryDate = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][CreditCardInfo][expiryDate]);
                if ($expiryDate != null)
                {
                   if ($testing == 'yes') {echo "<p>expiryDate = " . $expiryDate . "</p>";}
                }

                $cardType = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][CreditCardInfo][cardType]);
                if ($cardType != null)
                {
                  if ($testing == 'yes') {echo "<p>cardType = " . $cardType . "</p>";}
                }

                $cardDescription = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][CreditCardInfo][cardDescription]);
                if ($cardDescription != null)
                {
                  if ($testing == 'yes') {echo "<p>cardDescription = " . $cardDescription . "</p>";}
                }

                $bsbNumber = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][DirectEntryInfo][bsbNumber]);
                if ($bsbNumber != null)
                {
                   if ($testing == 'yes') {echo "<p>bsbNumber = " . $bsbNumber . "</p>";}
                }

                $accountNumber = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][DirectEntryInfo][accountNumber]);
                if ($accountNumber != null)
                {
                    if ($testing == 'yes') {echo "<p>accountNumber = " . $accountNumber . "</p>";}
                }

                $accountName = trim($xmlres[SecurePayMessage][Payment][TxnList][Txn][DirectEntryInfo][accountName]);
                if ($accountName != null)
                {
                   if ($testing == 'yes') {echo "<p>accountName = " . $accountName . "</p>";}
                }

                break;

            case "periodic":
                $messageID = trim($xmlres[SecurePayMessage][MessageInfo][messageID]);
                if ($messageID != null)
                {
                    if ($testing == 'yes') {echo "<p>messageID = ".$messageID."</p>";}
                }

                $messageTimestamp = trim($xmlres[SecurePayMessage][MessageInfo][messageTimestamp]);
                if ($messageTimestamp != null)
                {
                    if ($testing == 'yes') {echo "<p>messageTimestamp = ".$messageTimestamp."</p>";}
                }

                $apiVersion = trim($xmlres[SecurePayMessage][MessageInfo][apiVersion]);
                if ($apiVersion != null)
                {
                    if ($testing == 'yes') {echo "<p>apiVersion = ".$apiVersion."</p>";}
                }

                $RequestType = $_POST["request_type"]; //trim($xmlres[SecurePayMessage][RequestType]);
                if ($RequestType != null)
                {
                    if ($testing == 'yes') {echo "<p>RequestType = ".$RequestType."</p>";}
                }

                $merchantID = trim($xmlres[SecurePayMessage][MerchantInfo][merchantID]);
                if ($merchantID != null)
                {
                    if ($testing == 'yes') {echo "<p>merchantID = ".$merchantID."</p>";}
                }

                $statusCode = trim($xmlres[SecurePayMessage][Status][statusCode]);
                if ($statusCode != null)
                {
                    if ($testing == 'yes') {echo "<p>statusCode = ".$statusCode."</p>";}
                }

                $statusDescription = trim($xmlres[SecurePayMessage][Status][statusDescription]);
                if ($statusDescription != null)
                {
                    if ($testing == 'yes') {echo "<p>statusDescription = ".$statusDescription."</p>";}
                }

                $actionType = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][actionType]);
                if ($actionType != null)
                {
                    if ($testing == 'yes') {echo "<p>actionType = ".$actionType."</p>";}
                }

                $clientID = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][clientID]);
                if ($clientID != null)
                {
                    if ($testing == 'yes') {echo "<p>clientID = ".$clientID."</p>";}
                }

                $responseCode = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][responseCode]);
                if ($responseCode != null)
                {
                    if ($testing == 'yes') {echo "<p>responseCode = ".$responseCode."</p>";}
                }

                $responseText = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][responseText]);
                if ($responseText != null)
                {
                    if ($testing == 'yes') {echo "<p>responseText = ".$responseText."</p>";}
                }

                $successful = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][successful]);
                if ($successful != null)
                {
                    if ($testing == 'yes') {echo "<p>successful = ".$successful."</p>";}
                }

                $pan = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][CreditCardInfo][pan]);
                if ($pan != null)
                {
                    if ($testing == 'yes') {echo "<p>pan = ".$pan."</p>";}
                }

                $expiryDate = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][CreditCardInfo][expiryDate]);
                if ($expiryDate != null)
                {
                    if ($testing == 'yes') {echo "<p>expiryDate = ".$expiryDate."</p>";}
                }

                $recurringFlag = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][CreditCardInfo][recurringFlag]);
                if ($recurringFlag != null)
                {
                    if ($testing == 'yes') {echo "<p>recurringFlag = ".$recurringFlag."</p>";}
                }

                $cardType = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][CreditCardInfo][cardType]);
                if ($cardType != null)
                {
                    if ($testing == 'yes') {echo "<p>cardType = ".$cardType."</p>";}
                }

                $cardDescription = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][CreditCardInfo][cardDescription]);
                if ($cardDescription != null)
                {
                    if ($testing == 'yes') {echo "<p>cardDescription = ".$cardDescription."</p>";}
                }

                $bsbNumber = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][DirectEntryInfo][bsbNumber]);
                if ($bsbNumber != null)
                {
                    if ($testing == 'yes') {echo "<p>bsbNumber = ".$bsbNumber."</p>";}
                }

                $accountNumber = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][DirectEntryInfo][accountNumber]);
                if ($accountNumber != null)
                {
                    if ($testing == 'yes') {echo "<p>accountNumber = ".$accountNumber."</p>";}
                }

                $accountName = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][DirectEntryInfo][accountName]);
                if ($accountName != null)
                {
                    if ($testing == 'yes') {echo "<p>accountName = ".$accountName."</p>";}
                }

                $creditFlag = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][DirectEntryInfo][creditFlag]);
                if ($creditFlag != null)
                {
                    if ($testing == 'yes') {echo "<p>creditFlag = ".$creditFlag."</p>";}
                }

                $amount = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][amount]);
                if ($amount != null)
                {
                    if ($testing == 'yes') {echo "<p>amount = ".$amount."</p>";}
                }

                $txnID = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][txnID]);
                if ($txnID != null)
                {
                    if ($testing == 'yes') {echo "<p>txnID = ".$txnID."</p>";}
                }

                $settlementDate = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][settlementDate]);
                if ($settlementDate != null)
                {
                    if ($testing == 'yes') {echo "<p>settlementDate = ".$settlementDate."</p>";}
                }

                $currency = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][currency]);
                if ($currency != null)
                {
                    if ($testing == 'yes') {echo "<p>currency = ".$currency."</p>";}
                }

                $periodicType = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][periodicType]);
                if ($periodicType != null)
                {
                    if ($testing == 'yes') {echo "<p>periodicType = ".$periodicType."</p>";}
                }

                $paymentInterval = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][paymentInterval]);
                if ($paymentInterval != null)
                {
                    if ($testing == 'yes') {echo "<p>paymentInterval = ".$paymentInterval."</p>";}
                }

                $startDate = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][startDate]);
                if ($startDate != null)
                {
                    if ($testing == 'yes') {echo "<p>startDate = ".$startDate."</p>";}
                }

                $endDate = trim($xmlres[SecurePayMessage][Periodic][PeriodicList][PeriodicItem][endDate]);
                if ($endDate != null)
                {
                    if ($testing == 'yes') {echo "<p>endDate = ".$endDate."</p>";}
                }
                break;
    }
    
    get_header();
  //$testing = 'yes';
  
    if ($testing == 'yes') {
      $responseText = 'Successful';
      $approved = 'Yes';
    }

     $amount = $_POST['payment_amount']; 

      // N.D.
    /*
    if ($_POST["campaign"] == 'other') {
      $_POST["campaign"] = 'other - '.$_POST["campaign_other"];
    }
    */
    if ($_POST["prompt"] == 'other') {
      $_POST["prompt"] = 'other - '.$_POST["prompt_other"];
    }
//echo $responseText." ".$approved; exit;
        if ($responseText == 'Successful' || $responseText == 'Approved' || $approved == 'Yes')
        {
      $paymentInterval = $_POST['payment_interval'];
      $adj_amount = number_format($amount,2);
            //$adj_amount = $amount / 100;   // store as 2 decimal places
            if ($paymentInterval == 1) {
        $paymentInterval_formatted = 'Weekly';
      } elseif ($paymentInterval == 2) {
        $paymentInterval_formatted = 'Fortnightly';
      } elseif ($paymentInterval == 3) {
        $paymentInterval_formatted = 'Monthly';
      } elseif ($paymentInterval == 4) {
        $paymentInterval_formatted = 'Quarterly';
      } elseif ($paymentInterval == 5) {
        $paymentInterval_formatted = 'Half Yearly';
      } elseif ($paymentInterval == 6) {
        $paymentInterval_formatted = 'Annually';
      }  
            
            $date = substr($timestamp, 0, 12);
      
      $date_for_db = date("Y-m-d");
            
            $sql = "INSERT INTO wp_donors 
          SET
          donor_date = '".$date_for_db."',
          donor_title = '".$_POST["title"]."',
          donor_fname = '".$_POST["fname"]."',
          donor_lname = '".$_POST["last_name"]."',
          donor_email = '".$_POST["han_email"]."',
          donor_position = '".$_POST["position"]."', 
          donor_organisation = '".$_POST["organisation"]."', 
          donor_day_phone = '".$_POST["phone"]."', 
          donor_street = '".$_POST["street"]."', 
          donor_suburb = '".$_POST["city"]."',
          donor_state = '".$_POST["state"]."',
          donor_postcode = '".$_POST["postcode"]."',
          donor_send_info = '".$_POST["signup"]."',
          donor_amount = '".$_POST['payment_amount']."',
          donor_temptation = '".$_POST["prompt"]."',
          donor_no_payments = '".$_POST["number_of_payments"]."',
          donor_donation_type = '".$RequestType."',
          donor_payment_interval = '".$paymentInterval_formatted."',
          donor_start_date = '".$startDate."',
          donor_end_date = '".$endDate."',
          donor_approved = '".$responseText."',
          donor_transaction_id = '".$transact_id."'
          ";        
      
            if ($testing != 'yes') {
            $insert = mysql_query($sql);
            }
            
            
                                                            echo "
<div class='vc_container_inner container'>

  <div  class='vc_row wpb_row vc_row_stcontent vc_row-fluid vc_custom_1440733794249 donate-thanks-container'>
    <div class='vc_col-md-3 wpb_column vc_column_container'>
      <div class='wpb_wrapper'> 
        <div class='wpb_text_column wpb_content_element'>
          <div class='wpb_wrapper'>
            <img src='http://www.launchhousing.org.au/wp-content/uploads/2015/10/THANKYOU-STAMP-optimised1.png' alt='Thank you sticker'>
          </div>
        </div> 
      </div> 
    </div>

    <div class='vc_col-md-9 wpb_column vc_column_container'>
      <div class='wpb_wrapper'> 
        <div class='wpb_text_column wpb_content_element '>
          <div class='wpb_wrapper'>
            <p>Thank you for donating.</p>
            <p>Your donation directly benefits the people who are most in need of your support.</p>
            <p>You are making a real difference to the lives of some of the most vulnerable people in Victoria.</p>
            <p>You are helping us to end homelessness.</p>
            <p>Thank you.</p>
            <p>A receipt has been emailed to you automatically. If you have any questions about your donation, or you would like to cancel a recurring payment, please call <a href='tel:1800720660' target='_parent'>1800 720 660</a>, quoting your transaction ID of <b>'" .
                                                                     $transact_id . "'</b>.</p>
          </div> 
        </div> 
      </div> 
    </div> 
  </div>

</div> 

";
            
// N.D. START CREATE PDF //
$donation_amount = number_format($_POST['payment_amount'],2);  
         
/*require_once("fpdf/fpdf.php");
  
  $pdf = new FPDF('P', 'pt', 'A4');
  $pdf->AddPage();
  $pdf->SetFont('Helvetica','',12);
  $pdf->SetTextColor(35,31,32);
    
  if ($RequestType == 'payment') 
  {
                                                                $pdf->Image(get_bloginfo('template_url') . '/images/newpdflogo.png', 375, 15, 200);

                                                                $pdf->Cell(0, 90, "", 0, 2);

                                                                $pdf->MultiCell(0, 15, "" . $_POST["fname"] . " " . $_POST["last_name"] . " \n" . $_POST["street"] . " \n" . $_POST["city"] . " " . $_POST["state"] . " " . $_POST["postcode"] . "");

                                                                $pdf->Cell(0, 20, "", 0, 2);

                                                                $pdf->MultiCell(455, 15, "Dear " . $_POST["title"] . " " . $_POST["last_name"] . ",\n
On behalf of all of us at Launch Housing, I would like to thank you for your donation of $" . $donation_amount . "  to help Launch Housing and our work with Victorians experiencing housing crisis. People of all different ages and background experience homelessness and find themselves in need of support.\n
Thankfully, your generosity will help provide support for those people experiencing housing crisis, by providing a safe and secure place to sleep. Your gift may be used to help families purchase basic necessities such as clothing, groceries and school materials for children.\n 
Our work also focuses on empowering people to break the cycle of homelessness by assisting clients with employment services, training and education.\n
Launch Housing has been providing support to people experiencing homelessness for nearly 50 years. It is only with your help that we are able to continue our work so once again, thank you from all the clients and staff at Launch Housing.\n
");
                                                                $y1 = $pdf->GetY() + 5;
                                                                $pdf->Image(get_bloginfo('template_url') . '/images/signature.png', 30, $y1, 0);

                                                                $pdf->Cell(0, 55, "", 0, 2);
//$pdf->Image('signature.png',30,$y1,0);
                                                                $pdf->MultiCell(455, 15, "Kind Regards,
Tony Keenan
Chief Executive Officer");

                                                                if ($_POST["organisation"] != '') {
                                                                    $received = $_POST["organisation"];
                                                                } else {
                                                                    $received = $_POST["fname"] . " " . $_POST["last_name"];
                                                                }

                                switch ($_POST["campaign"]) {
                                                                  case "general":
                                                                  $particulars = "General Donation";
                                                                  break;
                                                                  case "christmas_appeal":
                                                                  $particulars = "Christmas Appeal";
                                                                  break;
                                                                  case "launchhousingview-autumn":
                                                                  $particulars = "launchhousingview - Autumn";
                                                                  break;
                                                                  case "launchhousingview-spring":
                                                                  $particulars = "launchhousingview - Spring";
                                                                  break;
                                                                  case "winter_appeal":
                                                                  $particulars = "Winter Appeal";
                                                                  break;
                                                                  default:
                                                                  $particulars = $_POST["campaign"];
                                                                }

                                $wi = 460;

                                                                $pdf->SetXY(35, 535);
                                                                $pdf->Cell(0, 40, "", 0, 2);
                                                                $pdf->SetFont("Arial", "B", 12);
                                                                $pdf->Cell($wi, 14, "launchhousing ", 0, 2, 'C');
                                                                $pdf->SetFont("Arial", "", 12);
                                                                $pdf->Cell($wi, 14, "ABN: 89 742 307 083", 0, 2, 'C');
                                                                $pdf->SetFont("Arial", "B", 12);
                                $pdf->Cell($wi, 14, "Official Receipt Number: $purchaseOrderNo", 0, 2, 'C');
                                                                //$pdf->Cell($wi, 14, "Official Receipt Number: W-UdAy201208090608", 0, 2, 'C');
                                                                $pdf->SetFont("Arial", "B", 10);
                                                                $pdf->Cell(0, 14, "", 0, 2);
                                                                $y1 = $pdf->GetY();
                                                                $x1 = 110;
                                                                $pdf->SetXY($x1, $y1);

                                                                $pdf->Cell(80, 18, "DATE", 1, 0, 'C');
                                                                $pdf->Cell(160, 18, "RECEIVED FROM", 1, 0, 'C');
                                                                //$pdf->Cell(150,18,"PARTICULARS",1,0,'C');
                                                                $pdf->Cell(70, 18, "AMOUNT", 1, 1, 'C');
                                                                $pdf->SetFont("Arial", "", 10);

                                                                $y1 = $pdf->GetY();
                                                                $x1 = 110;
                                                                $pdf->SetXY($x1, $y1);
                                                                $pdf->MultiCell(80, 18, date('j/m/Y'), 1, 'C');
                                                                $x1 = $x1 + 80;
                                                                $pdf->SetXY($x1, $y1);
                                                                $pdf->MultiCell(160, 18, "$received", 1, 'C');
                                                                $x1 = $x1 + 160;
                                                                $pdf->SetXY($x1, $y1);
                                                                //$pdf->MultiCell(150,18,"$particulars",1,'C');
                                                                //$x1=$x1+150;
                                                                //$pdf->SetXY($x1, $y1);
                                                                $pdf->MultiCell(70, 18, "$" . $donation_amount, 1, 'C');
  
  $pdf->SetFont('Helvetica','B',7);
  $pdf->SetTextColor(120,120,120);
  $pdf->SetXY(490, 340);
  $pdf->MultiCell(95,9, "Administration Office", 0, 'R');
  
  $pdf->SetFont('Helvetica','',7);
  $pdf->SetXY(490, 350);
  $pdf->MultiCell(95,9, "52 Haig St
  South Melbourne
  Vic 3205
  PO Box 1016
  South Melbourne
  Vic 3205
  T 03 9699 6388
    F 03 9699 6790", 0, 'R');
  
  $pdf->SetFont('Helvetica','B',7);
  $pdf->SetXY(490, 430);
  $pdf->MultiCell(95,9, "Cheltenham", 0, 'R');
  
  $pdf->SetFont('Helvetica','',7);
  $pdf->SetXY(490, 440);
  $pdf->MultiCell(95,9, "Level 1
  11 Chesterville Road
  Cheltenham VIC 3192
  PO Box 135
  Southland VIC 3192
  T 03 9556 5777
  F 03 9584 9980", 0, 'R');
  
  $pdf->SetFont('Helvetica','B',7);
  $pdf->SetXY(490, 510);
  $pdf->MultiCell(95,9, "Dandenong \"Bob's Place\"", 0, 'R'); 
  
  $pdf->SetFont('Helvetica','',7);
  $pdf->SetXY(490, 520);
  $pdf->MultiCell(95,9, "35 Robinson Street
  Dandenong VIC 3175
  Postal Suit 75
  MBE 245 Thomas Street
  Dandenong VIC 3175
  T 1800 802 398
  T 03 9792 0750
  F 03 9792 0751", 0, 'R');
  
  $pdf->SetFont('Helvetica','B',7);
  $pdf->SetXY(490, 600);
  $pdf->MultiCell(95,9, "East St Kilda", 0, 'R'); 
  
  $pdf->SetFont('Helvetica','',7);
  $pdf->SetXY(490, 610);
  $pdf->MultiCell(95,9, "346 Dandenong Road
  East St Kilda VIC 3182
  T 03 9525 8499
  F 03 9525 8590", 0, 'R');
  
  $pdf->SetFont('Helvetica','B',7);
  $pdf->SetXY(490, 655);
  $pdf->MultiCell(95,9, "Fitzroy", 0, 'R'); 
  
  $pdf->SetFont('Helvetica','',7);
  $pdf->SetXY(490, 665);
  $pdf->MultiCell(95,9, "Mezzanine Floor
  145 Smith Street
  Fitzroy VIC 3065
  PO Box 1767
  Collingwood BC VIC 3066
  T 03 9288 9800
  F 03 9288 9899", 0, 'R');
  
  $pdf->SetFont('Helvetica','B',7);
  $pdf->SetXY(490, 735);
  $pdf->MultiCell(95,9, "Southbank", 0, 'R');
  
  $pdf->SetFont('Helvetica','',7);
  $pdf->SetXY(490, 745);
  $pdf->MultiCell(95,9, "52 Haig Street
  South Melbourne VIC 3205
  T 03 9699 4566
  F 03 9682 2070", 0, 'R');

  $pdf->Image(get_bloginfo('template_url') . '/images/newpdffooterlogo.png', 30, 760, 50);
  $pdf->Image(get_bloginfo('template_url') . '/images/newpdffooter.png', 205, 790, 380);
  }
  else
  {

                                                                $pdf->Image(get_bloginfo('template_url') . '/images/hannover-header-pdf.jpg', 20, 15, 420);
                                                                //$pdf->Image(get_bloginfo('template_url') . '/images/han-logo.jpg', 375, 15, 200);

                                                                $pdf->Cell(0, 90, "", 0, 2);

                                                                $pdf->MultiCell(0, 15, "" . $_POST["fname"] . " " . $_POST["last_name"] . " \n" . $_POST["street"] . " \n" . $_POST["city"] . " " . $_POST["state"] . " " . $_POST["postcode"] . "");

                                                                $pdf->Cell(0, 20, "", 0, 2);

                                                                $frequency["1"] = "Weekly";
                                                                $frequency["2"] = "Fortnightly";
                                                                $frequency["3"] = "Monthly";
                                                                $frequency["4"] = "Quarterly";
                                                                $frequency["5"] = "Half Yearly";
                                                                $frequency["6"] = "Annually";
                                                                $pdf->MultiCell(0, 15, "Dear " . $_POST["title"] . " " . $_POST["last_name"] . ",\n
I would like to take this opportunity to thank you for your gift of $" . $donation_amount . " " . $frequency[$_POST['payment_interval']] . " until " . substr($endDate, -2) . "/" . substr($endDate, 4, 2) . "/" . substr($endDate, 0, 4) . " to support Launch Housing and our work with people experiencing homelessness. Your ongoing generosity will assist those who are most in need within our community.

Your pledge is enabling Launch Housing to help young families purchase clothing, footwear and school materials for their children, while some of our more senior clients will be able to purchase much needed work wear for new jobs, receive assistance with course fees and support with transport to sustain employment, meet appointments and attend interviews.

A tax receipt which includes details of your donations for the entire financial year will be forwarded to you in July.

It is through your support that Launch Housing can continue to provide practical, long term solutions and assistance to Victorians experiencing homelessness.

Once again, thank you for your support; it is greatly appreciated by all the clients and staff at Launch Housing.");
                                                                $y1 = $pdf->GetY() + 5;
                                                                $pdf->Image(get_bloginfo('template_url') . '/images/signature.png', 30, $y1, 0);

                                                                $pdf->Cell(0, 55, "", 0, 2);
//$pdf->Image('signature.png',30,$y1,0);
                                                                $pdf->MultiCell(455, 15, "Kind Regards,
Tony Keenan
Chief Executive Officer");

                                                                if ($_POST["organisation"] != '') {
                                                                    $received = $_POST["organisation"];
                                                                } else {
                                                                    $received = $_POST["fname"] . " " . $_POST["last_name"];
                                                                }
                
                                                                $y1 = $pdf->GetY() + 5;
                                                                
                                                                $pdf->SetXY(35, $y1);
                                                                $pdf->Cell(0, 40, "", 0, 2);
                                                                $pdf->SetFont("Arial", "B", 12);
                                                                $pdf->Cell($wi, 14, "Launchhousing ", 0, 2, 'C');
                                                                $pdf->SetFont("Arial", "", 12);
                                                                $pdf->Cell($wi, 14, "ABN: 89 742 307 083", 0, 2, 'C');
                                                                $pdf->SetFont("Arial", "B", 12);
                                                                $pdf->Cell($wi, 14, "Official Receipt Number: $purchaseOrderNo", 0, 2, 'C');
                                                                $pdf->SetFont("Arial", "B", 10);
                                                                $pdf->Cell(0, 14, "", 0, 2);
                                                                $y1 = $pdf->GetY();
                                                                $x1 = 110;
                                                                $pdf->SetXY($x1, $y1);

                                                                $pdf->Cell(80, 18, "DATE", 1, 0, 'C');
                                                                $pdf->Cell(160, 18, "RECEIVED FROM", 1, 0, 'C');
                                                                //$pdf->Cell(150,18,"PARTICULARS",1,0,'C');
                                                                $pdf->Cell(70, 18, "AMOUNT", 1, 1, 'C');
                                                                $pdf->SetFont("Arial", "", 10);

                                                                $y1 = $pdf->GetY();
                                                                $x1 = 110;
                                                                $pdf->SetXY($x1, $y1);
                                                                $pdf->MultiCell(80, 18, date('j/m/Y'), 1, 'C');
                                                                $x1 = $x1 + 80;
                                                                $pdf->SetXY($x1, $y1);
                                                                $pdf->MultiCell(160, 18, "$received", 1, 'C');
                                                                $x1 = $x1 + 160;
                                                                $pdf->SetXY($x1, $y1);
                                                                //$pdf->MultiCell(150,18,"$particulars",1,'C');
                                                                //$x1=$x1+150;
                                                                //$pdf->SetXY($x1, $y1);
                                                                $pdf->MultiCell(70, 18, "$" . $donation_amount, 1, 'C');


                                                                $pdf->Image(get_bloginfo('template_url') . '/images/hannover-footer-pdf.jpg', 0, 785, 600);
  }
*/
#
// email stuff (change data below)
#
$to = $_POST["han_email"]; 

$from = "Launch Housing <launchhousing@Launchhousing.org.au>";
$subject = "Thank you for your donation";
if ($RequestType == 'payment') {

    $message = "

    <table cellspacing='0' cellpadding='10' border='0'>

  <tr>

    <td width='440'>
      <p>".$date_for_db."</p>
      <p>".$_POST["street"]."<br>".$_POST["city"]." <br>".$_POST["state"]." <br>".$_POST["postcode"]."</p>
    </td>

    <td width='160'>
      <p><img width='130' height='61' src='http://www.launchhousing.org.au/wp-content/uploads/2015/12/logo-small.png' ></p>
      <p>ABN 20 605 113 535 <br>
      CAN 605 113 595</p>

      <p>Launch Housing <br>
        Collingwood</p>

      <p>68 Oxford Street <br>
        Collingwood VIC 3066</p>

      <p><a href='https://twitter.com/LaunchHousing'><img height='14' width='14' src='http://www.launchhousing.org.au/wp-content/uploads/2015/12/image005-8.jpg' ></a> 03 9288 9600 <br>
      <a href='https://www.facebook.com/launchhousing'><img height='14' width='14' src='http://www.launchhousing.org.au/wp-content/uploads/2015/12/image006-7.jpg'></a> 03 9288 9601</p>


      <a href='launchhousing.org.au'>launchhousing.org.au</a>

    </td>

  </tr>

</table>

<table width='600'>
  <tr>
    <td>
      
      <p>Dear ".$_POST["fname"] . " " . $_POST["last_name"].",</p>

      <p>On behalf of everyone at Launch Housing, I would like to take this opportunity to thank you for your kind donation of $".$donation_amount." supporting Victorians struggling with housing and homelessness.</p>

      <p>Your contribution will make a very real difference to the people we support.</p>

      <p>Formed from a merger between Hanover and HomeGround Services, we aspire to be Victoria’s strongest advocates for affordable housing and leaders of research into homelessness. Our work will provide better outcomes for our clients and lasting positive change in your community.</p>

      <p>The main causes of homelessness include a critical shortage of affordable housing for those on low incomes; family violence unemployment and financial hardship. </p>

      <p>We believe housing is a basic human right that affords people dignity.</p>

      <p>Your support empowers people to break the cycle of homelessness by getting them housed and keeping them housed. Your gift will help some of the thousands of men, women, young people, children and families people we support every year. </p>

      <p>It is only with your help that we are able to continue our work so once again, thank you from everyone at Launch Housing for your generosity.</p>

      <p>Kind Regards, <br>Tony Keenan <br>
      Chief Executive Officer</p>

      <p>Launch Housing
      <br>ABN 20 605 113 595
      </p>


      <p>OFFICIAL RECEIPT NUMBER: <b>".$transact_id."</b></p>

      <table cellpadding='20'>
        <tr>
          <td>DATE</td>
          <td>RECEIVED FROM</td>
          <td>PARTICULARS</td>
          <td>AMOUNT</td>
        </tr>
        <tr>
          <td>".$date_for_db."</td>
          <td>".$_POST["fname"]." ".$_POST["last_name"]."</td>
          <td>Launch Housing donation</td>
          <td>$".$donation_amount."</td>
        </tr>
      </table>

      <p>Gifts of $2 and over are tax deductible  Received with thanks</p>

      <table>
        <tr>
          <td width='300'>
            <p>A merger between</p>
          </td>
          <td width='150'>
            <img src='http://www.launchhousing.org.au/wp-content/uploads/2015/12/homeground.png'>
          </td>
          <td width='150'>
            <img src='http://www.launchhousing.org.au/wp-content/uploads/2015/12/hanover1.png'>
          </td>
        </tr>
      </table>

    </td>
  </tr>
</table>

";
} elseif($RequestType == 'periodic') {
    $message = "


    <table cellspacing='0' cellpadding='10' border='0'>

  <tr>

    <td width='440'>
      <p>".$date_for_db."</p>
      <p>".$_POST["street"]."<br>".$_POST["city"]." <br>".$_POST["state"]." <br>".$_POST["postcode"]."</p>
    </td>

    <td width='160'>
      <p><img width='130' height='61' src='http://www.launchhousing.org.au/wp-content/uploads/2015/12/logo-small.png' ></p>
      <p>ABN 20 605 113 535 <br>
      CAN 605 113 595</p>

      <p>Launch Housing <br>
        Collingwood</p>

      <p>68 Oxford Street <br>
        Collingwood VIC 3066</p>

      <p><a href='https://twitter.com/LaunchHousing'><img height='14' width='14' src='http://www.launchhousing.org.au/wp-content/uploads/2015/12/image005-8.jpg' ></a> 03 9288 9600 <br>
      <a href='https://www.facebook.com/launchhousing'><img height='14' width='14' src='http://www.launchhousing.org.au/wp-content/uploads/2015/12/image006-7.jpg'></a> 03 9288 9601</p>

      <a href='launchhousing.org.au'>launchhousing.org.au</a>

    </td>

  </tr>

</table>

<table width='600'>
  <tr>
    <td>
          
      <p>Dear ".$_POST["fname"] . " " . $_POST["last_name"].",</p>

      <p>I would like to take this opportunity to thank you for your gift of $".$donation_amount." ".$paymentInterval_formatted." until ".$substr($_POST['start_date'], 0, 2)."/".substr($_POST['start_date'], 3, 2)."/".substr($_POST['start_date'], 6, 4)." to support Launch Housing and our work with people experiencing homelessness. Your ongoing generosity will assist those who are most in need within our community.</p>

      <p>Your contribution will make a very real difference to the people we support.</p>

      <p>Formed from a merger between Hanover and HomeGround Services, we aspire to be Victoria’s strongest advocates for affordable housing and leaders of research into homelessness. Our work will provide better outcomes for our clients and lasting positive change in your community.</p>

      <p>The main causes of homelessness include a critical shortage of affordable housing for those on low incomes; family violence unemployment and financial hardship. </p>

      <p>We believe housing is a basic human right that affords people dignity.</p>

      <p>Your support empowers people to break the cycle of homelessness by getting them housed and keeping them housed. Your gift will help some of the thousands of men, women, young people, children and families people we support every year. </p>

      <p>It is only with your help that we are able to continue our work so once again, thank you from everyone at Launch Housing for your generosity.</p>

      <p>A tax receipt which includes details of your donations for the entire financial year will be forwarded to you in July.</p>

      <p>Kind Regards, <br>Tony Keenan <br>
      Chief Executive Officer</p>

      <p>Launch Housing
      <br>ABN 20 605 113 595
      </p>


      <p>OFFICIAL RECEIPT NUMBER: <b>".$transact_id."</b></p>

      <table cellpadding='20'>
        <tr>
          <td>DATE</td>
          <td>RECEIVED FROM</td>
          <td>PARTICULARS</td>
          <td>AMOUNT</td>
        </tr>
        <tr>
          <td>".$date_for_db."</td>
          <td>".$_POST["fname"]." ".$_POST["last_name"]."</td>
          <td>Launch Housing donation</td>
          <td>$".$donation_amount."</td>
        </tr>
      </table>

      <p>Gifts of $2 and over are tax deductible  Received with thanks</p>

      <table>
        <tr>
          <td width='300'>
            <p>A merger between</p>
          </td>
          <td width='150'>
            <img src='http://www.launchhousing.org.au/wp-content/uploads/2015/12/homeground.png'>
          </td>
          <td width='150'>
            <img src='http://www.launchhousing.org.au/wp-content/uploads/2015/12/hanover1.png'>
          </td>
        </tr>
      </table>

    </td>
  </tr>
</table>


    "; // message
}

// carriage return type (we use a PHP end of line constant)
#
$eol = PHP_EOL;

#
// a random hash will be necessary to send mixed content
#
$separator = md5(time());
#
 
#

#
 
#
// attachment name
#
  //$filename = "donation-receipt.pdf";
#
 
#
// encode data (puts attachment in proper format)
#
  //$pdfdoc = $pdf->Output("", "S");
#
  //$attachment = chunk_split(base64_encode($pdfdoc));

// main header (multipart mandatory)
$headers = "From: ".$from.$eol;
$headers .= "MIME-Version: 1.0".$eol;
$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;
$headers .= "Content-Transfer-Encoding: 7bit".$eol;
$headers .= "This is a MIME encoded message.".$eol.$eol;
 
// message
$headers .= "--".$separator.$eol;
$headers .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
$headers .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
$headers .= $message.$eol.$eol;
 
// attachment
$headers .= "--".$separator.$eol;
//$headers .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol;
//$headers .= "Content-Transfer-Encoding: base64".$eol;
//$headers .= "Content-Disposition: attachment".$eol.$eol;
//$headers .= $attachment.$eol.$eol;
//$headers .= "--".$separator."--";
 
#
// send message
#
$message = '';
  mail($to, $subject, $message, $headers);
  
  

// N.D. END CREATE PDF //
      if($testing == 'no'){
        $notify_email1 = 'supporters@Launchhousing.org.au';
        $notify_email2 = 'nathan@technologymatters.com.au';
        $notify_email3 = 'competitionandmine@gmail.com';
      }else{
        //$notify_email1 = 's.dhaubanjar@andmine.com';
        //$notify_email2 = 'scloseandmine@gmail.com';
        //$notify_email2 = 'a.ong@andmine.com';
        //$notify_email1 = 'chetanmohan86@gmail.com';
        $notify_email2 = 'pavi.gulati@gmail.com';
      }
	  
      $notify_subject = 'Donation Received';
      $message = "<p>A donation has been successfully made through the Launch Housing website.</p>
      <p>Name: " . $_POST["fname"] . " " . $_POST["last_name"] . "<br/>
      Organisation: " . $_POST["organisation"] . "<br/>
      Phone: " . $_POST["phone"] . "<br/>
      Email: <a href='mailto:".$_POST["han_email"]."'>" . $_POST["han_email"] . "</a><br/>
      Amount: " . number_format( str_replace($remove, "", $_POST["payment_amount"]), 2, '.', '') . "
      </p>";
      
      // main header (multipart mandatory)
      $headers = "From: ".$from.$eol;
      $headers .= "MIME-Version: 1.0".$eol;
      $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;
      $headers .= "Content-Transfer-Encoding: 7bit".$eol;
      $headers .= "This is a MIME encoded message.".$eol.$eol;
       
      // message
      $headers .= "--".$separator.$eol;
      $headers .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
      $headers .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
      $headers .= $message.$eol.$eol;
       
      // attachment
      $headers .= "--".$separator.$eol;
      //$headers .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol;
      //$headers .= "Content-Transfer-Encoding: base64".$eol;
      //$headers .= "Content-Disposition: attachment".$eol.$eol;
      //$headers .= $attachment.$eol.$eol;
      //$headers .= "--".$separator."--";
            
            //$headers = "From: ".$from.$eol;
      //$headers .= "MIME-Version: 1.0".$eol;
            
      // message
      //$headers .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
      //$headers .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
             
            // N.D.
    //mail($_POST['han_email'], $notify_subject, $message, $headers);
    $message = '';
    mail($notify_email1, $notify_subject, $message, $headers);
    mail($notify_email2, $notify_subject, $message, $headers);
    mail($notify_email3, $notify_subject, $message, $headers);
      
    } 
    else {
      echo "
<div class='vc_container_inner container'>

  <div  class='vc_row wpb_row vc_row_stcontent vc_row-fluid vc_custom_1440733794249 donate-thanks-container'>
    <div class='vc_col-md-12 wpb_column vc_column_container'>
      <div class='wpb_wrapper'> 
        <div class='wpb_text_column wpb_content_element'>
          <div class='wpb_wrapper'>
            <img src='".get_bloginfo('template_url')."/img/securepay-logo.gif' alt='Securepay'>
            <p>&nbsp;</p>
            <h3>Sorry - but we could not process your donation!</h3>
            <p>There may have been a processing error, or you may have entered incorrect information. Please try again and check your credit card number and expiry date carefully or call <a href='tel:1800720660' target='_blank'>1800 720 660</a> to discuss donating to Launch Housing personally.</p>

            <p>The message returned from the bank was: <b>" . $responseText . "</b>. 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
";
      
        /*$notify_email1 = 'p.shrestha@andmine.com';
        $notify_email2 = 'p.shrestha@andmine.com';*/
      if($testing == 'no'){
        //$notify_email4 = 'pavi.gulati@gmail.com';
    $notify_email1 = 'supporters@Launchhousing.org.au';
        $notify_email2 = 'nathan@technologymatters.com.au';
        $notify_email3 = 'competitionandmine@gmail.com';
        //$notify_email3 = 'nathan@technologymatters.com.au';
      }else{
        //$notify_email1 = 'a.kakshapati@andmine.com';
        //$notify_email2 = 'competitionandmine@gmail.com';
        $notify_email1 = 'chetanmohan86@gmail.com';
        $notify_email2 = 'sandeepp3ldev@gmail.com';
    
      }
      
      
      $notify_subject = 'Donation Unsuccessful!';
      $notify_headers = 'From: Launch Housing <launchhousing@Launchhousing.org.au>';
      $notify_message = "
      An error has occurred while making a donation through the Launch Housing website. Please contact the donor to discuss, with the details below:\r\n
      Name: " . $_POST["fname"] . " " . $_POST["last_name"] . "\r
      Organisation: " . $_POST["organisation"] . "\r
      Phone: " . $_POST["phone"] . "\r
      Email: " . $_POST["han_email"] . "\r";
    //mail($notify_email4, $notify_subject, $notify_message, $notify_headers);
    
      mail($notify_email1, $notify_subject, $notify_message, $notify_headers);
      mail($notify_email2, $notify_subject, $notify_message, $notify_headers);
      mail($notify_email3, $notify_subject, $notify_message, $notify_headers);            
      //mail($_POST['han_email'], $notify_subject, $notify_message, $notify_headers);      
            }     

        echo "
        <div class='vc_container_inner container'>
          <div  class='vc_row wpb_row vc_row_stcontent vc_row-fluid return-to-donate-para'>
            <div class='vc_col-md-12 wpb_column vc_column_container'>
              <div class='wpb_wrapper'> 
                <div class='wpb_text_column wpb_content_element'>
                  <div class='wpb_wrapper'>
                    <p class='return'><a href=\"donate\">Return to Donate Page</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        ";
        echo "</body></html>";
        break;
}


?>

<!-- mid ends -->


<!-- top-bottom ends -->

<!-- content ends -->
<?php //woo_subscribe_connect(); ?>
<?php //woo_postnav(); ?>

<!-- col-mid ends -->

<?php //if (get_option('woo_ad_content') == 'true') {  include (TEMPLATEPATH . "/ads/content_ad.php"); } ?>
<?php
                //$comm = get_option('woo_comments'); if ( $comm == "post" || $comm == "both" ) : ?>
<?php //comments_template('', true); ?>
<?php //endif; ?>
<?php endwhile; ?>
<?php wp_reset_query();?>
</div>
</div>
</div>
</section>
<!-- col-2 starts -->
<!-- col-2 ends -->
<div class="fix"></div>
<?php else: ?>
<!-- /.post -->
<?php endif; ?>
<?php //get_sidebar(); ?>


    <!-- container ends -->
    
<?php
// generate unique message ID
// this function makes use a a password generator for this purpose
function generateMessageID($length=20, $strength=4) 
{
    $vowels = 'aeuy';
    $consonants = 'bdghjmnpqrstvz';
    if ($strength & 1) {
        $consonants .= 'BDGHJLMNPQRSTVWXZ';
    }
    if ($strength & 2) {
        $vowels .= "AEUY";
    }
    if ($strength & 4) {
        $consonants .= '123456789012345';
    }
 //  message id must be alpha numeric so cannot use special characters
 //   if ($strength & 8) {
 //       $consonants .= '@#$%';
 //   }

    $messageID = '';
    $alt = time() % 2;
    for ($i = 0; $i < $length; $i++) {
        if ($alt == 1) {
            $messageID .= $consonants[(rand() % strlen($consonants))];
            $alt = 0;
        } else {
            $messageID .= $vowels[(rand() % strlen($vowels))];
            $alt = 1;
        }
    }
    return $messageID;
}

// set up XML transactions according to transaction type
function setPaymentCreditCard($merchantID, $time, $transaction_password, $transact_id)
{
    $remove = array('$', ',');  // remove these characters from the donated amount
    $setXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" . 
  "<SecurePayMessage>" .
        "<MessageInfo>" . 
      "<messageID>" . $_POST["message_id"] . "</messageID>" .
          "<messageTimestamp>" . $time . "</messageTimestamp>" .
          "<timeoutValue>60</timeoutValue>" . 
      "<apiVersion>xml-4.2</apiVersion>" .
        "</MessageInfo>" . 
    "<MerchantInfo>" . 
      "<merchantID>" . $merchantID . "</merchantID>" . 
      "<password>" . $transaction_password . "</password>" .
        "</MerchantInfo>" . 
    "<RequestType>" . $_POST["request_type"] . "</RequestType>" .
        "<Payment>" . 
      "<TxnList count=\"1\">" . 
        "<Txn ID=\"1\">" . 
          "<txnType>" . $_POST["payment_type"] . "</txnType>" . 
          "<txnSource>23</txnSource>" . 
          "<amount>" .  str_replace(".", "", number_format( str_replace($remove, "", $_POST["payment_amount"]), 2, '.', '')) . "</amount>" .
              "<purchaseOrderNo>" . $transact_id . "</purchaseOrderNo>" . 
          "<currency>" . $_POST["currency"] . "</currency>" . 
          "<preauthID>" . $_POST["preauthid"] . "</preauthID>" . 
          "<txnID>" . $_POST["txnid"] . "</txnID>" . 
          "<CreditCardInfo>" . 
            "<cardNumber>" . $_POST["card_number"] . "</cardNumber>" . 
            "<cvv>" . $_POST["card_cvv"] . "</cvv>" . 
            "<expiryDate>" . $_POST["card_expiry_month"] . "/" . $_POST["card_expiry_year"] . "</expiryDate>" . 
          "</CreditCardInfo>" .
            "</Txn>" . 
      "</TxnList>" . 
    "</Payment>" . 
  "</SecurePayMessage>";
      
    return $setXML;
}

function setPeriodicCreditCardPayment($merchantID, $time, $recurring, $transaction_password,
    $start_date, $transact_id)
{
    $remove = array('$', ',');  // remove these characters from the donated amount
    $setXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" . "<SecurePayMessage>" .
        "<MessageInfo>" . "<messageID>" . $_POST["message_id"] . "</messageID>" .
        "<messageTimestamp>" . $time . "</messageTimestamp>" .
        "<timeoutValue>60</timeoutValue>" . "<apiVersion>spxml-3.0</apiVersion>" .
        "</MessageInfo>" . "<MerchantInfo>" . "<merchantID>" . $merchantID .
        "</merchantID>" . "<password>" . $transaction_password . "</password>" .
        "</MerchantInfo>" . "<RequestType>" . $_POST["request_type"] . "</RequestType>" .
        "<Periodic>" . "<PeriodicList count=\"1\">" . "<PeriodicItem ID=\"1\">" .
        "<actionType>" . $_POST["action_type"] . "</actionType>" . "<clientID>" . $transact_id .
        "</clientID>" . "<currency>" . $_POST["currency"] . "</currency>" .
        "<CreditCardInfo>" . "<cardNumber>" . $_POST["card_number"] . "</cardNumber>" .
        "<cvv>" . $_POST["card_cvv"] . "</cvv>" . "<expiryDate>" . $_POST["card_expiry_month"] .
        "/" . $_POST["card_expiry_year"] . "</expiryDate>" . "<recurringFlag>" . $recurring .
        "</recurringFlag>" . "</CreditCardInfo>" . 
    "<amount>" .  str_replace(".", "", number_format( str_replace($remove, "", $_POST["payment_amount"]), 2, '.', '')) . "</amount>" .
        "<periodicType>" . $_POST["periodic_type"] . "</periodicType>" .
        "<paymentInterval>" . $_POST["payment_interval"] . "</paymentInterval>" .
        "<startDate>" . $start_date . "</startDate>" . "<numberOfPayments>" . $_POST["number_of_payments"] .
        "</numberOfPayments>" . "</PeriodicItem>" . "</PeriodicList>" . "</Periodic>" .

        "</SecurePayMessage>";
    return $setXML;
}

function getGMTtimeStamp()
{
    $stamp = date("YmdHis") . "000+1000";
    return $stamp;
}

function openSocket($host, $query)
{
    // Break the URL into usable parts
  $path = explode('/',$host);
  $host = $path[0];
  unset($path[0]);
  $path = '/'.(implode('/',$path));
  
  
  
  // Prepare the post query
  $post  = "POST $path HTTP/1.1\r\n";
  $post .= "Host: $host\r\n";
  $post .= "Content-type: application/x-www-form-urlencoded\r\n";
  $post .= "Content-type: text/xml\r\n";
  $post .= "Content-length: ".strlen($query)."\r\n";
  $post .= "Connection: close\r\n\r\n$query";

  //echo "<p>post = </p>";
  //echo $post;

  $h = fsockopen("ssl://".$host, 443, $errno, $errstr);
  
   if ($errstr){
     print "$errstr ($errno)<br/>\n";
   }
   fwrite($h,$post);
  
   /*******************************************/
   /* Retrieve the HTML headers (and discard) */
   /*******************************************/

  $headers = "";
  while ($str = trim(fgets($h, 4096))) {
//echo "Headers1: ".$str."\n";
    $headers .= "$str\n";
  }

  $headers2 = "";
  while ($str = trim(fgets($h, 4096))) {
    $headers2 .= "$str\n";
  }

  /**********************************************************/
  /* Retrieve the response */
  /**********************************************************/
  
  $body = "";
  while (!feof($h)) {
    $body .= fgets($h, 4096);
  }
  
  // Close the socket
  fclose($h);
  
  return $body;
}


function openSocket_backup($host, $query)
{
    /**************************/
    /* Secure Socket Function */
    /**************************/

    // Break the URL into usable parts
    $path = explode('/', $host);
    $host = $path[0];
    unset($path[0]);
    $path = '/' . (implode('/', $path));

    // Prepare the post query
    $post = "POST $path HTTP/1.1\r\n";
    $post .= "Host: $host\r\n";
    $post .= "Content-type: application/x-www-form-urlencoded\r\n";
    $post .= "Content-type: text/xml\r\n";
    $post .= "Content-length: " . strlen($query) . "\r\n";
    $post .= "Connection: close\r\n\r\n$query";

    /***********************************************/
    /* Open the secure socket and post the message */
    /***********************************************/
    $h = fsockopen("ssl://" . $host, 443, $errno, $errstr);

    if ($errstr)
        print "$errstr ($errno)<br/>\n";
    fwrite($h, $post);

    /*******************************************/
    /* Retrieve the HTML headers (and discard) */
    /*******************************************/

    $headers = "";
    while ($str = trim(fgets($h, 4096)))
    {
        $headers .= "$str\n";
    }

    $headers2 = "";
    while ($str = trim(fgets($h, 4096)))
    {
        $headers2 .= "$str\n";
    }

    /**********************************************************/
    /* Retrieve the response */
    /**********************************************************/

    $body = "";
    while (!feof($h))
    {
        $body .= fgets($h, 4096);
    }

    // Close the socket
    fclose($h);
	
    // Return the body of the response
    return $body;
}


function makeXMLTree($data)
{
    $output = array();

    $parser = xml_parser_create();

    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, $data, $values, $tags);
    xml_parser_free($parser);

    $hash_stack = array();

    foreach ($values as $key => $val)
    {
        switch ($val['type'])
        {
            case 'open':
                array_push($hash_stack, $val['tag']);
                break;

            case 'close':
                array_pop($hash_stack);
                break;

            case 'complete':
                array_push($hash_stack, $val['tag']);
                eval("\$output['" . implode($hash_stack, "']['") . "'] = \"{$val['value']}\";");
                array_pop($hash_stack);
                break;
        }
    }

    return $output;
}
?>
    
<?php get_footer(); ?>