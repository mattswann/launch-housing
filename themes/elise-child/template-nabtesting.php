<?php
/*
Template Name: Nab Testing
*/
get_header();
?>
<div class="container">
	<div class="row">
<div class="col-md-12">
<form method='POST' action='new_test.php'>
	<label for="title title">Title:: </label>
		<select required="" id="title" name="title">
			<option value="">Please select title</option>
			<option value="Mr">Mr</option>
			<option value="Mrs">Mrs</option>
			<option value="Mr &amp; Mrs">Mr &amp; Mrs</option>    
			<option value="Ms">Ms</option>
			<option value="Mids">Miss</option>
			<option value="Dr">Dr</option>
		</select>
<label for="fname">First Name: <span style="color: #f00;">*</span></label>
<input type="text" required="" value="" class="inputbox" size="40" id="fname" name="fname">	

<label for="last_name">Last Name: <span style="color: #f00;">*</span></label>
	<input type="text" required="" value="" class="inputbox" size="40" id="last_name" name="last_name">
	
	<p><label for="position">Position:</label><input type="text" value="" class="inputbox" size="40" id="position" name="position"></p>
	<p><label for="organisation">Organisation:</label><input type="text" value="" class="inputbox" size="40" id="organisation" name="organisation"></p>
	<p><label for="han_email">Email address: </label><span style="color: #f00;">*</span><input type="email" required="" value="" class="inputbox" size="40" id="han_email" name="han_email"></p>
	<p><label for="phone">Daytime Phone: </label>
<input type="text" value="" class="inputbox" size="40" id="phone" name="phone"></p>
<p><label for="street">Street: </label><span style="color: #f00;">*</span><input type="text" required="" value="" class="inputbox" size="40" id="street" name="street"></p>
<p><label for="city">Suburb: </label><span style="color: #f00;">*</span><input type="text" required="" value="" class="inputbox" size="40" id="city" name="city"></p>
<p><label for="state">State: </label><span style="color: #f00;">*</span>
<select required="" name="state" id="state">
<option value="">Please select region, state or province</option>
<option title="Australian Capital Territory" value="Australian Capital Territory">Australian Capital Territory</option>
<option title="New South Wales" value="New South Wales">New South Wales</option>
<option title="Northern Territory" value="Northern Territory">Northern Territory</option>
<option title="Queensland" value="Queensland">Queensland</option>
<option title="South Australia" value="South Australia">South Australia</option>
<option title="Tasmania" value="Tasmania">Tasmania</option>
<option title="Victoria" value="Victoria">Victoria</option>
<option title="Western Australia" value="Western Australia">Western Australia</option>
</select>
</p>
<p><label for="postcode">Postcode: </label><span style="color: #f00;">*</span><input type="text" required="" value="" class="inputbox" size="40" id="postcode" name="postcode"></p>
<p><input type="checkbox" style="width:5%;" checked="" value="yes" name="signup" class="roundedOne"> <span style="width: 96%;float: left;font-size: 12px;margin-top: -18px;line-height: 15px;">Please send me informational material about Launch Housing</span></p>

	
		
<input type="radio" name="time_span" value="Periodic"> Periodic<br>
<input type="radio" name="time_span" value="One Time"> One Time<br>  	
<input type='hidden' name='EPS_TIMESTAMP' value='20050620123505'>
<input type='hidden' name='EPS_AMOUNT' value='53.20'>
<input type='hidden' name='EPS_MERCHANT' value='2MO0010'>
<input type='hidden' name='EPS_PASSWORD' value='Ouu2JXYA'>
<input type='hidden' name='EPS_REFERENCEID' value='Test Reference'>
Card Type : <input type='text' name='EPS_CARDTYPE'>
Amount : <input type='text' name='EPS_AMOUNT'>
<select name='EPS_EXPIRYMONTH'>
<option value='1'>01</option>
<option value='2'>02</option>
<option value='3'>03</option>
<option value='4'>04</option>
<option value='5'>05</option>
<option value='6'>06</option>
<option value='7'>07</option>
<option value='8'>08</option>
<option value='9'>09</option>
<option value='10'>10</option>
<option value='11'>11</option>
<option value='12'>12</option>
</select>
<select name='EPS_EXPIRYYEAR'>
<option value='2016'>2016</option>
<option value='2017'>2017</option>
<option value='2018'>2018</option>
<option value='2019'>2019</option>
<option value='2020'>2020</option>
</select>
Cvv number : <input type='text' name='EPS_CCV'>
<input type='hidden' name='EPS_REFERENCEID' value='Test Reference'>
<input type='hidden' name='EPS_AMOUNT' value='1.00'>
<input type='hidden' name='EPS_TIMESTAMP' value='200506141010'>
<input type='hidden' name='EPS_FINGERPRINT' value='76f5gf5df6d57654fd4f'>
<input type='hidden' name='EPS_RESULTURL' value='https://www.resulturl.com'>
Customer-entered fields:
Card Number :<input type='text' name='EPS_CARDNUMBER'>
<input type='hidden' name='EPS_TXNTYPE' value='ANTIFRAUD'>
<input type='hidden' name='EPS_3DSECURE' value='true'>
<input type='hidden' name='3D_XID' value='12345678901234567890'>
<input type='hidden' name='EPS_MERCHANTNUM' value='22123456'>
<input type='hidden' name='EPS_IP' value='203.123.456.789'>
Optional (any combination is acceptable):
<input type='hidden' name='EPS_FIRSTNAME' value='John'>
<input type='hidden' name='EPS_LASTNAME' value='Smith'>
<input type='hidden' name='EPS_ZIPCODE' value='2345'>
<input type='hidden' name='EPS_TOWN' value=''>
<input type='hidden' name='EPS_BILLINGCOUNTRY' value='US'>
<input type='hidden' name='EPS_DELIVERYCOUNTRY' value='US'>
<input type='hidden' name='EPS_EMAILADDRESS' value='john@email.com'>
<input type='submit' name='submit' value=Submit>
</form>
</div></div></div>
<?php get_footer();?>
