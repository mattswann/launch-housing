<?php 
/** 
 * Content: No Results/No content found 
*/
 ?>

<h3><?php _e('No results were found matching your criteria.', 'Elise'); ?></h3>
<small><?php _e('Please try something else.', 'Elise'); ?></small>