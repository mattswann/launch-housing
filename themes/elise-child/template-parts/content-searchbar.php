<?php 
/**
 * Content: Service Search Bar  
*/
 ?>

<div class="panel-group searchcat-mobile">
  <div class="panel panel-default">
    <div class="panel-heading">
      <a data-toggle="collapse" href="#collapse1">
        <h4 class="panel-title">Search for Services</h4>
      </a>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
        <?php  echo do_shortcode( '[searchandfilter fields="client-type,region" headings="Services for, located in" submit_label="Find"]' ); ?>
      </div>
    </div>
  </div>
</div>

<div class="searchcat-large">
  <?php  echo do_shortcode( '[searchandfilter fields="client-type,region" headings="Services for, located in" submit_label="Find"]' ); ?>
</div>