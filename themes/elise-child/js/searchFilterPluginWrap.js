jQuery(document).ready(function($) {
	/** 
	 * wraps a <div> tag to <select> or any form element in Search and Filter plugin
	 * This enables the use of pseudo-classes to the form fields through the wrapper div
	*/
	$('select.postform').wrap('<div class="select-wrapper"></div>');
	// $('.searchandfilter input[type="submit"]').wrap('<div class="btn-wrapper"></div>');
});