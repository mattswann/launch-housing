
/* ------------------------------------------------------------ */
/*  Common Problems and Solutions */
/* ------------------------------------------------------------ */

** please update to maintain sanity **

1. When using LH Post Grid VC element, there is a padding that makes the element not aligned to the page margins
	SOLUTION: 
	Check the column (not row) of the VC element while editing the page/post. You can set the padding left and right to 0

2. Breadcrumbs and page title do not show up for new custom post types
	SOLUTION: 
	Make sure the CPT settings are set to
				- Rewrite: true
				- Has Archive: true

3. Breadcrumbs do not follow the right page structure/ you want to customise it
	SOLUTION: 
	Check Elise Breadcrumbs Override in child theme functions.php, and edit the code according to how you want the breadcrumbs to show up.

4. Page Options metabox not showing up on new custom post type edit page
	SOLUTION: 
	In the child theme, go to eliseCore > ReduxCore > elise-meta-config.php, then edit the array with post types, adding your new custom post type

5. Cannot preview custom post type Post
	SOLUTION:
	In the CPT UI settings, Post Format should not be ticked