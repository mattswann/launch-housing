<?php 
/**
 * Category Template
 * This template is used for display post by category
 * for this page, classes from Elise index.php was used
*/
get_header(); 
?>

<?php 
  global $elise_options;

  // Blog style
  if ($elise_options['opt-blog-style'] == 1) {
    $style_class = 'blog-large';
  }
  elseif ($elise_options['opt-blog-style'] == 2) {
    $style_class = 'blog-medium';
  }
  elseif ($elise_options['opt-blog-style'] == 3) {
    $style_class = 'blog-masonry row bm-hidden';
  }
  elseif ($elise_options['opt-blog-style'] == 4) {
    $style_class = 'blog-full-width';
  }

  // Sidebar position
  if ($elise_options['opt-blog-style'] != 4) {
    if ($elise_options['opt-show-sidebar'] == 1) {
      if ($elise_options['opt-blog-sidebar-position'] == 1) {
        $section_class = 'col-md-9 sidebar-content';
        $sidebar_class = 'col-md-3';
        $content_class = 'sidebar-right';
      }
      else {
        $section_class = 'col-md-9 col-md-push-3 sidebar-content';
        $sidebar_class = 'col-md-3 col-md-pull-9';
        $content_class = 'sidebar-left';
      }
    }
    else {
      $section_class = 'col-md-12 no-sidebar';
    }
  }
  else {
    $section_class = 'col-md-8 col-md-offset-2 no-sidebar';
  }
?>
     

      <div class="content blog-content-wrap sidebar-right section">
        <div class="container">
          <div class="row">
            <div class="col-md-9 sidebar-content">
              <div class="row">
                <div class="col-md-12">
                  <?php echo do_shortcode( '[searchandfilter taxonomies="category" headings="I am looking for" submit_label="Find"]' ); ?>
                  <!-- vc separator (elise) -->
                  <div class="wpb_column vc_column_container vc_col-md-12">
                    <div class="vc_column-inner ">
                      <div class="wpb_wrapper">
                        <div class="elise_separator wpb_content_element elise_separator_align_center elise_el_width_wide">
                          <div class="elise_sep_wrapper">
                            <div class="elise_sep_container">
                              <span class="elise_sep_holder elise_sep_holder_l"><span class="elise_sep_line"></span></span>
                              <span class="elise_sep_holder elise_sep_holder_r"><span class="elise_sep_line"></span></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- vc separator (elise) end -->
                  <h2><?php echo single_cat_title ('', false) ?></h2>
                  <p><?php echo category_description(); ?></p>
                  <!-- vc separator (elise) -->
                  <div class="wpb_column vc_column_container vc_col-md-12">
                    <div class="vc_column-inner ">
                      <div class="wpb_wrapper">
                        <div class="elise_separator wpb_content_element elise_separator_align_center elise_el_width_wide">
                          <div class="elise_sep_wrapper">
                            <div class="elise_sep_container">
                              <span class="elise_sep_holder elise_sep_holder_l"><span class="elise_sep_line"></span></span>
                              <span class="elise_sep_holder elise_sep_holder_r"><span class="elise_sep_line"></span></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- vc separator (elise) end -->
                </div>
              </div>
              <div class="blog-large">

                <?php
                if(have_posts()) : while(have_posts()) : the_post();
                  
                  get_template_part( 'includes/post-templates/content', get_post_format() );  ?>
          
                <?php endwhile; else : ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class('no-posts'); ?>>

                  <h2><?php _e('No posts were found.', 'Elise'); ?></h2>
                
                </article>

               <?php endif; ?>

                <?php
                if ( function_exists('elise_pagination') ) {
                  elise_pagination();
                }
                ?>

              </div>
            </div>

            <aside class="col-md-3 sidebar-wrap">

              <?php get_sidebar(); ?>

            </aside>

          </div>
        </div>
      </div>


<?php get_footer(); ?>