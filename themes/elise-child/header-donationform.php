<?php 
  global $elise_options;
?>

<!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
      <meta charset="<?php bloginfo('charset'); ?>">
      <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
      <?php if ($elise_options['opt-responsive'] == 1) { ?>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <?php } ?>

      <!-- Pingbacks -->
      <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
      
      <?php if (isset($elise_options['opt-favicon']['url']) && $elise_options['opt-favicon']['url']) { ?>      
        <link rel="icon" type="image" href="<?php echo esc_url($elise_options['opt-favicon']['url']) ?>"/>
      <?php } ?>

      <?php if (isset($elise_options['opt-apple-icons']['url']) && $elise_options['opt-apple-icons']['url'] ) { ?>

        <link href="<?php echo esc_url(apple_touch_icons(72)); ?>" rel="apple-touch-icon" sizes="76x76" />
        <link href="<?php echo esc_url(apple_touch_icons(120)); ?>" rel="apple-touch-icon" sizes="120x120" />
        <link href="<?php echo esc_url(apple_touch_icons(152)); ?>" rel="apple-touch-icon" sizes="152x152" />
        <link href="<?php echo esc_url(apple_touch_icons(180)); ?>" rel="apple-touch-icon" sizes="180x180" />

      <?php } ?>
	
      <?php wp_head(); ?>

    </head>
    <body <?php body_class(); ?>>

    <?php 
    if ($elise_options['opt-show-header'] == 1) {

      if ($elise_options['opt-show-sticky-header'] == 1) {
        include('includes/headers/addons/sticky-header.php');
      }
    }

    $layout_wrapper_shadow = '';
    if ($elise_options['opt-layout'] != 1) {

      if ($elise_options['opt-layout-wrapper-shadow'] == 1) {
        $layout_wrapper_shadow = ' layout-wrapper-shadow';
      }
    }
    ?>

    <div class="layout-wrapper <?php echo esc_attr($layout_wrapper_shadow) ?>">

    <?php if ($elise_options['opt-show-header'] == 1) { ?>

    <?php 

    get_template_part('includes/headers/header', elise_header_style() );
	?>
	<section class="page-title-container page-title-left">

      	
        <div class="container">
          <div class="row">
            <!-- Page Title -->
            <div class="col-md-12 donation-form-responsive">
              <div class="page-title">
              	<div class="title-wrap">
				
              	 <?php
				 require ('customfiles/formstyle.php');
                 require ('customfiles/donation_form.php');
				 
				 ?>
              	<!--span class="page-title"><span class="fancy-font contact-us">Donate</span></span><br> <span class="phone-number"><span class="line-1">Form goes here</span><br> <span class="line-2">Form goes here</span><br><span class="line-3">Form goes here</span>&nbsp;<br><span class="line-5">Form goes here
</span></span-->     

<?php 

//if (function_exists('elise_page_title')) {
      //elise_page_title(); 
   // }
//if (have_posts()) : $count = 0; ?>
<?php //while (have_posts()) : the_post(); $count++; 
		//$pid = get_the_ID();
		?>         	            	
<span class="col-md-7" style="float:left;"><span class="fancy-font contact-us"> <br></span></span>
<span class="col-md-7" style="float:left;"><span class="fancy-font contact-us"> <br></span></span>
<span class="col-md-7" style="float:left;"><span class="fancy-font contact-us"> <br></span></span>
<span class="col-md-7" style="float:left;"><span class="fancy-font contact-us"> <br></span></span>
<span class="col-md-7" style="float:left;"><span class="fancy-font contact-us"> <br></span></span>
<span class="col-md-7" style="float:left;"><span class="fancy-font contact-us"> <br></span></span>
<span class="col-md-7" style="float:left;"><span class="fancy-font contact-us"> <br></span></span>
<span class="col-md-7 line-rmv" style="float:left;"><span class="fancy-font contact-us line-rmv"> <br></span></span>
<span style="float: left;" class="col-md-7"><span class="fancy-font contact-us" style="color:#ffffff;">
<?php
if (have_posts()) : $count = 0;
while (have_posts()) : the_post(); $count++;
//Your donation <br>will help end <br>homelessness.
the_title();
?>
<?php endwhile; endif;  ?>
</span></span>
<?php //endwhile; endif;  ?>
				</div>

	          </div>
            </div><!-- Page Title End -->
          </div>
        </div>
      </section>
	<?php
    //if (function_exists('elise_page_title')) {
      //elise_page_title(); 
    //}
	
	
   

    if ($elise_options['opt-breadcrumbs-style'] == 1) {
      elise_breadcrumbs();
    } // breadcrumbs style - bar 

    ?>

    <?php } // blank template ending ?>