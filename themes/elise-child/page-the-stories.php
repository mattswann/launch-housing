<?php 
/**
 * Page: The Stories
 * This template is used for slug the-stories, which contains posts of "story" post type
 * for this page, classes from Elise blog masonry was used
*/

wp_reset_postdata();
get_header(); 
$args = array(
                    'post_type' => 'story',
                    'orderby' => 'post_id', 
                    'order' => 'DESC'
                );
$query = new WP_Query( $args );
?>
     
  <div class="content blog-content-wrap section">
    <div class="container">
      <div class="row">
        <section class="col-md-12 no-sidebar">

          <!-- services post grid -->
          <div class="blog-masonry row bm-visible">
              <?php $colnum = 0; if( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
              <div class="col-md-4 col-sm-6">
                <div class="blog-grid">
                  <?php // from parent theme 
                    get_template_part( 'includes/post-templates/content', get_post_format() );  ?>
                </div>
              </div>
              <?php endwhile; 
              else : ?>
                <div class="vc_col-md-12 wpb_column vc_column_container">
                  <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                      <?php get_template_part( 'template-parts/content', 'none' ); ?>
                    </div>
                  </div>
                </div>
              <?php endif; ?>    
          </div>
          <!-- services post grid end -->
    
        </section>
      </div>
    </div>
  </div>

<?php get_footer(); ?>