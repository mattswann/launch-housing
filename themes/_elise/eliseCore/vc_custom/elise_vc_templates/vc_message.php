<?php
$output = $color = $el_class = $css_animation = '';
extract(shortcode_atts(array(
    'color' => 'alert-info',
    'el_class' => '',
    'style' => '',
    'css_animation' => '',
    'bg_color' => '',
), $atts));
$el_class = $this->getExtraClass($el_class);

$class = "";
$bg_color_class = '';
if (!empty($bg_color)) {
	$bg_color_class = 'style="background:'. esc_attr($bg_color) .'"';
}

//$style = "square_outlined";
// $class .= ($style!='') ? ''.$style : '';
$class .= ( $color != '' && $color != "alert-block") ? ' '.$color : '';

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'alert ' . $class . $el_class, $this->settings['base'], $atts );

$css_class .= $this->getCSSAnimation($css_animation);
?>
<div class="<?php echo $css_class; ?>" <?php echo $bg_color_class; ?>>
	<?php echo wpb_js_remove_wpautop($content, true); ?>
</div>
<?php echo $this->endBlockComment('alert box')."\n";