<?php
$output = $title = $interval = $el_class = '';
extract( shortcode_atts( array(
	'title' => '',
	'interval' => 0,
	'el_class' => '',
	'tab_bg' => '',
	'tabs_bg' => '',
	'tour_border' => '',
	'tab_active_color' => '',
	'css_animation' => '',
), $atts ) );

// echo $tabs_bg;

if (!empty($tab_bg) || !empty($tab_active_color) || !empty($tabs_bg)) {
	$tab_bg_style = 'style="background:'. $tab_bg .'"';

	$css_active = '';
	$css_reg = '';
	if (!empty($tab_bg)) {
		$css_active .= 'background:'. $tab_bg .';';
	}
	if (!empty($tab_active_color)) {
		$css_active .= 'color:'. $tab_active_color .';';
	}
	if (!empty($tabs_bg)) {
		$css_reg .= 'background:'. $tabs_bg .';';
	}

	$random_class = 'tab-style-'.rand(0, 9999);

	$output .= '<style type="text/css" scoped>';
	if (!empty($tab_bg) || !empty($tab_active_color)) {
		$output .= '.'.$random_class.'>li.ui-tabs-active>a {'. $css_active .'}';
	}
	if (!empty($tabs_bg)) {
		$output .= '.'.$random_class.'>li>a {'. $css_reg .'}';
	}

	$output .= '</style>';

} else {
	$tab_bg_style = '';
	$random_class = '';
}

$tour_border_style = '';
if ( 'vc_tour' == $this->shortcode && !empty($tour_border) ) {
	$tour_border_style = 'style="border-color: '. $tour_border .' !important;"';
}

wp_enqueue_script( 'jquery-ui-tabs' );

$el_class = $this->getExtraClass( $el_class );

$element = 'wpb_tabs';
if ( 'vc_tour' == $this->shortcode ) $element = 'wpb_tour';

// Extract tab titles
preg_match_all( '/vc_tab([^\]]+)/i', $content, $matches, PREG_OFFSET_CAPTURE );
$tab_titles = array();
/**
 * vc_tabs
 *
 */
if ( isset( $matches[1] ) ) {
	$tab_titles = $matches[1];
}
$tabs_nav = '';
$tabs_nav .= '<ul class="wpb_tabs_nav elise-tabs-nav ui-tabs-nav vc_clearfix '.$random_class.'">';
foreach ( $tab_titles as $tab ) {
	$tab_atts = shortcode_parse_atts($tab[0]);
	if(isset($tab_atts['title'])) {
		$tabs_nav .= '<li><a href="#tab-' . ( isset( $tab_atts['tab_id'] ) ? $tab_atts['tab_id'] : sanitize_title( $tab_atts['title'] ) ) . '" '. $tour_border_style .' >' . $tab_atts['title'] . '</a></li>';
	}
}
$tabs_nav .= '</ul>' . "\n";

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, trim( $element . ' wpb_content_element ' . $el_class ), $this->settings['base'], $atts );
$css_class .= $this->getCSSAnimation($css_animation);

$output .= "\n\t" . '<div class="' . $css_class . '" data-interval="' . $interval . '">';
$output .= "\n\t\t" . '<div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix">';
$output .= wpb_widget_title( array( 'title' => $title, 'extraclass' => $element . '_heading' ) );
$output .= "\n\t\t\t" . $tabs_nav;
$output .='<div class="tab-content" '. $tab_bg_style .'>';
$output .= "\n\t\t\t" . wpb_js_remove_wpautop( $content );
$output .='</div>';
// if ( 'vc_tour' == $this->shortcode ) {
// 	$output .= "\n\t\t\t" . '<div class="wpb_tour_next_prev_nav vc_clearfix"> <span class="wpb_prev_slide"><a href="#prev" title="' . __( 'Previous tab', 'js_composer' ) . '">' . __( 'Previous tab', 'js_composer' ) . '</a></span> <span class="wpb_next_slide"><a href="#next" title="' . __( 'Next tab', 'js_composer' ) . '">' . __( 'Next tab', 'js_composer' ) . '</a></span></div>';
// }
$output .= "\n\t\t" . '</div> ' . $this->endBlockComment( '.wpb_wrapper' );
$output .= "\n\t" . '</div> ' . $this->endBlockComment( $element );

echo $output;
